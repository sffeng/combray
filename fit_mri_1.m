% Reworked for combray fmri data
% Runs the "round 6a" fits, which are the round 6 with added
% T0. Trials pooled across subjects for each of the four cues: .5,
% .4/.6, .3/.7, .2/.8, pooling accross all perceptual condition.
%
% 11 Oct 2015: copied from fit_6b.m, adjusted for mri data

clear
close all

%% Genetic algorithm options
lb = [-.2 -.2 0.05 1.75  0]; %d1 d2 z dl T0
ub = [3.0 3.0 5.0  10.75  0.5];
nVars = length(lb);
opts = gaoptimset('UseParallel','always','Display','diagnose',...
                  'Generations',100*nVars,...
                  'PopulationSize',10*nVars);

%% Data settings
datadir = 'mri_data/';
subjects = [7:18 20:23];
cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
percept = [.65 .85];
% cueArray = [.3 .7];
% percept = [.85];

%% Setup arrays
recoveredParameters = nan(4,length(ub));
nTrialArray = nan(4,1);
finalChiSq = nan(4,1);
exitFlags = nan(4,1);


for k = 1:length(cueArray(:,1))
    
    % collect data and output basic stuff
    cues = cueArray(k,:);
    [tr] = gettesttrials(subjects,cues,0,percept,datadir);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues)])

    
    % Solve the sucker
    rng(19850604,'twister'); 
    [recoveredParameters(k,:),finalChiSq(k), exitFlags(k)] = ...
        ga(@(x) obj2a1z1DT0(x,rt,resp), length(lb), [],[],[],[],...
           lb,ub,[],opts);

    nTrialArray(k) = nTrials;
    exitFlags
    recoveredParameters
    nTrials = nTrialArray;
    save('fit_mri_1.mat','recoveredParameters','nTrials','finalChiSq','exitFlags')
end

    nTrials = nTrialArray;
    save('fit_mri_1.mat','recoveredParameters','nTrials','finalChiSq','exitFlags')

