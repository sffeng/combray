function val = obj_1ddm_2(x,rt,rtResp)
d1 = x(1);
d2 = x(2);
dl = x(3);
z = x(4);
x1 = x(5);
x2 = x(6);
T0 = x(7);

s = 1;

tFinal = max(rt)+3;

nPts = 1000;
tArray = linspace(0,tFinal,nPts);
yPlus = zeros(1,nPts);
yMinus = zeros(1,nPts);
[~,dlInd] = min(abs(tArray - dl));

% First stage
[p1Minus,~,myP1Minus] = ddmpdf(tArray,0,d1,T0,x1,z,s);
[p1Plus,~,myP1Plus]   = ddmpdf(tArray,1,d1,T0,x1,z,s);
p1Minus(1) = 0;
p1Plus(1) = 0;

yMinus(1:dlInd) = p1Minus(1:dlInd);
yPlus(1:dlInd)  = p1Plus(1:dlInd);

P1 = trapz(tArray,yMinus)+trapz(tArray,yPlus);

% Diagnostic %

% disp('First Stage')
% trapz(tArray,p1Minus)
% trapz(tArray,p1Plus)

% figure(1)
% plot(tArray,p1Minus,'r')
% hold on
% plot(tArray,p1Plus,'b')
% title('first stage')
% hold off

% disp('overall')
% trapz(tArray,yMinus)
% trapz(tArray,yPlus)

% figure(2)
% plot(tArray,yMinus,'r')
% hold on
% plot(tArray,yPlus,'b')
% title('overall')
% hold off

% end diagnostic %

% Second stage

[p2Minus,~,myP2Minus] = ddmpdf(tArray,0,d2,dl,x2,z,s);
[p2Plus,~,myP2Plus]   = ddmpdf(tArray,1,d2,dl,x2,z,s);
p2Minus = (1-P1)*p2Minus;
p2Plus  = (1-P1)*p2Plus;
yMinus((dlInd+1):end) = p2Minus((dlInd+1):end);
yPlus((dlInd+1):end)  = p2Plus((dlInd+1):end);

% Diagnostic

% disp('Second Stage')
% trapz(tArray,p2Minus)
% trapz(tArray,p2Plus)

% figure(3)
% plot(tArray,p2Minus,'r')
% hold on
% plot(tArray,p2Plus,'b')
% title('second stage')
% hold off

% figure(4)
% plot(tArray,yMinus,'r')
% hold on
% plot(tArray,yPlus,'b')
% title('overall')
% hold off
% disp('overall')
% trapz(tArray,yMinus)
% trapz(tArray,yPlus)

% trapz(tArray,yMinus)+trapz(tArray,yPlus)

% pause

% End diagnostic


%% Copy code from rt2002
yPlus = cumsum(yPlus);
yMinus = cumsum(yMinus);

nTotalTrials = length(rt);
[val1,df1,q1] = chisq(rt(rtResp>0),tArray,yPlus,nTotalTrials);
[val2,df2,q2] = chisq(rt(rtResp<=0),tArray,yMinus,nTotalTrials);
val = val1+val2;

% figure(3)
% q1 = [0 q1(1:end-1)];
% q2 = [0 q2(1:end-1)];
% df = [df1'; df2'];
% plot(q1,df1,'r-*')
% hold on
% plot(q2,df2,'b-*')
% hold off
% drawnow


