clear, close all, clc
load('fit_8.mat');

nparams = 4;
%%%%%%%%%%%%%%
d = recoveredParameters(:,1);
z = recoveredParameters(:,2);
x0 = recoveredParameters(:,3);
T0 = recoveredParameters(:,4);
cues = [.5 .6 .7 .8];

nTrials = [394 160 111 438 187 181 256 188];

figure(1)
plot(cues,d(1:2:end),'r-*','Linewidth',2.5)
hold on
plot(cues,d(2:2:end),'r--+','Linewidth',2.5)
hold off
legend('Drift (.65)',...
       'Drift (.85)',...
       'Location','NorthWest')
formatfitfigure('drift')
saveas(gcf,'figures/fit_8_drifts.eps','psc2')

figure(2)
plot(cues,z(1:2:end),'g-*','Linewidth',2.5), hold on
plot(cues,z(2:2:end),'g--+','Linewidth',2.5), hold off
legend('Threshold (.65)','Threshold (.85)')
formatfitfigure('threshold')
saveas(gcf,'figures/fit_8_threshold.eps','psc2')


figure(3)
bar(cues,[nTrials(1:2:end); nTrials(2:2:end)]')
legend('Percept = .65','Percept = .85')
formatfitfigure('nTrials')
saveas(gcf,'figures/fit_8_ntrials.eps','psc2')

figure(4)
bar(cues,[finalChiSq(1:2:end) finalChiSq(2:2:end)])
legend('Percept = .65','Percept = .85')
formatfitfigure('chisq')
saveas(gcf,'figures/fit_8_chisq.eps','psc2')

figure(5)
plot(cues,x0(1:2:end),'m-*','Linewidth',2.5), hold on
plot(cues,x0(2:2:end),'m--+','Linewidth',2.5), hold off
legend('x0 (.65)','x0 (.85)')
formatfitfigure('x0')
saveas(gcf,'figures/fit_8_x0.eps','psc2')

figure(6)
plot(cues,T0(1:2:end),'k-*','Linewidth',2.5), hold on
plot(cues,T0(2:2:end),'k--+','Linewidth',2.5), hold off
legend('Nondecision (.65)','Nondecision (.85)')
formatfitfigure('T0')
saveas(gcf,'figures/fit_8_T0.eps','psc2')

set(1,'Position',[50 690 560 420])
set(2,'Position',[630 690 560 420])
set(3,'Position',[50 185 560 420])
set(4,'Position',[630 185 560 420])
set(5,'Position',[1200 690 560 420])
set(6,'Position',[1200 185 560 420])

fprintf(1,'\n\n*** Writing table to fit_8_table.txt ***\n\n\n')
fid = fopen('fit_8_table.txt','w');


totalAIC = 0;
totalBIC = 0;
for k = 1:8


    aic = finalChiSq(k) + 2*nparams;
    bic = finalChiSq(k) + nparams*log(nTrials(k));

    totalAIC = totalAIC + aic;
    totalBIC = totalBIC + bic;

    fprintf(fid,'\\hline ');
    fprintf(fid,'$%2.1f$ / $%2.1f$ -- ',fitSettings(k,1),fitSettings(k,2));
    fprintf(fid,'$%2.1f$ & ',fitSettings(k,3));
    fprintf(fid,'$%4.1f$ & ',finalChiSq(k));
    fprintf(fid,'$%3.3d$ & ',nTrials(k));
    fprintf(fid,'$%4.1f$ & ',aic);
    fprintf(fid,'$%4.1f$ & ',bic);
    fprintf(fid,'$%4.4f$ & ',d(k));
    fprintf(fid,'$%4.4f$ & ',z(k));
    fprintf(fid,'$%4.4f$ & ',x0(k));
    fprintf(fid,'$%4.4f$ '  ,T0(k));
    fprintf(fid,'\\\\\n');
end
fclose(fid);

sum(finalChiSq)
totalAIC
totalBIC