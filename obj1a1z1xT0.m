% 4 params: drift, threshold, x0, T0

function val = obj1a1z1xT0(x,rt,rtResp)
a = [x(1) x(1)];
z = [x(2) x(2)];
dl = [0 2.5]; % doesn't matter
x0 = x(3); 
T0 = x(4);
% Fixed (experimentally set) values
s = [1 1];
tFinal = max(rt) + 3;
val = rt2002(rt,rtResp,a,s,z,x0,1,dl,tFinal,T0);
