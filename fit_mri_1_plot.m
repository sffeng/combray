clear, close all, clc
load('fit_6b.mat');

nparams = 5;
%%%%%%%%%%%%%%
d1 = recoveredParameters(:,1);
d2 = recoveredParameters(:,2);
z = recoveredParameters(:,3);
D = recoveredParameters(:,4);
T0 = recoveredParameters(:,5);
cues = [.5 .6 .7 .8];


figure(1)
plot(cues,d1,'r-*','Linewidth',2.5)
hold on
plot(cues,d2,'b-*','Linewidth',2.5)
hold off
legend('Cue drift','Perceptual drift','Location','NorthWest')
formatfitfigure('drift')
saveas(gcf,'figures/fit_6b_drifts.eps','psc2')

figure(2)
plot(cues,z,'g-*','Linewidth',2.5)
formatfitfigure('threshold')
saveas(gcf,'figures/fit_6b_threshold.eps','psc2')

figure(3)
bar(cues,nTrials)
formatfitfigure('nTrials')
saveas(gcf,'figures/fit_6b_ntrials.eps','psc2')

figure(4)
bar(cues,finalChiSq)
formatfitfigure('chisq')
saveas(gcf,'figures/fit_6b_chisq.eps','psc2')

figure(5)
plot(cues,D,'m-*','Linewidth',2.5)
formatfitfigure('dl')
saveas(gcf,'figures/fit_6b_deadline.eps','psc2')

figure(6)
plot(cues,T0,'k-*','Linewidth',2.5)
formatfitfigure('T0')
saveas(gcf,'figures/fit_6b_T0.eps','psc2')


set(1,'Position',[50 690 560 420])
set(2,'Position',[630 690 560 420])
set(3,'Position',[50 185 560 420])
set(4,'Position',[630 185 560 420])
set(5,'Position',[1200 690 560 420])
set(6,'Position',[1200 185 560 420])

fprintf(1,'\n\n*** Writing table to fit_6b_table.txt ***\n\n\n')
fid = fopen('fit_6b_table.txt','w');

for k = 1:4


    aic = finalChiSq(k) + 2*nparams;
    bic = finalChiSq(k) + nparams*log(nTrials(k));

    fprintf(fid,'\\hline ');
    fprintf(fid,'$%2.1f$ / $%2.1f$ & ',cues(k),1-cues(k));
    fprintf(fid,'$%4.1f$ & ',finalChiSq(k));
    fprintf(fid,'$%3.3d$ & ',nTrials(k));
    fprintf(fid,'$%4.1f$ & ',aic);
    fprintf(fid,'$%4.1f$ & ',bic);
    fprintf(fid,'$%4.4f$ & ',d1(k));
    fprintf(fid,'$%4.4f$ & ',d2(k));
    fprintf(fid,'$%4.4f$ & ',z(k));
    fprintf(fid,'$%4.4f$ & ',D(k));
    fprintf(fid,'$%4.4f$ '  ,T0(k));
    fprintf(fid,'\\\\\n');
end
fclose(fid);