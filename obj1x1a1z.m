% objective function for fitting three parameters for a two stage DDM:
% one initial condition, one drift for perceptual period, one
% threshold, using rt2002.
%
% 5 Nov 2014 fengman@gmail.com

function val = obj1x1a1z(x,rt,rtResp)
x0 = x(1); 
a = [0 x(2)]; % assume zero drift for cue period
z = [x(3) x(3)];

% Fixed (experimentally set) values
s = [1 1];
dl = [0 2.5];
tFinal = max(rt) + 3;
val = rt2002(rt,rtResp,a,s,z,x0,1,dl,tFinal);
