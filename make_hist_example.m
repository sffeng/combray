
clear
close all

a = [-.05 .52];
s = [1 1];        
th = [1.2 1.2];
x0dist = 1;        
dl = [0 2.00];
tFinal = 5;
x0 = .05;

tic
[tArray,~,yPlus,yMinus] = multistage_ddm_fpt_dist(...
    a,s,th,x0,x0dist,dl,tFinal);

toc

for jj = 1:length(yPlus)-1
    yy(jj) = (yPlus(jj+1)-yPlus(jj))/(tArray(jj+1)-tArray(jj));
end

% dt = tArray(3)-tArray(2);
 tt = tArray(2:end);
% yy = diff(yPlus)/dt;

plot(tt,yy/trapz(tt,yy),'r','Linewidth',2)
drawnow


return

