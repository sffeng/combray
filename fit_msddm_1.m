clear, close all

dat = load('collatedData');
dat = dat.collatedData;

%% Genetic algorithm options
lb = [-.2 -.2 2 .05 -1  0]; %d1 d2 dl z x0 T0
ub = [3.0 3.0 10 5.0  1  .5];
nVars = length(lb);
opts = gaoptimset('UseParallel','always','Display','diagnose',...
                  'Generations',100*nVars,...
                  'PopulationSize',12*nVars);
% impose abs(x0) \leq z
conA = [0 0 0 -1 1 0;
        0 0 0 -1 -1 0];
conB = [0;
        0];

%% Arrays setup
cueArray = [.5 .6 .7 .8];
cohArray = [.65 .85];
dlArray = [4 6 8];
nFits = length(cueArray(:,1))*length(cohArray)*length(dlArray);

% initialize
recoveredParameters = nan(nFits,length(ub));
nTrialArray = nan(nFits,1);
finalChiSq = nan(nFits,1);
exitFlags = nan(nFits,1);
fitSettings = nan(nFits,3);

runIter = 1;
for jj = 1:length(cueArray)
    for kk = 1:length(cohArray)
        for ll = 1:length(dlArray)

            % Unwrap cue/coh/dl
            cue = cueArray(jj);
            coh = cohArray(kk);
            dl  = dlArray(ll);

            %% Grab data and nTrials %%
            D = dat{jj,kk,ll};
            rt = D(:,1);
            resp = D(:,2);
            nTrials = length(rt);

            % Progress output
            disp(['Using ' num2str(nTrials) ' trials for'...
                  ' cue: ' num2str(cue) ...
                  ' coh: ' num2str(coh) ...
                  ' deadline: ' num2str(dl)]);

            % Deadline clamp
            lb(3) = dl+.75;
            ub(3) = dl+.75;

            % Random seed for reproducibility
            rng(19850604,'twister');

            % Gentic algorithm for fitting
            [recoveredParameters(runIter,:),...
             finalChiSq(runIter), ...
             exitFlags(runIter)  ] = ...
                ga(@(x) obj2a1d1z1xT0(x,rt,resp), ...
                   length(lb), conA,conB,...
                   [],[], lb,ub,[],opts);
            

            % Save for bookkeeping
            fitSettings(runIter,:) = [cue coh dl];
            nTrialArray(runIter) = nTrials;

            % Save
            save('fit_msddm_1.mat',...
                 'recoveredParameters',...
                 'nTrialArray',...
                 'finalChiSq',...
                 'exitFlags',...
                 'fitSettings');
            
            runIter = runIter + 1;
        end
    end
end
