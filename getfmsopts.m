function opts = getfmsopts
opts = optimset('fminsearch');
opts = optimset(opts,'Display','final');
opts = optimset(opts,'TolFun',1e-8);
opts = optimset(opts,'TolX',1e-8);
opts = optimset(opts,'MaxFunEvals',1000*5);
opts = optimset(opts,'MaxIter',500*5);
