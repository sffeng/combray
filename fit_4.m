% Runs the "round 4" fits, which is more of a sanity check.  How does
% a 1-stage DDM do? We have one overall drift, and one overall
% threshoold for this 2 parameter fit, and again assume x0=0.  As
% always, we use trials pooled across subjects for each of the four
% cues: .5, .4/.6, .3/.7, .2/.8.  Uses fminsearch along with RT2002
% chisq fitting.
%
% fengman@gmail.com

clear
close all

cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
recoveredParameters = nan(4,2);
nTrialArray = nan(4,1);
finalChiSq = nan(4,1);

initialX = [.16 2.0]; %[a z]
fmsopts = getfmsopts;
fmsopts = optimset(fmsopts,'Display','iter');

for k = 1:length(cueArray(:,1))
    
    cues = cueArray(k,:);
    
    [tr] = gettesttrials([1:9 12:16],cues);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues)])
    
    [recoveredParameters(k,:),finalChiSq(k)] = ...
        fminsearch(@(x) obj1a1z(x,rt,resp),initialX,fmsopts);

    nTrialArray(k) = nTrials;
    recoveredParameters
end
nTrials = nTrialArray;

save('fit_4.mat','recoveredParameters','nTrials','finalChiSq')