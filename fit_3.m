% Runs the "round 3" fits.  Fitting initial condition, drift for
% perceptual period, and one threshold over both periods, resulting in
% 3 parameters fit. Using trials pooled across subjects for each of
% the four cues: .5, .4/.6, .3/.7, .2/.8.  Uses fminsearch along with
% RT2002 chisq fitting.
%
% After running this, see fit_3_plot to see output.
%
% fengman@gmail.com

clear
close all

cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
recoveredParameters = nan(4,3);
nTrialArray = nan(4,1);
finalChiSq = nan(4,1);

initialX = [.16 .16 2.0]; %[a1 a2 z]
fmsopts = getfmsopts;
fmsopts = optimset(fmsopts,'Display','iter');

for k = 1:length(cueArray(:,1))
    
    cues = cueArray(k,:);
    
    [tr] = gettesttrials([1:9 12:16],cues);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues)])
    
    [recoveredParameters(k,:),finalChiSq(k)] = ...
        fminsearch(@(x) obj1x1a1z(x,rt,resp),initialX,fmsopts);

    nTrialArray(k) = nTrials;
    recoveredParameters
end
nTrials = nTrialArray;

save('fit_3.mat','recoveredParameters','nTrials','finalChiSq')