clear
close all

subjects = [1 2 5 7 8 9 13 18 20 21 22 27 28 30 31 32 34 ...
            39 40 42 43];
cues = [.3 .7];
percept = [.85];
[tr] = gettesttrials(subjects,cues,0,percept);
rt = tr(:,1);
resp = tr(:,2);

I = find(rt<.2);
rt(I) = [];
resp(I) = [];


rtCor = rt(resp>0);
[N,X] = hist(rtCor,nbins(rtCor)+10);
dx = X(2)-X(1);
NN = cumsum(N);
acc = sum(resp>0)/length(resp);
NN = NN/NN(end)*acc;
%bar(X,NN,'b')
figure(1)
bar(X,N/trapz(X,N),'b')

%% Empirical distribution function
figure(2)
[ff,xx,flo,fup] = ecdf(rtCor,'alpha',.05,'bounds','on');
h1 = stairs(xx,ff);


%% Run msddm code, pick a set of fitted values for display
a = [.2656 3.3655];
s = [1 1];        
th = [1.6 2.9062];
x0dist = 1;        
dl = [0 1.8];
tFinal = 5;
x0 = 0.004359;
T0 = .2;
[tArray,~,yPlus,yMinus] = multistage_ddm_fpt_dist(...
    a,s,th,x0,x0dist,dl,tFinal);
figure(2)
hold on
h2 = plot(tArray+T0,yPlus/yPlus(end),'r','Linewidth',2);
stairs(xx,flo,'b:');
stairs(xx,fup,'b:');

for jj = 1:length(yPlus)-1
    yy(jj) = (yPlus(jj+1)-yPlus(jj))/(tArray(jj+1)-tArray(jj));
end

% dt = tArray(3)-tArray(2);
 tt = tArray(2:end);
% yy = diff(yPlus)/dt;
figure(1)
hold on
plot(tt+T0,yy/trapz(tt,yy),'r','Linewidth',2)
drawnow


[val,df] = rt2002(rt, resp, a,s,th,x0,x0dist, ...
                                  dl,tFinal,T0);



figure(1)
title('Fit of reaction times','FontSize',18,'FontWeight','bold')
axis([0 5 0 2])
xlabel('Reaction time (sec)','FontSize',14)
ylabel('Frequency','FontSize',14)
legend('Experimental','2-stage DDM')
saveas(gcf,'figures/rt_fit.eps','psc2')
saveas(gcf,'figures/rt_fit.fig','fig')

figure(2)
title('Fit of reaction time CDF','FontSize',18,'FontWeight','bold')
xlabel('Reaction time (sec)','FontSize',14)
ylabel('','FontSize',14)
legend([h1 h2],'Empirical distribution function','2-stage DDM CDF','Location','NorthWest')
saveas(gcf,'figures/rtDF_fit.eps','psc2')
saveas(gcf,'figures/rtDF_fit.fig','fig')

return

