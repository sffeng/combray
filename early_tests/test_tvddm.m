t = linspace(0,1,1000);
A = ones(1,length(t));
z = 1;
t0 = 0;
x0 = -.4;
c = 1;

[resp,rt,X] = tvddm(t,A,z,t0,x0,c);
grey = [.64,.64,.64];
plot(t,X,'Color',grey)
rt
resp