pv = linspace(0,1);
p = pv;
q = 1-p;
myentropy = -q.*log(q) - p.*log(p);
plot(pv,myentropy)