function [resp,rt,X] = tvddm(tArray, driftArray, threshold, t0, ...
                               initialX, sigma)

% unwrap for easier typing
tv = tArray;  % must have equal spacing
A = driftArray;  % for each point in tv
z = threshold;
x0 = initialX;
c = sigma;  % std of noice
nT = length(tArray);

% set simulation parameters
R = 4; % Multiplicative factor for brownian motion time resolution

% unwrapped simulation parameters
dt = (tv(2)-tv(1))/R; %browninan dt
Dt = tv(2)-tv(1);  % sim dt

%Generate brownian paths
dW = sqrt(dt)*randn(1,nT*R);

% em method
X = nan(1,nT);
Xtemp = x0;
for j = 1:nT
    Winc = sum(dW(R*(j-1)+1:R*j));
    Xtemp = Xtemp + A(j)*Dt + c*Winc;
    X(j) = Xtemp;
    if abs(Xtemp) > z
        rt = tv(j);
        resp = sign(Xtemp);
        return
    end
end
resp = nan;
rt = nan;