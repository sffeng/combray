% Code used to produce plots for p6 presentation on 12 Mar 2014
%
% To choose between serial/parallel set isserial to 1/0.
%
% Samuel Feng (fengman@gmail.com)

function p6plots

isserial = 0;

if isserial
    m2 = 0;
else
    m2 = .75;
end

%% Experimental parameters
fraccue = .500;  %sec
iti = .250; 
vstim = 1.000;

%% tvddm Simulation parameters
tEnd = fraccue+iti+vstim;
pon = fraccue+iti;
tArray = 0:.001:tEnd;
z = 1.3;
t0 = 0;
x0 = 0;
c = 1;

% setup memory/perceptual signals
memsig = esig(tArray,.75,m2,fraccue+iti);
persig = esig(tArray,0,.5,fraccue+iti);
A = memsig+persig;


%% Graphic generation

set(0,'defaultlinelinewidth',3)

% signals (i.e. drift rates)
figure(1), hold on
h(1) = plot(tArray,memsig,'c');
h(2) = plot(tArray,persig,'m');
pur = [120, 91, 169]/255;
h(3) = plot(tArray,A,'Color',pur,'linewidth',4);
%hold off
title('Drift rates (evidence signals)','fontsize',18)
xlabel('Time (sec)','fontsize',18)
ylabel('Evidence','fontsize',18)

% sample paths
%figure(2), hold on
nPaths = 15;
XX = nan(nPaths,length(tArray));
for k = 1:nPaths
    [resp,rt,XX(k,:)] = tvddm(tArray,A,z,t0,x0,c);
end
grey = [.64,.64,.64];
hhh = plot(tArray,XX,'Color',grey,'linewidth',.2)
h(4) = hhh(1);
plot([0 tEnd],[z z],'b')
plot([0 tEnd],-[z z],'b')
title('Sample paths','fontsize',18)
xlabel('Time (sec)','fontsize',18)
ylabel('Evidence','fontsize',18)

plot([pon pon],[-10 10],'k','linewidth',2)
axis([0 tEnd -z*1.1 z*1.1])

h(1) = plot(tArray,memsig,'c');
h(2) = plot(tArray,persig,'m');
pur = [120, 91, 169]/255;
h(3) = plot(tArray,A,'Color',pur,'linewidth',4);

legend(h,'Memory','Perceptual','Total Signal','Sample Paths')
hold off
saveas(gcf,'paths.png','png')


% RT distributions
figure(3), hold on
nTrials = 10000;
rts = nan(1,nTrials);
resps = nan(1,nTrials);
parfor k = 1:nTrials
    [resps(k),rts(k),~] = tvddm(tArray,A,z,t0,x0,c);
end

rt1 = rts(resps==1);
N1 = histn(rt1,tEnd,0);
xx = tEnd/N1:tEnd/N1:tEnd;
xx = linspace(0,tEnd,N1);
dx = tEnd/N1;
xx = dx/2:dx:tEnd;
[n1,x1] = hist(rt1,xx);
plot(x1,n1 * N1 / (tEnd  * length(rt1)),'b')

rt2 = rts(resps==-1);


N2 = histn(rt2,tEnd,0);
xx = tEnd/N2:tEnd/N2:tEnd;
xx = linspace(0,tEnd,N2);
dx = tEnd/N2;
xx = dx/2:dx:tEnd;

[n2,x2] = hist(rt2,xx);
plot(x2,n2 * N2 / (tEnd  * length(rt2)),'r')
xlabel('Time (sec)','fontsize',18)
legend('Top Boundary','Bottom Boundary')
ax = axis;
line([pon pon],[ax(3) ax(4)],'Color','k')
hold off
saveas(gcf,'distributions.png','png')


function N = histn(y,b,a)
n = length(y);
N = (b-a)*n^(1/3) / (2*iqr(y));

function y = esig(tv,v1,v2,tchange)
d = v2-v1;
y = heaviside(tv-tchange)*d+v1;