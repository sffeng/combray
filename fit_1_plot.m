
%% the fitted values, but it was the quickest way to hack out a
%% figure.  DO NOT USE THIS IN THE FUTURE -- Sam

clear, close all, clc
load('fit_1.mat');
d1 = recoveredParameters(:,1);
d2 = recoveredParameters(:,2);
z = recoveredParameters(:,3);


% d1 = [.0892 .1251 .2409 .7968];
% d2 = [.8808 1.0886 .772 -.1232];
% z = [1.5762 1.6076 1.6430 1.6191];
cues = [1 2];  % actual these are test phases


figure(1)
plot(cues,d1,'r-*','Linewidth',2.5)
hold on
plot(cues,d2,'b-*','Linewidth',2.5)
hold off
axis([0.9 2.1 -.2 1.2])
title('Fitted drift values')
xlabel('Test phase')
ylabel('Fitted drift rate')
legend('Cue drift','Perceptual drift','Location','NorthEast')
formatfigurefonts;
saveas(gcf,'figures/fit_1_drifts.eps','psc2')

figure(2)
plot(cues,z,'g-*','Linewidth',2.5)
axis([0.9 2.1 1 2])
title('Fits of threshold (fixed throughout cue and stim)')
xlabel('Test phase')
ylabel('Fitted threshold z')
formatfigurefonts
saveas(gcf,'figures/fit_1_threshold.eps','psc2')

figure(3)
bar(cues,nTrials)
title('Number of (pooled) trials for each fit')
xlabel('Test Phase')
ylabel('Number of trials')
formatfigurefonts
saveas(gcf,'figures/fit_1_ntrials.eps','psc2')

figure(4)
bar(cues,finalChiSq)
title('Final Chi-squared value of fitted parameters')
xlabel('Test Phase')
ylabel('Chi-squared')
formatfigurefonts
saveas(gcf,'figures/fit_1_chisq.eps','psc2')

set(1,'Position',[50 690 560 420])
set(2,'Position',[630 690 560 420])
set(3,'Position',[50 185 560 420])
set(4,'Position',[630 185 560 420])

fprintf(1,'\n\n*** Writing table to fit_1_table.txt ***\n\n\n')
fid = fopen('fit_1_table.txt','w');

for k = 1:2
    
    nparams = 3;
    aic = finalChiSq(k) + 2*nparams;
    bic = finalChiSq(k) + nparams*log(nTrials(k));

    fprintf(fid,'\\hline ');
    fprintf(fid,'$%1d$  & ',cues(k));
    fprintf(fid,'$%4.1f$ & ',finalChiSq(k));
    fprintf(fid,'$%3.3d$ & ',nTrials(k));
    fprintf(fid,'$%4.1f$ & ',aic);
    fprintf(fid,'$%4.1f$ & ',bic);
    fprintf(fid,'$%4.4f$ & ',d1(k));
    fprintf(fid,'$%4.4f$ & ',d2(k));
    fprintf(fid,'$%4.4f$ ',z(k));
    fprintf(fid,'\\\\\n');

end
fclose(fid);
