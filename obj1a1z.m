% objective function for fitting two parameters for a two stage DDM:
% one drift for perceptual period, one
% threshold, using rt2002.
%
% 5 Nov 2014 fengman@gmail.com

function val = obj1a1z(x,rt,rtResp)
a = [x(1) x(1)]; % assume ONE drift
z = [x(2) x(2)]; % and ONE threshold

% Fixed (experimentally set) values
x0 = 0; % unbiased
s = [1 1];
dl = [0 2.5];
tFinal = max(rt) + 3;
val = rt2002(rt,rtResp,a,s,z,x0,1,dl,tFinal);
