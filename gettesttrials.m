% gettesttrials.m
%
% Returns 2d array of test trial data given subjects and cues as
% inputs. Assumes trials 101:180, 281:360 correspond to the two test
% blocks.  
%

function [tr] = gettesttrials(subjArray, cueArray,testblock, pArray, ...
                                         datdir,dlArray)
if nargin < 3
    testblock = 0;
end

if nargin < 4
    pArray = [.65 .85];
end

if nargin < 5
    datdir = 'data/';
end

if nargin < 6
    dlArray = [4 6 8];
end
    
tr = [];
testInds = [101:180 281:360];

if testblock==1
    testInds = [101:180];
elseif testblock==2
    testInds = [281:360];
end


for subj = subjArray
    A = plotSubj(subj,0,0,0,-1,datdir);
    if length(A(:,1))==180
        testInds = 101:180;
    else
        testInds = [101:180 281:360];
    end
    
    cue = A(testInds,1);
    testtrials = A(testInds,2:3);
    percept = A(testInds,6);
    dl = A(testInds,9);

    isCue = ismember(cue,cueArray);
    isPercept = ismember(percept,pArray);
    isDl = ismember(dl,dlArray);

    tr = [tr; testtrials(isCue & isPercept & isDl,:)];
end

% Remove rows with NaNs
nanInd = sum(isnan(tr),2)>0;
tr(nanInd,:) = [];
