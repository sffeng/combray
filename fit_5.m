% Runs the "round 5" fits, fitting two driftrates, one threshold, and
% the deadline for a two stage ddm. As always, trials are pooled
% across subjects for each of the four cues: .5, .4/.6, .3/.7, .2/.8.
% Uses fminsearch along with RT2002 chisq fitting.
%
% fengman@gmail.com

clear
close all

cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
recoveredParameters = nan(4,4);
nTrialArray = nan(4,1);
finalChiSq = nan(4,1);

XX = load('fit_2.mat');
XX = XX.recoveredParameters;

fmsopts = getfmsopts;
fmsopts = optimset(fmsopts,'Display','iter');

for k = 1:length(cueArray(:,1))
    
    initialX = [XX(k,:) 2.5]; %[a1 a2 z deadline]    
    
    cues = cueArray(k,:);

    [tr] = gettesttrials([1:9 12:16],cues);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues)])
    
    [recoveredParameters(k,:),finalChiSq(k)] = ...
        fminsearch(@(x) obj2a1z1D(x,rt,resp),initialX,fmsopts);

    nTrialArray(k) = nTrials;
    recoveredParameters
end
nTrials = nTrialArray;

save('fit_5.mat','recoveredParameters','nTrials','finalChiSq')