subjs =  [7 10:14 17 18 20 22:24 26:30 33:34 36];
nTrials = 0;
for subjIdx = 1:length(subjs);
    pr{subjs(subjIdx)} = plotSubj(subjs(subjIdx), 0, 0, 0, -1,'mri_data/');
    trialFilter = [pr{subjs(subjIdx)}(:,10)==2 & ~ ...
                   isnan(pr{subjs(subjIdx)}(:,2))]; 
    nTrials = nTrials + sum(trialFilter);
end