% Runs the "round 1" fits, first reported to collaborators on 23 Oct
% 2014.  Fitting two driftrates and one threshold for a two stage ddm
% process, for all test period 1's and then all test period 2's.
% Sort of a silly thing to do, mainly executed to test stability of
% codes. Uses fminsearch along with RT2002 chisq fitting.
%
% fengman@gmail.com
%
% Remarks:
%
% Fits differ slightly from email correspondence sent to AB and MA
% on 23 Oct because algorithm has been better tuned.  Chi-sq values
% reported here are better than those reported on 23 Oct 2014.

clear
close all

cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
cues = cueArray(:);
recoveredParameters = nan(2,3);
nTrialArray = nan(2,1);
finalChiSq = nan(2,1);

initialX = [.16 .16 2.0]; %[a1 a2 z]
fmsopts = getfmsopts;
fmsopts = optimset(fmsopts,'Display','iter');
%% First test period
[tr] = gettesttrials([1:9 12:16],cues,1);
rt = tr(:,1); resp = tr(:,2);
nTrials = length(rt);
disp(['Using ' num2str(nTrials) ' trials for Test Period 1'])

[recoveredParameters(1,:),finalChiSq(1)] = ...
    fminsearch(@(x) obj2a1z(x,rt,resp),initialX,fmsopts);

nTrialArray(1) = nTrials;
recoveredParameters

%% Second test period
[tr] = gettesttrials([1:9 12:16],cues,2);
rt = tr(:,1); resp = tr(:,2);
nTrials = length(rt);
disp(['Using ' num2str(nTrials) ' trials for Test Period 2'])

[recoveredParameters(2,:),finalChiSq(2)] = ...
    fminsearch(@(x) obj2a1z(x,rt,resp),initialX,fmsopts);

nTrialArray(2) = nTrials;
recoveredParameters

%% Save fits
nTrials = nTrialArray;
save('fit_1.mat','recoveredParameters','nTrials','finalChiSq')