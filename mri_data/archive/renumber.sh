# Renumber behav data to match mri data 10.02.2015
# if < 10, add 0 prefix

foreach oldsubnum ( 09 08 07 06 05 04 03 02 01 )
    setenv newsubnum `expr $oldsubnum + 6`
    echo $newsubnum | setenv nn `wc -c` ; expr $nn = 2 && setenv newsubnum 0$newsubnum
    echo Moving subject $oldsubnum to $newsubnum

    foreach oldsubfn (`ls *$oldsubnum*`)
        setenv newsubfn `echo $oldsubfn | sed s/$oldsubnum/$newsubnum/`
        echo mv $oldsubfn $newsubfn
        mv $oldsubfn $newsubfn
    end
end

