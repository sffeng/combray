function makeSLmaps(subj, varargin)
% function makeSLmaps(subj, ...)
%
% OPTIONS:
%   verbose      (true)
%   veryverbose  (false) Output for every voxel
%   dryrun       (false) Don't write outputs
%   filterErrors (true)  Excise error trials
%
% Output files are named slmap_subj_face{A,B,C,D}_sessN_imgN and scene{A,B,C,D}_sessN
%

%XXX   datafile    (computeRSA_results_subjnumber.mat)
opts = parse_args(varargin, 'verbose', true, ...
                            'veryverbose', false, ...
                            'dryrun', false, ...
                            'filterErrors', true);

% qsub
if (ischar(subj))
    subj = str2num(subj);
end

%% Constants
dataDir     = ['/jukebox/cohen/aaronmb/combray/mri'];
resultsDir  = fullfile(dataDir, 'RSAresults');
behavDir    = fullfile(dataDir, 'behav');
baseDir     = dataDir;
mapNames    = {'same', 'other'};

faceNames   = {'F3-16.jpg', 'F4-18.jpg', 'F1-8.jpg', 'F2-11.jpg'};
faceStrs    = ['A':'D'];
sceneNames  = {'S3-12.jpg', 'S4-20.jpg', 'S1-15.jpg', 'S2-19.jpg'};
sceneStrs   = ['A':'D'];

resultsDataFn = ['computeRSA_results_' ...
                 num2str(subj, '%.2d') '.mat'];
resultsDataFn = fullfile(resultsDir, resultsDataFn);

if (opts.verbose)
    disp(['makeSLmaps: Loading ' resultsDataFn]);
end

resultsData = load(resultsDataFn);

behavdat    = loadSubj(subj, behavDir);

if (opts.verbose)
    disp(['makeSLmaps: Subject ' ...
          num2str(subj, '%.2d') ...
          ' Loading data from ' resultsDataFn]);
end

subjDir     = dir(fullfile(dataDir, 'combray_*'));
subjDir     = subjDir(subj).name;

rtFeatDir   = dir(fullfile(dataDir, subjDir, '*isorespTrain*feat'));
numSess     = length(rtFeatDir);                                            % XXX: In the future, automatically discard failed sessions? Also, discard incorrect responses?

% For each session, make a same and a diff map for each of the four images
for sessIdx = 1:numSess;
    thisFeatDir  = rtFeatDir(sessIdx).name;
    blockIdx     = behavdat.pracrec{sessIdx}(1).block;

    % Load dims for later ind2sub parsing (not needed for later subjects that have the dims saved)
    if (iscell(resultsData.dataDims))
        dataDims = resultsData.dataDims{sessIdx};
        centerPoints = resultsData.centerPoints{sessIdx};
    else
        maskFullFile = fullfile(dataDir, subjDir, thisFeatDir, 'mask.nii');
        if (opts.verbose)
            disp(['makeSLmaps: Loading ' maskFullFile ...
                  ', session ' num2str(sessIdx) ...
                  ', block ' num2str(blockIdx)]);
        end
        [maskData maskHdr]  = cbiReadNifti(maskFullFile);
        centerPoints = find(maskData);
        dataDims     = size(maskData);
    end

    if (opts.verbose)
        disp(['makeSLmaps: Writing ' num2str(length(centerPoints)) ...
              ' voxels, for maps of size ' num2str(dataDims)]);
    end

    numImages    = size(resultsData.results{sessIdx}{1}, 1);
    errorTrials  = find(arrayfun(@(x)(x.image ~= x.resp), [behavdat.pracrec{sessIdx}]));
    numTrials    = size(resultsData.results{sessIdx}{1}, 2);

    % Precompute the error trials for each image
    for imgIdx = 1:numImages;
        imageTrials{imgIdx} = find(arrayfun(@(x)(x.image==imgIdx), [behavdat.pracrec{sessIdx}]));

        trialFilter{imgIdx} = ones(1, length(imageTrials{imgIdx}));
        if (opts.filterErrors)
            [lia,locb]          = ismember(errorTrials, imageTrials{imgIdx});

            locb                = locb(locb~=0);
            if (~isempty(locb))
                trialFilter{imgIdx}(locb) = 0;
            end
            trialFilter{imgIdx} = find(trialFilter{imgIdx});

            if (opts.verbose)
                disp(['makeSLmaps: Session ' num2str(sessIdx) ...
                      ', image ' num2str(imgIdx) ...
                      ': Allowing ' num2str(length(trialFilter{imgIdx})) ...
                      '/' num2str(length(imageTrials{imgIdx})) ...
                      ' trials: ' num2str(imageTrials{imgIdx}(trialFilter{imgIdx}))]);
            end
        end
    end

    % For each image in this session, make a map of average same-trial correlations and average other-trial correlations
    parfor imgIdx = 1:numImages;
%    for imgIdx = 1:numImages;      % for debugging (ha get it, for)
        imageCategory = [imgIdx > 2];               % 0 faces, 1 scenes
        otherImage    = (imageCategory * 2) + ((imgIdx-(imageCategory*2))>1 * -1) + ((imgIdx-(imageCategory*2))<2 * 1);     % Pair 1 w 2 and 3 w 4

        for comparisonIdx = 1:size(resultsData.results{sessIdx}{1}, 3);                  % same = 1; other = 2
            outmap        = zeros(dataDims);

            mapname       = mapNames{comparisonIdx};

            for cpIdx = 1:length(centerPoints);
                [cx cy cz] = ind2sub(dataDims, centerPoints(cpIdx));
                % Average across all trials and pairs, first fisher-transforming the corrs
                if (comparisonIdx == 1 || imgIdx < otherImage)
                    theseCorrs         = squeeze(resultsData.results{sessIdx}{cpIdx}(imgIdx, trialFilter{imgIdx}, comparisonIdx, trialFilter{otherImage}));
                else
                    % Corr from this image to other is same as corr from other image to this one
                    theseCorrs         = squeeze(resultsData.results{sessIdx}{cpIdx}(otherImage, trialFilter{otherImage}, comparisonIdx, trialFilter{imgIdx}));
                end

                outmap(cx, cy, cz) = nanmean(atanh(theseCorrs(:)));

                if (opts.veryverbose)
                    disp(['makeSLmaps: Map ' num2str(sessIdx) ...
                          '_' num2str(imgIdx) ...
                          '_' mapname ...
                          ', voxel ' num2str(cpIdx) ...
                          '(' num2str([cx cy cz]) ...
                          '): transformed mean: ' num2str(outmap(cx, cy, cz), ...
                                                          '%.4f') ...
                          ', raw: ' num2str([theseCorrs(:)]')]);
                end
            end  % for cpIdx

            if (opts.dryrun)
                continue;
            end

            imgStr = '';
            if (imgIdx < 3)
                nameIdx = imgIdx + (blockIdx-1)*2;      % Second block uses faces 3, 4
                fIdx    = find(cellfun(@(x)(~isempty(x)), strfind(faceNames, behavdat.params.faceNames{nameIdx})));
                imgStr  = ['face' faceStrs(fIdx)];
            else
                nameIdx = imgIdx - (mod(blockIdx,2))*2; % First block uses scenes 1, 2
                sIdx    = find(cellfun(@(x)(~isempty(x)), strfind(sceneNames, behavdat.params.sceneNames{nameIdx})));
                imgStr  = ['scene' sceneStrs(sIdx)];
            end

            mapFn   = ['slmap_' ...
                        num2str(subj, '%.2d') ...
                        '_' imgStr ...
                        '_' num2str(sessIdx) ...
                        '_' num2str(imgIdx) ...
                        '_' mapname ...
                        '.nii'];

            funcFn      = fullfile(dataDir, subjDir, thisFeatDir, 'reg', 'example_func.nii');
            system(['gzip -dc ' funcFn '.gz >' funcFn]);
            funcNii     = load_untouch_nii(funcFn);
            funcNii.img = outmap;

            save_untouch_nii(funcNii, mapFn);
            system(['gzip -9 ' mapFn '; mv ' mapFn '.gz SLmaps/']);
        end	% for comparisonIdx

   end % for imgIdx

end % for sessIdx

% Clean up
trimFailedSessions(subj);
