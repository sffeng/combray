#!/bin/sh
#
# Reorient the structural image
#

STARTDIR=`pwd`
EXPTNAME=combray
DATADIR=/jukebox/cohen/aaronmb/$EXPTNAME/mri

if [ "$#" -lt 1 ]; then
    echo $0 "<subj_number>"
    exit
fi

SUBJ=$1

SUBJDIR=`ls -d1 $DATADIR/$EXPTNAME_*|head -$SUBJ|tail -1`

# /jukebox/cohen/aaronmb/$EXPTNAME/mri/_SUBJDIR_/_STRUCTFN_
STRUCTRAW=`ls -1 $SUBJDIR/*MPRAGE*nii.gz|tail -1|sed 's/.nii.gz//' | sed 's/ //g'`
STRUCTFN=`echo $STRUCTRAW | cut -d '/' -f 8 | sed 's/ //g'`

cd $SUBJDIR

echo "Reorienting $SUBJDIR - $STRUCTFN"

BACKUPIDX=`ls -1 $STRUCTFN|wc -l`
BACKUPIDX=`expr $BACKUPIDX + 1`
BACKUPFN="$STRUCTFN.original.$BACKUPIDX.nii.gz"
echo "Backing up to $BACKUPFN..."
cp "$STRUCTFN.nii.gz" "$BACKUPFN"
echo "Reorienting..."
fslreorient2std "$BACKUPFN" "$STRUCTFN.nii.gz"

cd $STARTDIR

# Next:
#	1. Run Feat
#	2. cd feat/reg dir
#	3. ln -s /jukebox/cohen/aaronmb/ctxbandit/ROIs/$SUBJ/mri/funcppa_005.nii .
#	4. applywarp

# echo "Applying warp..."
# applywarp --ref=standard --in=funcppa_005 --warp=highres2standard_warp --premat=example_func2highres.mat --out=funcppa_005_reoriented
# gzip -df funcppa_005_reoriented.nii.gz
# cp funcppa_005_reoriented.nii /jukebox/cohen/aaronmb/ctxbandit/ROIs/$SUBJ/mri/
# /opt/pkg/FSL/fsl-5.0.8/bin/bet $STRUCTFN "$STRUCTFN"_brain -f 0.5 -g 0 -s
# gzip -df "$STRUCTFN"_brain.nii.gz
