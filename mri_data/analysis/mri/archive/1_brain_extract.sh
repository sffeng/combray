#!/bin/sh
#
# Extract the brain
#

STARTDIR=`pwd`
EXPTNAME=combray
DATADIR=/jukebox/cohen/aaronmb/$EXPTNAME/mri

if [ "$#" -lt 1 ]; then
    echo $0 "<subj_number> [thresh] [gradient]"
    exit
fi

EXTRACTTHRESH=0.5
if [ "$#" -gt 1 ]; then
    EXTRACTTHRESH=$2
fi

GRADIENTFLAG=0
if [ "$#" -gt 2 ]; then
    GRADIENTFLAG=$3
fi

SUBJ=$1

SUBJDIR=`ls -d1 $DATADIR/$EXPTNAME_*|head -$SUBJ|tail -1`

# /jukebox/cohen/aaronmb/combray/mri/_SUBJDIR_/_STRUCTFN_
STRUCTRAW=`ls -1 $SUBJDIR/*MPRAGE*nii.gz|sed 's/.nii.gz//'`
STRUCTFN=`echo $STRUCTRAW | cut -d '/' -f 6 | sed 's/ //g'`

cd $SUBJDIR/$STRUCTDIR

rm -f *_brain*
echo "Extracting $SUBJDIR - $STRUCTFN:"

STRUCTBRAIN=`echo $STRUCTFN.brain|sed 's/.b/_b/'`
echo $STRUCTBRAIN

echo "Extracting brain with threshold $EXTRACTTHRESH and gradient $GRADIENTFLAG..."
/opt/pkg/FSL/fsl-5.0.8/bin/bet $STRUCTFN $STRUCTBRAIN -f $EXTRACTTHRESH -g $GRADIENTFLAG -s
# Save what we did
rm -f extract_params
echo "/opt/pkg/FSL/fsl-5.0.8/bin/bet $STRUCTFN $STRUCTBRAIN -f $EXTRACTTHRESH -g $GRADIENTFLAG -s" > extract_params
# gzip -df $STRUCTBRAIN.nii.gz

echo "CHECK THE RESULT FOR EXTRANEOUS TISSUE AND BONE (might need to re-run at a higher threshold)"
fslview $STRUCTBRAIN
cd $STARTDIR
