#!/bin/tcsh
#
# Visualize the individual anatomical warped to template
#

# XXX: Load these from a common file
setenv EXPTNAME combray
setenv DATADIR /jukebox/cohen/aaronmb/$EXPTNAME/mri

setenv NUMSUBJ `ls -1d $DATADIR/$EXPTNAME_*|wc -l`
echo "Checking $NUMSUBJ subjects..."

foreach SUBJDIR ( `ls -1d $DATADIR/$EXPTNAME_*` )
    echo $SUBJDIR

    setenv STDFN `ls -1 $SUBJDIR/*localizer*/*feat/reg/highres2standard.nii.gz | tail -1`
    echo $STDFN
    fslview /opt/pkg/FSL/fsl-5.0.8/data/standard/MNI152_T1_2mm_brain.nii.gz $STDFN -t 0.5 -l "Red-Yellow"

    echo Look good? [Y/n]
    set inputline=$<

    if (`echo $inputline|tr "[:upper:]" "[:lower:]"` == "n")
        echo Skipping $SUBJDIR...
        continue;
    else
        touch $DATADIR/anat_aligned
        echo $SUBJDIR >> $DATADIR/anat_aligned
    end
end

