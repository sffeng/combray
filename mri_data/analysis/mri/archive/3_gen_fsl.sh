#!/bin/sh
#
# Generate an FSL configuration file for each EPI
#

EXPTNAME=combray
DATADIR=/jukebox/cohen/aaronmb/$EXPTNAME/mri
EPITAG=TR1sRes25iso				# What string identifies an EPI?
STRUCTTAG=MPRAGE

if [ "$#" -lt 1 ]; then
    echo $0 "<subj_number>"
    exit
fi

SUBJ=$1

SUBJDIR=`ls -d1 $DATADIR/$EXPTNAME_*|head -$SUBJ|tail -1`

# XXX: Check that dir exists
echo "Processing $SUBJ: $SUBJDIR..."

# Get the name of the structural (take the last one if there are multiple)
# Struct files are of the form: /jukebox/cohen/aaronmb/$EXPTNAME/mri/_SUBJDIR_/_STRUCTFN_
STRUCTFN=`ls -1 $SUBJDIR/|fgrep $STRUCTTAG|fgrep _brain|tail -1`
echo "Using structural $STRUCTFN..."

# Loop through EPIs, generate a separate config file for each.
# EPI files are of the form: /jukebox/cohen/aaronmb/$EXPTNAME/mri/_SUBJDIR_/_EPIFN_
EPIIDX=0
for EPIFN in [`ls -1 $SUBJDIR/|fgrep $EPITAG`]; do
    echo "Processing EPI $EPIIDX: $EPIFN..."
    echo "cat preproc_template.fsf | sed 's/_SUBJDIR_/$SUBJDIR/g' | sed 's/_STRUCTFN_/$STRUCTFN/g' | sed 's/_EPIFN_/$EPIFN/g' > preproc_$EPIIDX.fsf"
#    cat preproc_template.fsf | sed 's/_SUBJDIR_/$SUBJDIR/g' | sed 's/_STRUCTFN_/$STRUCTFN/g' | sed 's/_EPIFN_/$EPIFN/g' > preproc_$EPIIDX.fsf
    EPIIDX=`expr $EPIIDX + 1`
done

echo "$SUBJDIR: Processed $EPIIDX EPI files."
