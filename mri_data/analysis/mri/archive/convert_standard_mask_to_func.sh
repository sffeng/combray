#!/bin/sh
#
# Create inverse warp and apply it to an anatomical ROI, generating a mask in individual space
#

if [ "$#" -lt 1 ];then
   echo "$0 <subj>"
   exit
fi

source sharedvars 2>/dev/null

# Filename of the mask in standard space
ANATMASKNAME=bilat_vtmask
# Filename of the output mask in individual space
FUNCMASKNAME=bilat_vtmask_warped

SORTKEY=`echo $ROIDIR/ | tr -d -c '/' | wc -c`
SORTKEY=`expr $SORTKEY + 1`

    SUBJDATADIR=`ls -1d $DATADIR/$EXPTNAME* | head -$SUBJ | tail -1`
    SUBJROIDIR=$ROIDIR/$SUBJ/mri

    # Check for the localizer FEAT dir.
    TESTLOCDIR=0
    test -d $SUBJDATADIR/*localizer*feat && TESTLOCDIR=1

    if [ "$TESTLOCDIR" -eq 1 ]; then
        echo "$OUTPUTPREFIX Subject $SUBJ - $SUBJDATADIR"
        # Change to subject directory
        cd $SUBJDATADIR/*localizer*feat/reg

	rm -f "$FUNCMASKNAME*"

        # Copy standard-space mask to the reg directory
        cp $SUBJROIDIR/$ANATMASKNAME.nii .

        echo "$OUTPUTPREFIX Generating inverse warp..."
        # Generate inverse warp from standard to functional
        invwarp --ref=highres --warp=highres2standard_warp --out=highres2standard_warp_inv

        echo "$OUTPUTPREFIX Applying inverse warp..."
        # Apply the new inverse transform
#	applywarp --ref=example_func --in=mask_in_standard_space --warp=highres2standard_warp_inv --postmat=highres2example_func.mat --out=mask_in_functional_space
        applywarp --ref=example_func --in=$ANATMASKNAME --warp=highres2standard_warp_inv --postmat=highres2example_func.mat --out=$FUNCMASKNAME

        echo "$OUTPUTPREFIX Re-binarizing..."
        # Re-binarize (because applywarp creates a weighted average of neighboring voxels)
        fslmaths $FUNCMASKNAME -thr 0.5 -bin $FUNCMASKNAME

        echo "$OUTPUTPREFIX Copying..."
        # Copy new mask to ROI directory
        cp $FUNCMASKNAME.nii.gz $SUBJROIDIR

	# Clean up
	rm -f $ANATMASKNAME.nii

        cd $STARTDIR
    fi

