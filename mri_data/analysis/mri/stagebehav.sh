#!/bin/sh
#
# Copy behavioral data processed from laptop over to jukebox
#
# stagebehav <subj>
#

if [ "$#" -lt 1 ];then
   echo "Usage: $0 <subj>"
   exit
fi

source sharedvars 2>/dev/null

SUBJ=$1
STARTDIR=`pwd`
BASEDIR=/jukebox/cohen/aaronmb/combray/mri
SRCDIR=~/matlab/combray

echo "$OUTPUTPREFIX Running motion exclusion..."
pni_matlab -quiet -r motion_excl $SUBJ
mv -f badvols_$SUBJ.mat $BASEDIR/badvols/

echo "$OUTPUTPREFIX Moving staged behavioral data..."
cd $SRCDIR/behav
mv -f localizer_regs_$SUBJ.mat $BASEDIR/localizer_regs/

mv -f * $BASEDIR/behav

cd $STARTDIR
