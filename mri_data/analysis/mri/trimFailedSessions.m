function trimFailedSessions(submat, varargin)
% function trimFailedSessions([submat], ...)
%
% Should be called from within makeSLmaps
%
% OPTIONS:
%   submat  - if not specified, all subjs in slMapDir
%   verbose (true)
%

opts = parse_args(varargin, 'verbose', true, ...
                            'exclflags', {});

dataDir     = ['/jukebox/cohen/aaronmb/combray/mri'];
behavDir    = fullfile(dataDir, 'behav');
slMapDir    = ['~/matlab/combray/SLmaps'];
failedDir   = fullfile(slMapDir, 'failed_sessions');

if (nargin < 1)
    submat     = dir(fullfile(slMapDir, 'slmap_*'));
    submat     = cellfun(@(x)(str2num(x(7:8))), {submat.name}, 'UniformOutput', false);
    submat     = unique([submat{:}]);
end

for subj = submat;
    behavdat      = loadSubj(subj, behavDir);

    % First, excise maps from trainRespMap runs that failed criterion (first of N same-block)
    badRuns   = cellfun(@(x)(x(1).block), behavdat.pracrec);
    badRuns   = find(diff(badRuns)==0);     % This is automatically indexed as repeatIdx-1, because diff output is of length(badRuns)-1

    if (opts.verbose)
        disp(['trimFailedSessions: Subject ', num2str(subj, '%.2d') ...
              ', removing ' num2str(length(badRuns)) ...
              ' runs: ' num2str(badRuns)]);
    end

    for brIdx = 1:length(badRuns);
        system(['mv ' slMapDir ...
                '/slmap_' num2str(subj, '%.2d') ...
                '_face?_' num2str(badRuns(brIdx)) ...
                '_* ' failedDir]);

        system(['mv ' slMapDir ...
                '/slmap_' num2str(subj, '%.2d') ...
                '_scene?_' num2str(badRuns(brIdx)) ...
                '_* ' failedDir]);
    end % for brIdx

    % Next, other exclusions?
end
