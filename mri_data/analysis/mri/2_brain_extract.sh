#!/bin/sh
#
# Extract the brain
#

if [ "$#" -lt 1 ]; then
    echo $0 "<subj_number> [thresh] [gradient]"
    exit
fi

source sharedvars 2>/dev/null

EXTRACTTHRESH=0.5
if [ "$#" -gt 1 ]; then
    EXTRACTTHRESH=$2
fi

GRADIENTFLAG=0
if [ "$#" -gt 2 ]; then
    GRADIENTFLAG=$3
fi

# e.g. /jukebox/cohen/aaronmb/combray/mri/_SUBJDIR_/_STRUCTFN_
STRUCTRAW=`ls -1 $SUBJDIR/*MPRAGE*nii.gz|grep -v '/co.-'|fgrep -v 'original'|fgrep -v '/o' | tail -1|sed 's/.nii.gz//' | sed 's/ //g'`
CUTFIELD=`echo $STRUCTRAW | tr -d -c '/' |wc -c`
CUTFIELD=`expr $CUTFIELD + 1`
STRUCTFN=`echo $STRUCTRAW | cut -d '/' -f $CUTFIELD| sed 's/ //g'`

cd $SUBJDIR

rm -f *_brain*
echo "$OUTPUTPREFIX Extracting $SUBJDIR - $STRUCTFN:"

STRUCTBRAIN=`echo $STRUCTFN.brain|sed 's/.b/_b/'`
echo "$OUTPUTPREFIX Outputting extracted brain to $STRUCTBRAIN"

echo "$OUTPUTPREFIX Extracting brain with threshold $EXTRACTTHRESH and gradient $GRADIENTFLAG..."
$FSLDIR/bin/bet $STRUCTFN $STRUCTBRAIN -f $EXTRACTTHRESH -g $GRADIENTFLAG -s
# Save what we did
rm -f extract_params
echo "$FSLDIR/bin/bet $STRUCTFN $STRUCTBRAIN -f $EXTRACTTHRESH -g $GRADIENTFLAG -s" > extract_params

echo "$OUTPUTPREFIX CHECK THE RESULT FOR EXTRANEOUS TISSUE AND BONE (might need to re-run at a higher threshold)"
fslview $STRUCTFN.nii.gz $STRUCTBRAIN.nii.gz -t 0.5 -l "Red-Yellow"
cd $STARTDIR
