#!/bin/sh
#

if [ "$#" -lt 1 ];then
   echo "$0 <subj>"
   exit
fi

source sharedvars 2>/dev/null

echo "$OUTPUTPREFIX Running compute_RSA..."
submit_long -l cores=8 pni_matlab -multi -r compute_RSA $SUBJ mailme

echo "$OUTPUTPREFIX Running run_mvpa..."
submit_short pni_matlab -r run_mvpa $SUBJ mailme
