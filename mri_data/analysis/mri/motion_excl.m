function [vols] = motion_excl(subj, varargin)
% function [vols] = motion_excl(subj, ...)
%
% Parse the position estimates from the scanner to exclude runs or volumes for excessive motion.
%
% Doesn't differentiate between target data and other EPIs (scout, non-target data, etc)
%
% Right now this uses static, linear single-trial and single-run thresholds.
% In the future, this could do something more intelligent with acceleration, multiple volumes, across runs, angular motion.
% Default thresholds are entirely arbitrary; if you identify something more intelligent, please let me know.
%
% INPUTS
%   subj        - (integer) Subject number; indexes from the data directory
%
% OPTIONS
%   dataDir     - (string)  Data directory [default {'/jukebox/cohen/aaronmb/combray/mri'}].
%   runs        - (array)   Which runs to parse [default -1, or all runs with position data in qadir].
%   qadir       - (string)  Name of QA directory [default [dataDir '/QA']].
%   volThresh   - (integer) Threshold displacement (in mm) from previous volume at which to exclude a given volume (default 0.25).
%   runThresh   - (integer) Threshold displacement (in mm) from startpoint at which to exclude an entire run (default 3).
%   verbose     - (integer) Print status messages (default 1: summary only; 0 = none, 2 = everything).
%   saveout     - (boolean) Saves 'badvols_XX.mat' (default true)
%
% RETURNS
%   vols        - (cellarray)   An array of volumes excluded (1 or 0). One array for each run.
%
% Aaron Bornstein <aaronmb@princeton.edu>, around August 2015: Refactored args, updated to use PNI file formats.
% Aaron Bornstein <aaronb@cns.nyu.edu>, around March 2009: Initial.
%

[opts] = parse_args(varargin, ...
                              'dataDir', {'/jukebox/cohen/aaronmb/combray/mri'}, ...
                              'runs', -1, ...
                              'qadir', {'QA'}, ...
                              'volThresh', 0.25, ...
                              'runThresh', 3, ...
                              'verbose', 1, ...
                              'saveout', true);

%%
% Constants
axislabels = ['xyz'];

% cmdline
if (ischar(subj))
    subj = str2num(subj);
end

%%
% XXX: Check inputs
if (iscellstr(opts.dataDir))
    opts.dataDir = opts.dataDir{:};
end
if (iscellstr(opts.qadir))
    opts.qadir = opts.qadir{:};
end

subjDir = dir([opts.dataDir '/combray_*']);
%subjDir = subjDir(arrayfun(@(x)(x.name(1)~='.'), ...
%                  subjDir));	% Exclude '.' and '..'

subjDir(subj).name

if (opts.verbose > 0)
    disp(['Subject ' subjDir(subj).name]);
end

subjDir = [opts.dataDir '/' subjDir(subj).name];

%%
% Load up the directories to parse.
if (-1 == opts.runs)
    opts.runs = [];

    qasubdirs = dir([subjDir '/' opts.qadir]);
    qasubdirs = qasubdirs(arrayfun(@(x)(x.name(1)~='.'), ...
                          qasubdirs));	% Exclude '.' and '..'

    for qsdidx = 1:length(qasubdirs);
        xposfile = dir([subjDir '/' opts.qadir ...
                        '/' qasubdirs(qsdidx).name ...
                        '/qa_data_cmassx.txt']);
        if (~isempty(xposfile))
            % NB: We are careful to store and report directory indexes by their name, not their index number.
            % This avoids reporting, e.g., run 10 as directory 2
            opts.runs = [str2num(qasubdirs(qsdidx).name) opts.runs];
        end
    end
end

if (opts.verbose > 1)
    disp(['... Parsing runs ' num2str(opts.runs)]);
end

vols    = cell(length(opts.runs), 1);

opts.runs = sort(opts.runs);

for runIdx = opts.runs;
    runDir = [subjDir '/' ...
              opts.qadir '/' num2str(runIdx)];

    for axisIdx = 1:3;
        runData = dlmread([runDir '/qa_data_cmass' axislabels(axisIdx) '.txt'], '\t', 1, 0);
        if (axisIdx == 1)
            if (opts.verbose > 1)
                disp(['... Run ' num2str(runIdx) ...
                      ': ' num2str(length(runData)) ...
                      ' total volumes.']);
            end

            vols{runIdx} = zeros(length(runData), 1);
        end

        startPos    = runData(1, 2);

        % Instantaneous motion
        idx = [0 ; runData(2:end, 2) - runData(1:end-1, 2)];
	% NB: This reshape is kinda silly.
        vols{runIdx} = vols{runIdx} | reshape([abs(idx) > opts.volThresh],size(vols{runIdx}));


        % Total motion
        maxPosDisp = max(runData(2:end, 2) - startPos);
        maxNegDisp = min(runData(2:end, 2) - startPos);

        if ((maxPosDisp  > opts.runThresh) || ...
            (maxNegDisp  > opts.runThresh))
            vols{runIdx} = ones(1, length(runData));
        end

        if (opts.verbose > 1)
            disp(['... ' axislabels(axisIdx) ' direction:']);
            disp(['... Volumes with above-threshold displacement: ' num2str(sum(abs(idx) > opts.volThresh))]);
            disp(['... Maximum displacement: ' num2str(max(abs(idx)))]);
            disp(['... Peak total displacement: ' num2str(max([maxPosDisp maxNegDisp]))]);
        end
    end

    if (opts.verbose > 0)
        exclVols = sum(vols{runIdx});
        if (exclVols == length(vols{runIdx}))
            exclVols = 'ALL';
        else
            exclVols = [num2str(exclVols) '/' num2str(length(vols{runIdx}))];
        end
        disp(['... Run ' num2str(runIdx) ...
              ': Exclude ' exclVols ...
              ' volumes.']);
    end
end

if (opts.saveout)
    save(['badvols_' ...
          num2str(subj, '%.2d') '.mat'], 'vols');
end
