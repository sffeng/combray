#!/bin/sh
#
# Check the functional-space mask we just created in step 6.
#

if [ "$#" -lt 1 ];then
   echo "$0 <subj> [anat mask name]"
   exit
fi

source sharedvars 2>/dev/null

# Filename of the mask in anatomical space
ANATMASKNAME=bilat_vtmask
# Filename of the output mask in individual functional space
FUNCMASKNAME=bilat_vtmask_warped

if [ "$#" -gt 1 ]; then
    ANATMASKNAME=`echo $2 | sed 's/.nii//'`
    FUNCMASKNAME=`basename $ANATMASKNAME`
    FUNCMASKNAME=$FUNCMASKNAME\_warped

    echo "$OUTPUTPREFIX Using anat $ANATMASKNAME and func $FUNCMASKNAME..."
fi

SUBJDATADIR=`ls -1d $DATADIR/$EXPTNAME* | head -$SUBJ | tail -1`

SUBJROIDIR=$ROIDIR/$SUBJ/mri

# Check for the localizer FEAT dir.
TESTLOCDIR=0
test -d $SUBJDATADIR/*localizer*feat && TESTLOCDIR=1

if [ "$TESTLOCDIR" -eq 1 ]; then
	echo "$OUTPUTPREFIX Subject $SUBJ - $SUBJDATADIR"

	fslview $SUBJDATADIR/*localizer*feat/filtered_func_data.nii $SUBJROIDIR/$FUNCMASKNAME -t 0.5 -l "Red-Yellow"
fi

