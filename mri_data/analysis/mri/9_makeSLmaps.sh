#!/bin/sh
#

if [ "$#" -lt 1 ];then
   echo "$0 <subj>"
   exit
fi

source sharedvars 2>/dev/null

echo "$OUTPUTPREFIX Running makeSLmaps..."
submit_short pni_matlab -r makeSLmaps $SUBJ mailme
