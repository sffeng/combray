#!/bin/csh
#
# Run once for all subjects. Will only start the process for subjects not already run (checking anatrun)
#

source sharedvars >&/dev/null

setenv SUBJECTS_DIR $DATADIR
source ${FREESURFER_HOME}/SetUpFreeSurfer.csh

setenv EXCLUDEFILE $DATADIR/anatrun
touch $EXCLUDEFILE

cd $ROIDIR

setenv SUBJNM 0
foreach fn (`find $DATADIR -name '*MPRAGE*mmipat.nii.gz' | grep -v '.o.-1-1MPRAGE'|fgrep -v 'original'|fgrep -v 'brain'|grep -v 'conformed'|sort`)
    setenv SUBJNM `expr $SUBJNM + 1`
    fgrep $fn $EXCLUDEFILE >/dev/null && echo "$OUTPUTPREFIX Skipping $SUBJNM..." && continue

    echo "$OUTPUTPREFIX Submitting subject $SUBJNM - $fn..."
    submit_verylong recon-all -i $fn -s $SUBJNM -all mailme
    echo submitted subject $SUBJNM - $fn >> $EXCLUDEFILE
end

cd $STARTDIR
