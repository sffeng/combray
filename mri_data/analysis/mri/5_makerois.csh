#!/bin/csh
#
# Make subject-space ROIs
#
source sharedvars >&/dev/null

setenv EXCLUDEFILE $DATADIR/roirun
touch $EXCLUDEFILE

setenv SORTFIELD `echo $ROIDIR/ | tr -d -c '/' |wc -c`
setenv SORTFIELD `expr $SORTFIELD + 1`

cd $ROIDIR

setenv SUBJNM 0

foreach fn (`find $ROIDIR -maxdepth 1 -mindepth 1 -type d | sort -t / -k $SORTFIELD -n`)
    setenv SUBJNM `expr $SUBJNM + 1`
    setenv SUBJNM `basename $fn`
    fgrep $fn $EXCLUDEFILE >/dev/null && echo "$OUTPUTPREFIX Skipping $SUBJNM..." && continue

    cd $SUBJNM/mri

    echo "$OUTPUTPREFIX Making subject-space ROIs for subject $SUBJNM - $SUBJDIR..."
    mri_convert aparc.a2009s+aseg.mgz aparc2009.nii

    echo "...L_fusiformGyr"
    fslmaths aparc2009.nii -thr 11121 -uthr 11121 -bin L_fusiformGyr.nii
    echo "...R_fusiformGyr"
    fslmaths aparc2009.nii -thr 12121 -uthr 12121 -bin R_fusiformGyr.nii
    echo "...L_lingualGyr"
    fslmaths aparc2009.nii -thr 11122 -uthr 11122 -bin L_lingualGyr.nii
    echo "...R_lingualGyr"
    fslmaths aparc2009.nii -thr 12122 -uthr 12122 -bin R_lingualGyr.nii
    echo "...L_parahipCtx"
    fslmaths aparc2009.nii -thr 11123 -uthr 11123 -bin L_parahipCtx.nii
    echo "...R_parahipCtx"
    fslmaths aparc2009.nii -thr 12123 -uthr 12123 -bin R_parahipCtx.nii

    # NB: fslmaths outputs .nii.gz, 3dcalc outputs .nii
    echo "...L_vtmask"
    rm -f L_vtmask.nii
    3dcalc -a L_lingualGyr.nii.gz -b L_parahipCtx.nii.gz -c L_fusiformGyr.nii.gz -expr 'a+b+c' -prefix L_vtmask.nii
    echo "...R_vtmask"
    rm -f R_vtmask.nii
    3dcalc -a R_lingualGyr.nii.gz -b R_parahipCtx.nii.gz -c R_fusiformGyr.nii.gz -expr 'a+b+c' -prefix R_vtmask.nii

    echo "...bilat_vtmask"
    rm -f bilat_vtmask.nii
    3dcalc -a L_vtmask.nii -b R_vtmask.nii -expr 'a+b' -prefix bilat_vtmask.nii

    echo submitted $fn >> $EXCLUDEFILE
    cd ../..
end

cd $STARTDIR
