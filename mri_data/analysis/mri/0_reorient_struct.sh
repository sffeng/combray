#!/bin/sh
#
# Reorient the structural image
#

if [ "$#" -lt 1 ]; then
    echo $0 "<subj_number>"
    exit
fi

source sharedvars 2>/dev/null

# /jukebox/cohen/aaronmb/$EXPTNAME/mri/_SUBJDIR_/_STRUCTFN_
STRUCTRAW=`ls -1 $SUBJDIR/*MPRAGE*nii.gz|grep -v '/co.-'|fgrep -v 'original'|fgrep -v '/o' | tail -1|sed 's/.nii.gz//' | sed 's/ //g'`
CUTFIELD=`echo $STRUCTRAW | tr -d -c '/' |wc -c`
CUTFIELD=`expr $CUTFIELD + 1`
STRUCTFN=`echo $STRUCTRAW | cut -d '/' -f $CUTFIELD | sed 's/ //g'`

cd $SUBJDIR

echo "$OUTPUTPREFIX Reorienting $SUBJDIR - $STRUCTFN..."

BACKUPIDX=`ls -1 $STRUCTFN.original.*.nii.gz|wc -l`
BACKUPIDX=`expr $BACKUPIDX + 1`
BACKUPFN="$STRUCTFN.original.$BACKUPIDX.nii.gz"
echo "$OUTPUTPREFIX Backing up to $BACKUPFN..."
cp "$STRUCTFN.nii.gz" "$BACKUPFN"
echo "$OUTPUTPREFIX Reorienting..."
fslreorient2std "$BACKUPFN" "$STRUCTFN.nii.gz"
echo "$OUTPUTPREFIX Conforming to 256x256x256..."
mri_convert -c "$STRUCTFN.nii.gz" "$STRUCTFN.nii.gz"

cd $STARTDIR
