#!/bin/tcsh
#
# fixnii <newbase>
#
# newbase is an 8-digit number uniquely identifying the subject; e.g. YYMMDD## 14032601
# It will be postfixed with a four-digit number uniquely identifying the scan: 0001, 0002
#


if ($#argv != 1) then
	echo "Usage: fixnii <newbase>"
	exit
endif

setenv postfn $1

foreach fn (`find . -name '*nii.gz'`)
	gzip -d $fn
end

setenv fileidx "0000"
foreach fn (`find . -wholename '*MultiEcho_BOLD/*nii'`)
#    setenv basefn `echo $fn | sed 's/^..ctxbandit_//' | sed 's/\/.*//'`
#    setenv postfn `echo $fn | cut -d '/' -f 3 | cut -d '_' -f 1`
#    setenv lastfn `echo $basefn$postfn | sed 's/-//g'`
#    setenv lastfn `echo $lastfn | cut -c 7-18`

    setenv fileidx `expr $fileidx + 1`

    setenv lastfn `echo "$postfn"000"$fileidx"`
    setenv dirfn `echo $fn | cut -d '/' -f 1-2`
    echo "mv $fn $dirfn/$lastfn""_uncor.nii"

    mv $fn $dirfn/$lastfn""_uncor.nii 
end

foreach fn (`find . -name '*MPRAGE1mm.nii'`)
	setenv prefn `echo $fn | cut -d '/' -f 1-2`
	setenv postfn `echo $fn | cut -d '/' -f 3 | sed 's/-1MPRAGE1mm.nii/+t1mprage.nii/'`
	echo "mv $fn $prefn/$postfn"
	mv $fn $prefn/$postfn
end
