#!/bin/sh
#
# mricopy <rootdir>
#

BASEDIR="/jukebox/cohen/aaronmb/ctxbandit"

if [ $# -lt 1 ]; then
    echo "mricopy <rootdir>"
    exit
fi

cp -R $1/NII $BASEDIR/$1
olddir=`pwd`
cd $BASEDIR/$1
NEWBASE=`echo $1 | sed 's/.*_//' | sed 's/-.*//'`

pni_matlab -r renameMRI
~/matlab/fixnii.sh $NEWBASE
cd ..
cp -R $1 /fastscratch/aaronmb/rawdata_mvpa/
cd $olddir
