targDir  = './';
targList = dir([targDir '*.nii.gz']);

% Rename EPIs to be in subdir XX+MultiEcho_BOLD
% Rename anat to end in XX+t1mprage
% All else just 'number+' remaining

for ddi = 1:length(targList);
    dashIdx = strfind(targList(ddi).name, '-');
    idxNum  = targList(ddi).name(1:dashIdx-1);
    if (length(idxNum) == 1)
        idxNum = ['0' idxNum];
    end
    strippedStr = targList(ddi).name(dashIdx+1:end);
    strippedStr = basename(strippedStr, '.nii.gz');

%    newPostfix = '.nii.gz';
    newPostfix = '';

    if (strfind(targList(ddi).name, 'epi'))
        newPostfix = ['+MultiEcho_BOLD'];
    elseif (strfind(targList(ddi).name, 'MPRAGE'))
        newPostfix = ['+t1mprage'];
    end


    newTargDir = [idxNum '_' strippedStr newPostfix];
    renamedStr = [newTargDir '/' targList(ddi).name];
    cmdStr = ['mv ' targList(ddi).name ' ' renamedStr];
    disp(cmdStr);

    system(['mkdir ' newTargDir]);
    system(cmdStr);
end
