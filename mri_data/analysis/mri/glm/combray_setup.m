function EXPT = combray_setup(cor, model, analysis)
% function EXPT = combray_setup(cor, model, [analysis])
%

% path to data directory
EXPT.base_dir   = ['/jukebox/cohen/aaronmb/combray/mri'];
EXPT.config_dir = ['/jukebox/cohen/aaronmb/combray/mri'];

% name of analysis (this allows you to group models into different preprocessing pathways)
% opts = plain, mvpa (no smoothing, etc)
if (~exist('analysis', 'var'))
    analysis  = 'plain';
end

EXPT.name = analysis;

% correction type
EXPT.cor = cor;

% variational bayes estimation? (0 = maximum likelihood)
EXPT.vb = 0;

% Take HRF derivatives?
if (strfind(analysis, '_hrfd'))
    hrfd = 2;
end
if (exist('hrfd', 'var'))
    if (hrfd > 1)
       EXPT.hrf = 'hrf (with time and dispersion derivatives)';
    elseif (hrfd == 1)
       EXPT.hrf = 'hrf (with time derivative)';
    end
end

EXPT.SubjectName = dir([EXPT.config_dir '/combray_*']);
EXPT.SubjectName = {EXPT.SubjectName(:).name};

EXPT.behavfn = dir([EXPT.config_dir '/behavior/*.mat']);
EXPT.behavfn = {EXPT.behavfn(:).name};

EXPT.subjects = [1:length(EXPT.SubjectName)];
EXPT.subjects = [7:length(EXPT.SubjectName)];

% Number of EPI runs per subject
EXPT.nRuns                   = 3;

% These are not different from the defaults, but stored for consistency and easy reference
% XXX: Change these for MVPA/localizer
% EXPT.preproc.options.globals  = 1;
EXPT.preproc.options.globals  = 0;		    % NB changed for FSL
EXPT.preproc.options.realign  = 1;
EXPT.preproc.options.coreg    = 0;                   % XXX?
EXPT.preproc.realign.fwhm     = 5;
EXPT.preproc.realign.graphics = 0;
EXPT.preproc.smooth.fwhm      = 5;                   % full-width half-maximum (mm) of Gaussian smoothing kernel applied after all other preprocessing steps
EXPT.hpf                      = 128;                 % high-pass filter
EXPT.SPM.xVi.form             = 'AR(1)';             % correct for serial correlations ('none', 'AR(1)')
%EXPT.mrp                      = true;                % include movement parameters
EXPT.mrp                      = false;                % NB changed for FSL


% These are different from the defaults
EXPT.preproc.realign.rtm     = true;                % Realign to mean EPI
EXPT.preproc.realign.quality = 1;                   % Quality vs. speed tradeoff (1 = highest quality)
EXPT.preproc.segment.coreg   = true;                % Register structural to template
EXPT.preproc.realign.sep     = 3;                   % Separation (mm) between the points sampled in the reference image (smaller -> slower & more accurate) Default: 4
EXPT.spm.preload             = 0;		    % NB changed for FSL

EXPT.TR                      = 1;
EXPT.nSlices                 = 44;
EXPT.version                 = 4;
EXPT.disdaqs                 = 0;                   % Number of discarded acquisitions at the beginning of each run (NB FSL does this for us)

% model type/identifier
EXPT.model = model;

% regressors function (see dawlab_make_regressors)
EXPT.funcname = ['combray_model_' model];

if (strfind(analysis, 'mvpa'))
    EXPT.nRuns                           = 3;

    EXPT.preproc.options.normalize       = 0;       % normalization
    EXPT.preproc.options.norm_structural = 0;       % normalize anatomicals
    EXPT.preproc.options.smooth          = 1;       % smoothing
    EXPT.preproc.options.reslice         = 1;       % final reslice (required for unnormalized)
    EXPT.preproc.smooth.fwhm             = 4;       % full-width half-maximum (mm) of Gaussian smoothing kernel applied after all other preprocessing steps
end

if (strfind(model, 'localizer'))
    EXPT.contrasts{1,1} = ['presxscene'];
    EXPT.contrasts{2,1} = [''];
    EXPT.contrasts{1,2} = ['presxscene'];
    EXPT.contrasts{2,2} = ['presxface|presxscrambled_scene|presxobject'];

    EXPT.contrasts{1,3} = ['presxface'];
    EXPT.contrasts{2,1} = [''];
    EXPT.contrasts{1,2} = ['presxface'];
    EXPT.contrasts{2,2} = ['presxscene|presxscrambled_scene|presxobject'];
elseif (strfind(model, 'main'))
%   EXPT.contrasts = {'choice', 'outcome', 'reward', 'probe', 'rewardxvalue'};
end

EXPT = dawlab_setup(EXPT);
