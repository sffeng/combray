function [names durations onsets pmod] = combray_model_localizer(EXPT, sub, sess)
% function [names durations onsets pmod] = combray_model_localizer(EXPT, [sub, sess])
%
%     names - (cell array) one per set of onsets
% durations - (cell array) length of each item in each set of onsets (0 for stick, n for boxcar)
%    onsets - (cell array) times, in seconds, of each onset, indexed from beginning
%             of first non-discarded TR
%      pmod - structure of cell arrays:
%                name - name of covariate
%               param - array of parameter values at each onset
%
% TODO:
%   Save parameters and code to a separate directory for each run
%
% OPTIONS:
%   _shift  - How many TR to shift (add '_shiftXX' to model name)
%

% Shift onsets by some # in either direction
soi = strfind(EXPT.name, '_shift');
if (soi)
    soi = str2num(EXPT.name(soi+6));
    disp(['Setting onset shift to ' num2str(soi)]);
    soi
else
    soi = 0;
end

subj    = EXPT.SubjectName{sub};
sd      = loadSubj(sub);

localizerOnsets = [sd.localizer.localizerOnsets{:}];
so      = ones(1, length(localizerOnsets));
fo      = 1;                                % first onset
lo      = length(localizerOnsets);          % last onset

% XXX: Remove TRs with excessive motion
if (~isempty(sd.badvols))
    badonsets = zeros(1, length(sd.onsets(fo:lo)));

    otrs      = sd.onsets(fo:lo)-sd.onsets(fo+soi);
    badtrs    = find(sd.badvols{end}) * EXPT.TR;     % XXX specify run

    disp(['Removing ' num2str(length(badtrs)) ...
          ' onsets due to excessive motion.']);

    for bti = 1:length(badtrs);
        badonsets = badonsets | [abs(otrs-badtrs(bti))<(EXPT.TR*2)]; % XXX: * 2 is a param; window of motion to exclude
    end

    badonsets = [zeros(1, length(sd.onsets(1:fo-1))) badonsets zeros(1, length(sd.onsets(lo+1:end)))];

    so = so & ~badonsets;
else
    disp(['NOT skipping onsets with excessive motion.']);
end

trialType = repmat(sd.localizer.blockType', 1, 10);
trialType = trialType';
trialType = trialType(:)';

% filter out repeat trials
repeatTrials = [[sd.localizer.probeOrder{:}]==0];

goodtr       = so & ~repeatTrials;

% LOGICAL: faces
facetr       = [trialType==1] & goodtr';
% LOGICAL: objects
objecttr     = [trialType==2] & goodtr';
% LOGICAL: scenes
scenetr      = [trialType==3] & goodtr';
% LOGICAL: scrambled_scenes
scrambledtr  = [trialType==4] & goodtr';

pmod      = [];

names        = {'pres'};
durations    = repmat({0}, 1, length(names));
onsets       = cell(0);

disp([num2str(sum(facetr)) ' face trials.']);
disp([num2str(sum(objecttr)) ' object trials.']);
disp([num2str(sum(scenetr)) ' scene trials.']);
disp([num2str(sum(scrambledtr)) ' scrambled scene trials.']);

% Construct onset times - the cumulative error of doing this programmatically is less than 0.2s
raw_onsets       = ones(1,10)*1.8;              % 500ms pres + 1.3s ISI
raw_onsets(1)    = raw_onsets(1) + 12;          % 12s IBI
raw_onsets       = repmat(raw_onsets, 12, 1);   % 12 blocks total
raw_onsets       = raw_onsets';
raw_onsets       = raw_onsets(:);
raw_onsets(1)    = 0+(soi*EXPT.TR);             % First pres at 0s
raw_onsets       = cumsum(raw_onsets);

onsets{1}    = raw_onsets;

%% ONSET 1: faces
pmod_face   = zeros(1,length(raw_onsets));
pmod_face(facetr) = 1;
pmod(1).name{1}  = 'face';
pmod(1).param{1} = pmod_face;

%% ONSET 2: objects
pmod_object   = zeros(1,length(raw_onsets));
pmod_object(objecttr) = 1;
pmod(1).name{2}  = 'object';
pmod(1).param{2} = pmod_objects;

%% ONSET 3: scenes
pmod_scene   = zeros(1,length(raw_onsets));
pmod_scene(scenetr) = 1;
pmod(1).name{3}  = 'scene';
pmod(1).param{3} = pmod_scene;

%% ONSET 4: scrambled_scenes
pmod_scrambled   = zeros(1,length(raw_onsets));
pmod_scrambled(scrambledtr) = 1;
pmod(1).name{4}  = 'scrambled_scene';
pmod(1).param{4} = pmod_scrambled;
