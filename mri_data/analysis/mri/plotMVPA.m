function [opts mnrt ARTB mnRTstat perPVals] = plotMVPA(submat, varargin)
% function [opts mnrt ARTB mnRTstat perPVals] = plotMVPA(submat, ...)
%
% args:
%   'TR'                    (1)
%   'scaleFactor'           (1)         Plot 1/x seconds per time bin (1 = plot by seconds, 2 = plot by half-seconds, etc)
%   'plotWindow'            (10)        Plot x seconds post-cue
%   'plotScrambled'         (true)
%   'plotObject'            (true)
%   'doPlot'                (false)
%   'doGroupPlot'           (false)
%   'numConds'              (4)
%   'shiftVols'             (5)
%   'testTrialOffset'       (0)         Offset the starting test trial. Useful for examining early/late etc.
%   'trimLastVolume'        (true)      Don't use the last volume before the ISI, to avoid being polluted by the stimulus
%   'usePerformance'        (false)     Rather than use calibrated perceptual levels, use actual accuracy (XXX: Unimplemented, requires different plotting code)
%   'manuallyExclude'       (false)
%   'excludeSessions        (false)             Exclude individual sessions from subjSummary.sessmat
%   'excludeMotion'         (false)
%   'excludeLearnErrors'    (0) (was 0.3)       Exclude sessions with too many errors in the learning phase (0 to turn off)
%   'excludeSkippedThresh'  (0) (was 0.1)       Exclude sessions with too many skipped trials in learning or test (fraction of either; 0 to turn off)
%   'excludeCalibThresh'    (0) (was 0.1)       Exclude sessions with below-threshold calibration differences (0 to turn off)
%   'useDiffScore'          (false)     For aggregate mem/per plots, use a score that is the difference of same-other, rather than just same
%   'residualBins'          (4)         How many bins to split up residual RTs when plotting effects for poster/paper
%   'zscoreSameEv'          (false)     Z-Score sameEvidence before computing/plotting residual RTs?
%   'useSkippedCorr'        (false)     Use robust correlations
%   'verbose'               (true)
%   'savefigs'              (false)     XXX unimplemented
%   'mvpaDir'               ('/jukebox/cohen/aaronmb/combray/mri/mvpa')
%

[opts] = parse_args(varargin, ...
                              'TR', 1, ...
                              'scaleFactor', 1, ...
                              'plotWindow', 10, ...
                              'plotScrambled', true, ...
                              'plotObject', true, ...
                              'doPlot', false, ...
                              'doGroupPlot', false, ...
                              'numConds', 4, ...
                              'shiftVols', 5, ...
                              'usePerformance', false, ...
                              'trimLastVolume', true, ...
                              'useSkippedCorr', false, ...
                              'manuallyExclude', false, ...
                              'excludeSessions', false, ...
                              'excludeMotion', false, ...
                              'excludeLearnErrors', 0, ...
                              'excludeSkippedThresh', 0, ...
                              'excludeCalibThresh', 0, ...
                              'residualBins', 4, ...
                              'zscoreSameEv', false, ...
                              'useDiffScore', false, ...
                              'testTrialOffset', 0, ...
                              'verbose', true, ...
                              'savefigs', false, ...
                              'mvpaDir', {'/jukebox/cohen/aaronmb/combray/mri/mvpa'});

if (iscell(opts.mvpaDir))
    opts.mvpaDir   = opts.mvpaDir{:};
end

dataDir     = ['/jukebox/cohen/aaronmb/combray/mri'];
behavDir    = fullfile(dataDir, 'behav');

subjSummary     = load(fullfile(behavDir, 'subjSummary.mat'));
sessmat         = subjSummary.sessmat;
subjSummary     = subjSummary.subjSummary;
memLevels       = [0.5 0.6 0.7 0.8];
perLevels       = [0.65 0.85];
cueTime         = 0.75;
numTestTrials   = 80;
numLearnTrials  = 100;
sessTrials      = numLearnTrials + numTestTrials;
memConds        = 4;                    % 0.5, 0.6, 0.7, 0.8

maxSess      = 2;
memSameScore = cell(max(submat), length(memLevels));
perSameScore = cell(max(submat), length(perLevels));
ERTB         = cell(max(submat), 1);
LRTB         = cell(max(submat), 1);

% Translate TRs to time bins
scaleVol = round(opts.TR*opts.scaleFactor);

if (isvector(submat))
    totalObjectEvidence = [];
    totalSceneEvidence  = [];
    totalScramEvidence  = [];

    totalObjectReg      = [];
    totalSceneReg       = [];
    totalScramReg       = [];
end

for subj = submat;
    sd            = loadSubj(subj, behavDir);

    numSess       = length(sd.calibRec);     % XXX: Fix
    sessList      = [1:numSess];

    if (opts.manuallyExclude)
        %% Too many very-fast responses, suggests just responding to cue-motor association
        if (subj == 10)
            sessList = []; % both sessions
        end

        if (subj == 15)
            sessList = 1;
        end
        % Borderline, leave in for now.
        if (0 && subj == 11 || subj == 18)
            sessList = 1;
        end
        if (0 && subj == 15)
            sessList = []; % both sessions
        end

        %% Too few early responses, suggests just waited for flicker and ignored cues entirely
        if (subj == 17 || subj == 20)
            sessList = 1;
        end
        % Borderline, leave in for now.
        if (0 && subj == 20)
            sessList = []; % both sessions
        end

        % Skipped and errors, leave in for now (should be excluded by the option for this)
        if (0 && subj == 9)
            sessList = 1;
        end
    end

    if (opts.excludeSessions)
        sessList = [sessmat(sessmat(:, 1)==subj, :)];
        sessList = sessList(:, 2);
        sessList = sessList';
    end

    if (isempty(sessList))
        % Skip!
        submat(submat==subj) = -1;
        continue;
    end

    faceEvidence  = NaN(numSess, numTestTrials, opts.plotWindow);
    objEvidence   = NaN(numSess, numTestTrials, opts.plotWindow);
    sceneEvidence = NaN(numSess, numTestTrials, opts.plotWindow);
    scramEvidence = NaN(numSess, numTestTrials, opts.plotWindow);

    thisCue = NaN(numSess, numTestTrials);
    thisMem = NaN(numSess, numTestTrials);
    thisPer = NaN(numSess, numTestTrials);
    thisISI = NaN(numSess, numTestTrials);
    thisRT  = NaN(numSess, numTestTrials);
    thisSamples  = NaN(numSess, numTestTrials);

    subjDir = dir(fullfile(dataDir, 'combray_*'));
    subjDir = subjDir(subj).name;
    runEpis = dir(fullfile(dataDir, subjDir, '*isotest.nii.gz'));

    % Because MRI runs come off the scanner numbered in a weird way
    testRunNumbers = sort(cellfun(@(x)(str2num(x)),strtok({runEpis.name},'-')));

    for sessIdx = sessList;
        sessBase  = ((sessIdx-1) * sessTrials) + 1;
        testStart = sessBase + numLearnTrials + opts.testTrialOffset;
        testEnd   = sessBase + numLearnTrials + numTestTrials - 1;      % 101:180, 281:360

        % Calculate mean classifier probability for face (blue), object (green), scene (red) and scrambled_scene (yellow)
        mvpaDir = opts.mvpaDir;
        mvdata  = load(fullfile(mvpaDir, ['mvpa_' num2str(subj, '%.2d') '.mat']));

        acts_test = mvdata.acts_test{sessIdx};

        if (iscell(acts_test))
            % If this is multiple classifiers, take average
            numConds = size(acts_test{1}, 1);
            tmp_test = zeros(numConds, size(acts_test{1}, 2));

            for actsIdx = 1:length(acts_test);
                tmp_test = tmp_test + acts_test{actsIdx};
            end

            tmp_test  = tmp_test/length(acts_test);
            acts_test = tmp_test;
        end

        % Exclude sessions with below-threshold accuracy or too many skipped trials
        if (opts.excludeLearnErrors)
            if ((subjSummary(subj).errorsByPhase{1, sessIdx}+subjSummary(subj).skippedTrials{1, sessIdx}) > (opts.excludeLearnErrors * numLearnTrials))
                if (opts.verbose)
                    disp(['run_mvpa: Subject ' num2str(subj) ...
                          ', excluding session ' num2str(sessIdx) ...
                          ' for too many errors in learn-phase: ' num2str(subjSummary(subj).errorsByPhase{1, sessIdx})]);
                end
                continue;
            end
        end

        if (opts.excludeSkippedThresh)
            if (subjSummary(subj).skippedTrials{1, sessIdx} > (opts.excludeSkippedThresh * numLearnTrials))
                if (opts.verbose)
                    disp(['run_mvpa: Subject ' num2str(subj) ...
                          ', excluding session ' num2str(sessIdx) ...
                          ' for too many skipped learn-phase trials: ' num2str(subjSummary(subj).skippedTrials{1, sessIdx})]);
                end
                continue;
            end

            if (subjSummary(subj).skippedTrials{2, sessIdx} > (opts.excludeSkippedThresh * numTestTrials))
                if (opts.verbose)
                    disp(['run_mvpa: Subject ' num2str(subj) ...
                          ', excluding session ' num2str(sessIdx) ...
                          ' for too many skipped test-phase trials: ' num2str(subjSummary(subj).skippedTrials{2, sessIdx})]);
                end
                continue;
            end
        end

        % Exclude sessions with below-threshold calibration differences
        if (opts.excludeCalibThresh)
            calibLen = sd.calibRec{sessIdx}{1}.trialCount;
            acc = zeros(2, 1);
            for stimIdx = 1:4;
                % perIdx 1 = 0.65, 2 = 0.85
                perIdx      = (sd.calibRec{sessIdx}{stimIdx}.pThreshold == max(perLevels))+1;
                acc(perIdx) = acc(perIdx) + mean(sd.calibRec{sessIdx}{stimIdx}.response(1:calibLen));
            end
            calibDiff = diff(acc/2);

            % Skip this session if below threshold
            if (calibDiff < opts.excludeCalibThresh)
                if (opts.verbose)
                    disp(['run_mvpa: Subject ' num2str(subj) ...
                          ', excluding session ' num2str(sessIdx) ...
                          ' for below-threshold calibration difference: ' num2str(calibDiff)]);
                end
                continue;
            end
        end

        % Excise high-motion volumes for this session
        % XXX: Delete entire trial on which this volume occured? (Might also count towards excluded trials above)
        % XXX: Delete adjacent volumes? (Maybe this should be done in motion_excl)
        if (opts.excludeMotion)
            if (opts.verbose)
                disp(['run_mvpa: Subject ' num2str(subj) ...
                      ', session ' num2str(sessIdx) ...
                      ' excising ' num2str(sum(sd.badvols{testRunNumbers(sessIdx)})) ...
                      ' high-motion volumes.']);
            end
            acts_test(:, sd.badvols{testRunNumbers(sessIdx)}) = NaN;
        end

        %% Plot evidence by TR for entire session (one per session per subject)
        % (this is just a sanitycheck, can turn off)
        if (0 && opts.doPlot)
            aaron_newfig;
            set(gca, 'FontSize', 24);
            set(gca, 'FontWeight', 'demi');
            hold on;

            % XXX: This needs to be updated for if we turn off includeObjects, includeScrambled (not yet relevant cuz we're using anatomical ROIs)
            plot(acts_test(1, :), 'b--', 'LineWidth', 4);       % face

            if (opts.plotObject)
                plot(acts_test(2, :), 'g--', 'LineWidth', 4);   % object
            end

            plot(acts_test(3, :), 'r--', 'LineWidth', 4);       % scene

            if (opts.plotScrambled)
                plot(acts_test(4, :), 'k--', 'LineWidth', 4);   % scrambled
            end

            xlabel('TR');
            ylabel('Classifier evidence');
            title(['Timeseries for subject ' ...
                   num2str(subj, '%.2d'), ...
                   ', session ' num2str(sessIdx)]);

            axis('tight');
        end

        % Select out _cue-locked_ evidence timeseries for each class on each trial
        % XXX: In the future, we want stim-locked and response-locked timeseries as well
        theseTrials = [testStart:testEnd];

        for trialIdx = 1:length(theseTrials);
            % Store trial info
            % XXX: also accuracy?
            thisCue(sessIdx, trialIdx)              = sd.trials(theseTrials(trialIdx)).cue;
            thisMem(sessIdx, trialIdx)              = sd.params.memNoiseLevels(sessIdx, thisCue(sessIdx, trialIdx));
            thisPer(sessIdx, trialIdx)              = sd.calibRec{sessIdx}{thisCue(sessIdx, trialIdx)}.pThreshold;      % NB: Should we use actual calibrated level instead?
            thisISI(sessIdx, trialIdx)              = sd.trials(theseTrials(trialIdx)).ISI;
            thisRT(sessIdx, trialIdx)               = sd.trials(theseTrials(trialIdx)).RT;
            thisSamples(sessIdx, trialIdx)          = sd.observedFraction(theseTrials(trialIdx));                       % NB: Could use observed count as well

            prepad   = [];
            postpad  = [];
            firstIdx = floor(sd.trials(theseTrials(trialIdx)).runTime/opts.TR)+1+opts.shiftVols;
            % Don't grab the last volume to avoid being polluted by stimulus evidence
            if (opts.trimLastVolume)
                thisISI(sessIdx, trialIdx) = thisISI(sessIdx, trialIdx) - 1;
            end

            % Delete evidence after the ISI
            if (thisRT(sessIdx, trialIdx) < cueTime + thisISI(sessIdx, trialIdx))
                % Trim to RT on early response trials
                lastIdx = ceil((sd.trials(theseTrials(trialIdx)).runTime + cueTime + thisRT(sessIdx, trialIdx))/opts.TR);
            else
                lastIdx = ceil((sd.trials(theseTrials(trialIdx)).runTime + cueTime + thisISI(sessIdx, trialIdx))/opts.TR);
            end

            lastIdx = lastIdx + opts.shiftVols;


            % Pad TRs that don't exist
            postpad = NaN(1, opts.plotWindow - (lastIdx-firstIdx) - 1);
            if (lastIdx > size(acts_test, 2))
                % This shouldn't happen but.
                postpad = [];
                lastIdx = size(acts_test, 2);
            end

            faceEvidence(sessIdx, trialIdx, :)       = [prepad acts_test(1, firstIdx:lastIdx) postpad];
            sceneEvidence(sessIdx, trialIdx, :)      = [prepad acts_test(3, firstIdx:lastIdx) postpad];
            if (opts.plotObject)
                objEvidence(sessIdx, trialIdx, :)    = [prepad acts_test(2, firstIdx:lastIdx) postpad];
            end
            if (opts.plotScrambled)
                scramEvidence(sessIdx, trialIdx, :)  = [prepad acts_test(4, firstIdx:lastIdx) postpad];
            end

        end % for trialIdx

    end % for sessIdx

    % Stack sessions on top of each other
    stackedCue = reshape(thisCue, [numSess*size(thisCue, 2) 1]);
    stackedISI = reshape(thisISI, [numSess*size(thisISI, 2) 1]);
    stackedRT  = reshape(thisRT,  [numSess*size(thisRT, 2)  1]);
    stackedMem = reshape(thisMem, [numSess*size(thisMem, 2) 1]);
    stackedPer = reshape(thisPer, [numSess*size(thisPer, 2) 1]);
    stackedSamples = reshape(thisSamples, [numSess*size(thisSamples, 2) 1]);

    faceEvidence    = reshape(faceEvidence, [numSess*size(faceEvidence, 2) size(faceEvidence, 3)]);
    sceneEvidence   = reshape(sceneEvidence, [numSess*size(sceneEvidence, 2) size(sceneEvidence, 3)]);
    objEvidence     = reshape(objEvidence, [numSess*size(objEvidence, 2) size(objEvidence, 3)]);
    scramEvidence   = reshape(scramEvidence, [numSess*size(scramEvidence, 2) size(scramEvidence, 3)]);

    faceTrials  = [stackedCue == 1 | stackedCue == 2];
    sceneTrials = [stackedCue == 3 | stackedCue == 4];

    % Now one row per trial, with K non-NaN elements up to time of response or ISI-1, whichever comes first
    if (opts.useDiffScore)
        sameEvidence   = [faceEvidence(faceTrials, :) ; sceneEvidence(sceneTrials, :)] - [sceneEvidence(faceTrials, :) ; faceEvidence(sceneTrials, :)];
    else
        sameEvidence   = [faceEvidence(faceTrials, :) ; sceneEvidence(sceneTrials, :)];
    end
    otherEvidence  = [faceEvidence(sceneTrials, :) ; sceneEvidence(faceTrials, :)];
    % objEvidence(faceTrials, :) objEvidence(sceneTrials, :) scramEvidence(faceTrials, :) scramEvidence(sceneTrials, :)];

    earlyTrials    = stackedRT <= stackedISI+cueTime;
    lateTrials     = stackedRT > stackedISI+cueTime;
    earlyRTs       = stackedRT(earlyTrials);
    lateRTs        = stackedRT(lateTrials);% -stackedISI(lateTrials);  % NB or not-stackedISI

    sameEarly      = [faceEvidence(earlyTrials & faceTrials, :) ; sceneEvidence(earlyTrials & sceneTrials, :)];
    sameEarly      = nansum(sameEarly')./sum(~isnan(sameEarly'));
    try
        if (opts.useSkippedCorr)
            r              = skipped_correlation(earlyRTs, sameEarly, 0);
            sameEarlyCorr(subj) = r.Pearson;
        else
            throw(MException('VarifyOutput:test', 'test'))
        end
    catch
        r              = corrcoef(earlyRTs, sameEarly);
        sameEarlyCorr(subj) = r(1, 2);
    end
    if (opts.verbose)
        disp(['plotMVPA: Subject ' num2str(subj) ' same-early corr (Pearson) = ' num2str(sameEarlyCorr(subj))]);
    end

    sameLate       = [faceEvidence(lateTrials & faceTrials, :) ; sceneEvidence(lateTrials & sceneTrials, :)];
    sameLate       = nansum(sameLate')./sum(~isnan(sameLate'));
    try
        if (opts.useSkippedCorr)
            r              = skipped_correlation(lateRTs, sameLate, 0);
            sameLateCorr(subj) = r.Pearson;
        else
            throw(MException('VarifyOutput:test', 'test'))
        end
    catch
        r               = corrcoef(lateRTs, sameLate);
        sameLateCorr(subj) = r(1, 2);
    end
    if (opts.verbose)
        disp(['plotMVPA: Subject ' num2str(subj) ' same-late corr (Pearson) = ' num2str(sameLateCorr(subj))]);
    end

    % Take the sum of evidence in each trial, and divide by the # of non-NaN timepoints in that trial.
    sameScore{subj}      = nansum(sameEvidence')./sum(~isnan(sameEvidence'));
    otherScore{subj}     = nansum(otherEvidence')./sum(~isnan(otherEvidence'));
    objectScore{subj}    = nansum(objEvidence')./sum(~isnan(objEvidence'));
    scramScore{subj}     = nansum(scramEvidence')./sum(~isnan(scramEvidence'));

    %% Plot relationship between cue reliability and classifier evidence for same-category reinstatement
    if (opts.doPlot && ~exist('figMemBySub', 'var'))
        figMemBySub.h       = aaron_newfig;
        figMemBySub.dims(1) = ceil(sqrt(length(submat)));
        figMemBySub.dims(2) = floor(sqrt(length(submat)));
        if (length(submat) - prod([figMemBySub.dims]) > 0)
            figMemBySub.dims(2) = figMemBySub.dims(1);
        end
        set(gca, 'FontSize', 24);
        set(gca, 'FontWeight', 'demi');
        hold on;
    end

    for memIdx = 1:length(memLevels);
        memTrials           = [stackedMem == memLevels(memIdx)];
        if (opts.useDiffScore)
            memSameEvidence     = [faceEvidence(faceTrials & memTrials, :)  ; sceneEvidence(sceneTrials & memTrials, :)] - [sceneEvidence(faceTrials & memTrials, :) ; faceEvidence(sceneTrials & memTrials, :)];
        else
            memSameEvidence     = [faceEvidence(faceTrials & memTrials, :)  ; sceneEvidence(sceneTrials & memTrials, :)];
        end
        memOtherEvidence    = [faceEvidence(sceneTrials & memTrials, :) ; sceneEvidence(faceTrials & memTrials, :)];

        memSameScore{subj, memIdx}  = nansum(memSameEvidence')./sum(~isnan(memSameEvidence'));
        memOtherScore{subj, memIdx} = nansum(memOtherEvidence')./sum(~isnan(memOtherEvidence'));

        if (opts.doPlot)
            figure(figMemBySub.h);
            subplot(figMemBySub.dims(1), figMemBySub.dims(2), find(submat==subj));
            set(gca, 'FontSize', 24);
            set(gca, 'FontWeight', 'demi');
            hold on;

            errorbar(memIdx, nanmean(memSameScore{subj, memIdx}), nanstd(memSameScore{subj, memIdx})/sqrt(sum(~isnan(memSameScore{subj, memIdx}))), 'ko', 'LineWidth', 2);
            if (memIdx == length(memLevels))
                set(gca, 'XTick', [1:length(memLevels)]);
                set(gca, 'XTickLabel', arrayfun(@num2str, memLevels, ...
                                    'UniformOutput', false));
                title(num2str(subj));
            end
        end % if opts.doPlot
    end % for memIdx

    if (opts.doPlot)
        if (subj == submat(1))
            figure(figMemBySub.h);
            subplot(figMemBySub.dims(1), figMemBySub.dims(2), 1);
            xlabel('Cue reliability');
            ylabel('Same-category classifier evidence');
        end
    end

    %% Plot relationship between cued perceptual coherence and classifier evidence for same-category reinstatement
    if (opts.doPlot && ~exist('figPerBySub', 'var'))
        figPerBySub.h       = aaron_newfig;
        figPerBySub.dims(1) = ceil(sqrt(length(submat)));
        figPerBySub.dims(2) = floor(sqrt(length(submat)));
        if (length(submat) - prod([figPerBySub.dims]) > 0)
            figPerBySub.dims(2) = figPerBySub.dims(1);
        end
        set(gca, 'FontSize', 24);
        set(gca, 'FontWeight', 'demi');
        hold on;
    end

    for perIdx = 1:length(perLevels);
        perTrials           = [stackedPer == perLevels(perIdx)];
        if (opts.useDiffScore)
            perSameEvidence     = [faceEvidence(faceTrials & perTrials, :)  ; sceneEvidence(sceneTrials & perTrials, :)] - [sceneEvidence(faceTrials & perTrials, :) ; faceEvidence(sceneTrials & perTrials, :)];
        else
            perSameEvidence     = [faceEvidence(faceTrials & perTrials, :)  ; sceneEvidence(sceneTrials & perTrials, :)];
        end
        perOtherEvidence    = [faceEvidence(sceneTrials & perTrials, :) ; sceneEvidence(faceTrials & perTrials, :)];

        perSameScore{subj, perIdx}  = nansum(perSameEvidence')./sum(~isnan(perSameEvidence'));
        perOtherScore{subj, perIdx} = nansum(perOtherEvidence')./sum(~isnan(perOtherEvidence'));

        if (opts.doPlot)
            figure(figPerBySub.h);
            subplot(figPerBySub.dims(1), figPerBySub.dims(2), find(submat==subj));
            set(gca, 'FontSize', 24);
            set(gca, 'FontWeight', 'demi');
            hold on;

            errorbar(perIdx, nanmean(perSameScore{subj, perIdx}), nanstd(perSameScore{subj, perIdx})/sqrt(sum(~isnan(perSameScore{subj, perIdx}))), 'ko', 'LineWidth', 2);
            if (perIdx == length(perLevels))
                set(gca, 'XTick', [1:length(perLevels)]);
                set(gca, 'XTickLabel', arrayfun(@num2str, perLevels, ...
                                    'UniformOutput', false));
                title(num2str(subj));
            end
        end % if opts.doPlot
    end % for perIdx

    
%    glm         = [ones(sum(earlyTrials), 1) stackedMem(earlyTrials) stackedPer(earlyTrials) sameEarly'];
    glm         = [ones(sum(earlyTrials), 1) stackedISI(earlyTrials) stackedMem(earlyTrials) stackedPer(earlyTrials) sameEarly'];
    ERTB{subj}  = regress(earlyRTs, glm);

    glm         = [ones(sum(lateTrials), 1) stackedISI(lateTrials) stackedMem(lateTrials) stackedPer(lateTrials) stackedSamples(lateTrials) sameLate'];
    LRTB{subj}  = regress(lateRTs, glm);

    allTrials                = earlyTrials | lateTrials;
    transformedSamples       = stackedSamples;
    transformed(earlyTrials) = 1;                           % RTs are faster for early responses, preserves the direction of the effect

    if (opts.useDiffScore)
        sameAll{subj} = [faceEvidence(allTrials & faceTrials, :) ; sceneEvidence(allTrials & sceneTrials, :)] - [sceneEvidence(allTrials & faceTrials, :) ; faceEvidence(allTrials & sceneTrials, :)];
    else
        sameAll{subj} = [faceEvidence(allTrials & faceTrials, :) ; sceneEvidence(allTrials & sceneTrials, :)];
    end
    sameAll{subj} = nansum(sameAll{subj}')./sum(~isnan(sameAll{subj}'));
    glm           = [ones(sum(allTrials), 1) stackedISI(allTrials) stackedMem(allTrials) stackedPer(allTrials) transformedSamples(allTrials) sameAll{subj}'];

    allRTs{subj}  = stackedRT(allTrials);
    ARTB{subj}    = regress(allRTs{subj}, glm);

    %% Take residual RT
    glm           = glm(:, [1:end-1]);
    B             = regress(allRTs{subj}, glm);
    rRTs{subj}    = allRTs{subj} - glm*B;

    sameEarlyEvidence{subj,1} = sameEarly';
    sameEarlyEvidence{subj,2} = earlyRTs;
    sameLateEvidence{subj,1}  = sameLate';
    sameLateEvidence{subj,2}  = lateRTs;
    sameAllEvidence{subj,1}   = sameAll{subj}';
    sameAllEvidence{subj,2}   = allRTs{subj};
    residualAllEvidence{subj,1} = sameAll{subj}';
    residualAllEvidence{subj,2} = rRTs{subj}';

    if (opts.verbose)
        disp(['plotMVPA: Subject ' num2str(subj) ' early RT beta on same-cat evidence = ' num2str(ERTB{subj}(end))]);
        disp(['plotMVPA: Subject ' num2str(subj) ' late RT beta on same-cat evidence = ' num2str(LRTB{subj}(end))]);
    end

    if (opts.doPlot)
        if (subj == submat(1))
            figure(figPerBySub.h);
            subplot(figPerBySub.dims(1), figPerBySub.dims(2), 1);
            xlabel('Cued perceptual coherence');
            ylabel('Same-category classifier evidence');
        end
    end

end % for subj = submat


    savedEarlyEvidence = NaN(length(submat), 160, 2);
    for subjIdx = 1:length(submat);
        for sliceIdx = 1:2;
            tmp = [sameEarlyEvidence{subjIdx, sliceIdx}(:)];
            savedEarlyEvidence(subjIdx, :, sliceIdx) = [tmp ; NaN(size(savedEarlyEvidence,2)-length(tmp), 1)];
        end
    end
    save('savedEarlyEvidence.mat', 'savedEarlyEvidence');
    if (opts.verbose)
        if (exist('ghootstrap', 'file'))
            [p r] = ghootstrap(savedEarlyEvidence);
            disp(['plotMVPA: reinstatement-RT: early RTs, ghootstrap R=' ...
                 num2str(nanmean(r), '%.2f') ...
                 ', p=' ...
                 num2str(p, '%.2d')]);
        end
    end

    savedLateEvidence = NaN(length(submat), 160, 2);
    for subjIdx = 1:length(submat);
        for sliceIdx = 1:2;
            tmp = [sameLateEvidence{subjIdx, sliceIdx}(:)];
            savedLateEvidence(subjIdx, :, sliceIdx) = [tmp ; NaN(size(savedLateEvidence,2)-length(tmp), 1)];
        end
    end
    save('savedLateEvidence.mat', 'savedLateEvidence');
    if (opts.verbose)
        if (exist('ghootstrap', 'file'))
            [p r] = ghootstrap(savedLateEvidence);
            disp(['plotMVPA: reinstatement-RT: late RTs, ghootstrap R=' ...
                 num2str(nanmean(r), '%.2f') ...
                 ', p=' ...
                 num2str(p, '%.2d')]);
        end
    end

    savedAllEvidence = NaN(length(submat), 160, 2);
    for subjIdx = 1:length(submat);
        for sliceIdx = 1:2;
            tmp = [sameAllEvidence{subjIdx, sliceIdx}(:)];
            savedAllEvidence(subjIdx, :, sliceIdx) = [tmp ; NaN(size(savedAllEvidence,2)-length(tmp), 1)];
        end
    end
    save('savedAllEvidence.mat', 'savedAllEvidence');
    if (opts.verbose)
        if (exist('ghootstrap', 'file'))
            [p r] = ghootstrap(savedAllEvidence);
            disp(['plotMVPA: reinstatement-RT: all RTs, ghootstrap R=' ...
                 num2str(nanmean(r), '%.2f') ...
                 ', p=' ...
                 num2str(p, '%.2d')]);
        end
    end

    savedResidualAllEvidence = NaN(length(submat), 160, 2);
    for subjIdx = 1:length(submat);
        for sliceIdx = 1:2;
            tmp = [residualAllEvidence{subjIdx, sliceIdx}(:)];
            savedResidualAllEvidence(subjIdx, :, sliceIdx) = [tmp ; NaN(size(savedResidualAllEvidence,2)-length(tmp), 1)];
        end
    end
    save('savedResidualAllEvidence.mat', 'savedResidualAllEvidence');
    if (opts.verbose)
        if (exist('ghootstrap', 'file'))
            [p r] = ghootstrap(savedResidualAllEvidence);
            disp(['plotMVPA: reinstatement-RT: all residual RTs, ghootstrap R=' ...
                 num2str(nanmean(r), '%.2f') ...
                 ', p=' ...
                 num2str(p, '%.2d')]);
        end
    end


%% Relationship between same-category reinstatement and RT
ERTB=[ERTB{:}];
[h,p]=ttest(ERTB(end, :));
LRTB=[LRTB{:}];
[h,p]=ttest(LRTB(end, :));
ARTB=[ARTB{:}];
[h,p]=ttest(ARTB(end, :));
mnRTstat = p;

submat = submat(submat~=-1);

mnrt = NaN(max(submat), opts.residualBins);

for subj = submat;
    if (subj == -1)
        continue;
    end
    totalRTs = [rRTs{subj}];

    allevs   = [sameAll{subj}];

    if (opts.zscoreSameEv)
        allevs   = zscore(allevs);
    end

    % Split by bin of reinstatement evidence
    evBins = unique(allevs);
    evBins = [min(evBins):((max(evBins)-min(evBins))/opts.residualBins):max(evBins)];
%    evBins = [0:.25:1];

    for binIdx = 1:length(evBins)-1;
        theseRTs    = [totalRTs(allevs >= evBins(binIdx) & allevs < evBins(binIdx+1))];
        mnrt(subj, binIdx)  = mean(theseRTs);
    end % for binIdx
end % for subj

% nanmean(mnrt)

[h,p]=ttest2(mnrt(:, 1), mnrt(:, end));
disp(['plotMVPA: Difference between low and high RT bins: p=' num2str(p)]);
[B,BINT,R,RINT,STATS] = regress([1:opts.residualBins]', [ones(1, opts.residualBins) ; nanmean(mnrt)]');
disp(['plotMVPA: Line fit to mean RT progression: ' num2str([B(2) STATS(2) STATS(3)])]);

if (opts.doGroupPlot)
    aaron_newfig;
    set(gca, 'FontSize', 24);
    set(gca, 'FontWeight', 'demi');
    hold on;

    bar(nanmean(mnrt), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(nanmean(mnrt), nanstd(mnrt)./sqrt(sum(~isnan(mnrt))));

    ylabel('Residual RT');
    xlabel('Same-category evidence');
end


totalSameScore = [];
totalOtherScore = [];
totalObjectScore = [];
totalScramScore = [];

for subj = submat;
    if (subj == -1)
        continue;
    end
    totalSameScore      = [totalSameScore mean(sameScore{subj})];
    totalOtherScore     = [totalOtherScore mean(otherScore{subj})];
    totalObjectScore    = [totalObjectScore mean(objectScore{subj})];
    totalScramScore     = [totalScramScore mean(scramScore{subj})];
end

[h,p] = ttest(totalSameScore, totalOtherScore);
if (opts.verbose)
    disp(['total same/other: ' ...
          num2str([mean(totalSameScore) mean(totalOtherScore)]) ...
          ', p=' num2str(p)]);
end

% Summary of same/other score
if (0 && opts.doGroupPlot)
    aaron_newfig;
    set(gca, 'FontSize', 24);
    set(gca, 'FontWeight', 'demi');
    hold on;

    if (p<=0.05)
        plot(1.5, 0.6, 'k*', 'MarkerSize', 12);
    elseif (p<=0.1)
        plot(1.5, 0.6, 'k^', 'MarkerSize', 12);
    end
    errorbar(1, mean(totalSameScore), std(totalSameScore)/sqrt(length(totalSameScore)), 'ko', 'LineWidth', 2);
    errorbar(2, mean(totalOtherScore), std(totalOtherScore)/sqrt(length(totalOtherScore)), 'ko', 'LineWidth', 2);
    xlabel(['Evidence type']);
    set(gca, 'XTick', [1:2]);
    set(gca, 'XTickLabel', {'Same ', 'Other'});
end

if (opts.doGroupPlot)
    %% Memory
    subfig = aaron_newfig;
    set(gca, 'FontSize', 24);
    set(gca, 'FontWeight', 'demi');
    hold on;

    allfig = aaron_newfig;
    set(gca, 'FontSize', 24);
    set(gca, 'FontWeight', 'demi');
    hold on;
end

    totalMemSameScore  = NaN(max(submat), length(memLevels));
    totalMemOtherScore = NaN(max(submat), length(memLevels));

    for memIdx = 1:length(memLevels);
        for subj = submat;
            if (subj == -1)
                continue;
            end
            totalMemSameScore(subj, memIdx)  = mean(memSameScore{subj, memIdx});
            totalMemOtherScore(subj, memIdx) = mean(memOtherScore{subj, memIdx});
        end

        [h,p] = ttest2(totalMemSameScore(:, memIdx), totalMemOtherScore(:, memIdx));
            if (opts.verbose)
                disp(['plotMVPA: ' num2str(memLevels(memIdx)) ....
                      ': same v other: ' ...
                      num2str([nanmean(totalMemSameScore(:, memIdx)) nanmean(totalMemOtherScore(:, memIdx))]) ...
                      ', p=' num2str(p)]);
            end

        if (opts.doGroupPlot)
            figure(subfig);
            subplot(2, 2, memIdx);
            set(gca, 'FontSize', 24);
            set(gca, 'FontWeight', 'demi');
            hold on;

            if (p<=0.05)
                plot(1.5, 0.6, 'k*', 'MarkerSize', 12);
            elseif(p<=0.1)
                plot(1.5, 0.6, 'k^', 'MarkerSize', 12);
            end
            errorbar(1, nanmean(totalMemSameScore(:, memIdx)), nanstd(totalMemSameScore(:, memIdx))/sqrt(sum(~isnan(totalMemSameScore(:, memIdx)))), 'ko', 'LineWidth', 2);
            errorbar(2, nanmean(totalMemOtherScore(:, memIdx)), nanstd(totalMemOtherScore(:, memIdx))/sqrt(sum(~isnan(totalMemOtherScore(:, memIdx)))), 'ko', 'LineWidth', 2);
            title(num2str(memLevels(memIdx)));
            xlabel(['Evidence type']);
            set(gca, 'XTick', [1:2]);
            set(gca, 'XTickLabel', {'Same ', 'Other'});

            figure(allfig);
%            if (opts.useDiffScore)
%                diffScore = totalMemSameScore(:, memIdx) - totalMemOtherScore(:, memIdx);
%                errorbar(memIdx, nanmean(diffScore), nanstd(diffScore)/sqrt(sum(~isnan(diffScore))), 'ko', 'LineWidth', 2);
%            else
                errorbar(memIdx, nanmean(totalMemSameScore(:, memIdx)), nanstd(totalMemSameScore(:, memIdx))/sqrt(sum(~isnan(totalMemSameScore(:, memIdx)))), 'ko', 'LineWidth', 2);
%            end
        end

    end % for memIdx

    % Compute pairwise comparisons
    psubj  = NaN(length(memLevels), length(memLevels));
    ptrial = NaN(length(memLevels), length(memLevels));

    for memIdx = 2:length(memLevels)
        for otherIdx = 1:memIdx-1;
            [h, psubj(memIdx, otherIdx)]  = ttest(totalMemSameScore(:, memIdx), totalMemSameScore(:, otherIdx));
            [h, ptrial(memIdx, otherIdx)] = ttest2([memSameScore{:, memIdx}], [memSameScore{:, otherIdx}]);

            if (opts.verbose)
                disp(['plotMVPA: Across-subjs ' ...
                      num2str(memLevels(memIdx)) ' v ' ...
                      num2str(memLevels(otherIdx)) ' [' ...
                      num2str([nanmean(totalMemSameScore(:, memIdx)) nanmean(totalMemSameScore(:, otherIdx))]) ...
                      '], p=' num2str(psubj(memIdx, otherIdx))]);

                disp(['plotMVPA: Across-trials ' ...
                      num2str(memLevels(memIdx)) ' v ' ...
                      num2str(memLevels(otherIdx)) ' [' ...
                      num2str([nanmean([memSameScore{:, memIdx}]) nanmean([memSameScore{:, otherIdx}])]) ...
                      '], p=' num2str(ptrial(memIdx, otherIdx))]);
            end
        end
    end

    if (opts.doGroupPlot)
        figure(allfig);
        xlabel('Memory level');
        set(gca, 'XTick', [1:length(memLevels)]);
        set(gca, 'XTickLabel', arrayfun(@num2str, memLevels, ...
                                'UniformOutput', false));
    end

    % Compute correlation between mem levels and classifier evidence
    scatterPts  = [];
    rcoef       = NaN(max(submat), 1);
    for subj = submat;
            if (subj == -1)
                continue;
            end
        for memIdx = 1:length(memLevels);
            theseVals   = [memSameScore{subj, memIdx}];
            theseVals   = theseVals(~isnan(theseVals));
            scatterPts  = [scatterPts [theseVals ; repmat(memLevels(memIdx), [1 length(theseVals)])]];
        end

%        cc = corrcoef(scatterPts(1, :), scatterPts(2, :));
%        rcoef(subj) = cc(1,2);
        try
            if (opts.useSkippedCorr)
                [r,t,h,outid,hboot] = skipped_correlation(scatterPts(1,:), scatterPts(2,:), 0);
                rcoef(subj) = r.Pearson;
            else
                throw(MException('VarifyOutput:test', 'test'))
            end
        catch
            r = corrcoef(scatterPts(1,:), scatterPts(2,:));
            rcoef(subj) = r(1, 2);
        end
    end

    if (opts.verbose)
        nanmean(rcoef);
        [h,p]=ttest(rcoef);
        disp(['plotMVPA: Across-subjs mem-classifier mean corr coef:' num2str(nanmean(rcoef)) ...
              ' p=' num2str(p)]);
    end

    % --
    scatterPts = [];
    for memIdx = 1:length(memLevels);
        theseVals  = [totalMemSameScore(:, memIdx)];
        theseVals  = theseVals(~isnan(theseVals))';
        scatterPts = [scatterPts [theseVals ; repmat(memLevels(memIdx), [1 length(theseVals)])]];
    end

    plotTrials = [1:length(scatterPts(1, :))];

    if (opts.useSkippedCorr)
        [r,t,h,outid,hboot] = skipped_correlation(scatterPts(1,:), scatterPts(2,:), 0);
        plotTrials          = setdiff([1:length(scatterPts(1, :))], [outid{:}]);
        robust_pearson      = tpdf(t.Pearson, length(plotTrials)-1);
        robust_spearman     = tpdf(t.Spearman, length(plotTrials)-1);

        if (opts.verbose)
            disp(['plotMVPA: Across-subjs mem-classifier robust corr: R=' ...
                   num2str(r.Pearson) ...
                   ', p=' num2str(robust_pearson)]);
        end
    else
        [R,p]               = corrcoef(scatterPts');

        if (opts.verbose)
            disp(['plotMVPA: Across-subjs mem-classifier correlation ' num2str(R(1, 2)) ...
                  ', p=' num2str(p(1, 2))]);
        end
    end

    if (opts.doGroupPlot)
        aaron_newfig;
        set(gca, 'FontSize', 24);
        set(gca, 'FontWeight', 'demi');
        hold on;
        for memIdx = 1:length(memLevels);
            theseScatters = scatterPts(1, plotTrials);
            theseScatters = theseScatters(scatterPts(2, plotTrials)==memLevels(memIdx));
            errorbar(memIdx, nanmean(theseScatters), nanstd(theseScatters)/sqrt(length(theseScatters)), 'ko', 'LineWidth', 4);
%            bar(memIdx, nanmean(theseScatters), 'FaceColor', [0.8 0.8 0.8])
        end
        if (opts.useSkippedCorr)
            title(['Cue level - same evidence: robust R=' num2str(r.Pearson, '%.2d') ...
                    ', p=' num2str(robust_pearson)]);
        else
            title(['Cue level - same evidence: R=' num2str(R(1,2),'%.2d') ...
                    ', p=' num2str(p(1,2))]);
        end
        set(gca, 'XTick', [1:length(memLevels)]);
        set(gca, 'XTickLabel', arrayfun(@num2str, memLevels, ...
                                'UniformOutput', false));
        xlabel('Cue reliability');
        ylabel('Same-category classifier evidence');
    end

    scatterPts = [];
    for memIdx = 1:length(memLevels);
        theseVals  = [memSameScore{:, memIdx}];
        scatterPts = [scatterPts [theseVals ; repmat(memLevels(memIdx), [1 length(theseVals)])]];
    end

    [R,p]               = corrcoef(scatterPts');
    if (opts.useSkippedCorr)
        [r,t,h,outid,hboot] = skipped_correlation(scatterPts(1,:), scatterPts(2,:), 0);
        plotTrials          = setdiff([1:length(scatterPts(1, :))], [outid{:}]);
        robust_pearson      = tpdf(t.Pearson, length(plotTrials)-1);
        robust_spearman     = tpdf(t.Spearman, length(plotTrials)-1);

        if (opts.verbose)
            disp(['Across-trials mem-classifier robust corr: R=' ...
                   num2str(r.Pearson) ...
                   ', p=' num2str(robust_pearson)]);
            disp(['plotMVPA: Across-trials mem-classifier correlation ' num2str(R(1, 2)) ...
                  ', p=' num2str(p(1, 2))]);
        end
    end

    if (opts.doGroupPlot)
        aaron_newfig;
        set(gca, 'FontSize', 24);
        set(gca, 'FontWeight', 'demi');
        hold on;

        for memIdx = 1:length(memLevels);
            theseScatters = scatterPts(1, plotTrials);
            theseScatters = theseScatters(scatterPts(2, plotTrials)==memLevels(memIdx));
            errorbar(memIdx, nanmean(theseScatters), nanstd(theseScatters)/sqrt(length(theseScatters)), 'ko', 'LineWidth', 4);
        end
        set(gca, 'XTick', [1:length(memLevels)]);
        set(gca, 'XTickLabel', arrayfun(@num2str, memLevels, ...
                                'UniformOutput', false));
        if (opts.useSkippedCorr)
            title(['Cue level - same evidence: R=' num2str(r.Pearson, '%.2d') ...
                    ', p=' num2str(robust_pearson)]);
        end
        ylabel('Same-category classifier evidence');
        xlabel('Cue reliability');
    end

    %% Perceptual coherence
    if (opts.doGroupPlot)
        subfig = aaron_newfig;
        set(gca, 'FontSize', 24);
        set(gca, 'FontWeight', 'demi');
        hold on;

        allfig = aaron_newfig;
        set(gca, 'FontSize', 24);
        set(gca, 'FontWeight', 'demi');
        hold on;
    end

    totalPerSameScore  = NaN(max(submat), length(perLevels));
    totalPerOtherScore = NaN(max(submat), length(perLevels));

    for perIdx = 1:length(perLevels);
        for subj = submat;
            if (subj == -1)
                continue;
            end
            totalPerSameScore(subj, perIdx)  = nanmean([perSameScore{subj, perIdx}]);
            totalPerOtherScore(subj, perIdx) = nanmean([perOtherScore{subj, perIdx}]);
        end

        [h,p] = ttest2(totalPerSameScore(:, perIdx), totalPerOtherScore(:, perIdx));
        if (opts.verbose)
            disp(['plotMVPA: ' ...
                   num2str(perLevels(perIdx)) ': same v other ' ...
                   num2str([nanmean(totalPerSameScore(:, perIdx)) nanmean(totalPerOtherScore(:, perIdx))]) ...
                  ', p=' num2str(p)]);
        end

        if (opts.doGroupPlot)
            figure(subfig);
            subplot(2, 1, perIdx);
            set(gca, 'FontSize', 24);
            set(gca, 'FontWeight', 'demi');
            hold on;

            if (p<=0.05)
                plot(1.5, 0.6, 'k*', 'MarkerSize', 12);
            elseif(p<=0.1)
                plot(1.5, 0.6, 'k^', 'MarkerSize', 12);
            end
            errorbar(1, nanmean(totalPerSameScore(:, perIdx)), nanstd(totalPerSameScore(:, perIdx))/sqrt(sum(~isnan(totalPerSameScore(:, perIdx)))), 'ko', 'LineWidth', 2);
            errorbar(2, nanmean(totalPerOtherScore(:, perIdx)), nanstd(totalPerOtherScore(:, perIdx))/sqrt(sum(~isnan(totalPerOtherScore(:, perIdx)))), 'ko', 'LineWidth', 2);
            title(num2str(perLevels(perIdx)));
            xlabel(['Evidence type']);
            set(gca, 'XTick', [1:2]);
            set(gca, 'XTickLabel', {'Same ', 'Other'});

            figure(allfig);
%            if (opts.useDiffScore)
%                diffScore = totalPerSameScore(:, perIdx) - totalPerOtherScore(:, perIdx);
%                errorbar(perIdx, nanmean(diffScore), nanstd(diffScore)/sqrt(sum(~isnan(diffScore))), 'ko', 'LineWidth', 2);
%            else
                errorbar(perIdx, nanmean(totalPerSameScore(:, perIdx)), nanstd(totalPerSameScore(:, perIdx))/sqrt(sum(~isnan(totalPerSameScore(:, perIdx)))), 'ko', 'LineWidth', 2);
%                bar(perIdx, nanmean(totalPerSameScore(:, perIdx)), 'FaceColor', [0.8 0.8 0.8]);
%            end
        end
    end % for perIdx

    [h,psubj] = ttest(totalPerSameScore(:, 1), totalPerSameScore(:, 2));
    [h,ptrial] = ttest2([perSameScore{:, 1}], [perSameScore{:, 2}]);

    perPVals = [psubj ptrial];
    if (opts.verbose)
        disp(['plotMVPA: Across-subjs 0.65 v 0.85 [' ...
              num2str([nanmean(totalPerSameScore(:, 1)) nanmean(totalPerSameScore(:, 2))]) ...
              '], p=' num2str(psubj)]);

        disp(['plotMVPA: Across-trials 0.65 v 0.85 [' ...
              num2str([nanmean([perSameScore{:, 1}]) nanmean([perSameScore{:, 2}])]) ...
              '], p=' num2str(ptrial)]);
    end

    if (opts.doGroupPlot)
        figure(allfig);
        xlabel('Perceptual coherence level');
        set(gca, 'XTick', [1:length(perLevels)]);
        set(gca, 'XTickLabel', arrayfun(@num2str, perLevels, ...
                                'UniformOutput', false));
    end

%%
% Response-locked
% Stimulus-locked
% Regression from cue-period evidence on late RTs, accuracy
%


if (0)

%% Plot summary figures for all subjects
if (isvector(submat) && opts.doGroupPlot)
    %% Plot average evidence for each category pre- and post- cue onset, by TR
    aaron_newfig;
    set(gca, 'FontSize', 24);
    set(gca, 'FontWeight', 'demi');
    hold on;

    plot(nanmean(totalObjectEvidence), 'r-', 'LineWidth', 4);
    plot(nanmean(totalObjectEvidence)+(nanstd(totalObjectEvidence)./sqrt(length(submat))), 'r--');
    plot(nanmean(totalObjectEvidence)-(nanstd(totalObjectEvidence)./sqrt(length(submat))), 'r--');
    [h,p] = ttest(totalObjectEvidence, 1/2);

    plotPts = find(nanmean(totalObjectEvidence)>1/2 & (p<=0.05));
    plot(plotPts, ones(1, length(plotPts))*0.95, 'r*');
    plotPts = find(nanmean(totalObjectEvidence)>1/2 & (p>0.05 & p<=0.1));
    plot(plotPts, ones(1, length(plotPts))*0.95, 'r^');

    plot(nanmean(totalSceneEvidence), 'g-', 'LineWidth', 4);
    plot(nanmean(totalSceneEvidence)+(nanstd(totalSceneEvidence)./sqrt(length(submat))), 'g--');
    plot(nanmean(totalSceneEvidence)-(nanstd(totalSceneEvidence)./sqrt(length(submat))), 'g--');
    [h,p] = ttest(totalSceneEvidence, 1/2);

    plotPts = find(nanmean(totalSceneEvidence)>1/2 & (p<=0.05));
    plot(plotPts, ones(1, length(plotPts))*0.9, 'g*');
    plotPts = find(nanmean(totalSceneEvidence)>1/2 & (p>0.05 & p<=0.1));
    plot(plotPts, ones(1, length(plotPts))*0.9, 'g^');

    if (opts.plotScrambled)
        plot(nanmean(totalScramEvidence), 'b-', 'LineWidth', 2);
        plot(nanmean(totalScramEvidence)+(nanstd(totalScramEvidence)./sqrt(length(submat))), 'b--');
        plot(nanmean(totalScramEvidence)-(nanstd(totalScramEvidence)./sqrt(length(submat))), 'b--');
    end

    plot(ones(1,101)*round(size(totalScramEvidence, 2)/2), [0:0.01:1], 'k--', 'LineWidth', 4);

    axis([1 (opts.plotWindow*2)+1 0 1]);
    set(gca, 'XTick', [1:(2*opts.plotWindow)+1]);
    set(gca, 'XTickLabel', arrayfun(@num2str, [-opts.plotWindow:opts.plotWindow], 'UniformOutput', false));
    ylabel('Classifier evidence');
    xlabel('TR (cue-locked)');
    title('All subjects');

    %% Plot regression coefficient by timepoint
    aaron_newfig;
    set(gca, 'FontSize', 24);
    set(gca, 'FontWeight', 'demi');
    hold on;

    plot(nanmean(totalObjectReg), 'r-', 'LineWidth', 4);
    plot(nanmean(totalObjectReg)+(nanstd(totalObjectReg)./sqrt(length(submat))), 'r--');
    plot(nanmean(totalObjectReg)-(nanstd(totalObjectReg)./sqrt(length(submat))), 'r--');
    [h,p] = ttest(totalObjectReg);

    plotPts = find(nanmean(totalObjectReg)<0 & (p<0.05));
    plot(plotPts, ones(1, length(plotPts))*0.95, 'r*');
    plotPts = find(nanmean(totalObjectReg)>1/2 & (p>0.05 & p<=0.1));
    plot(plotPts, ones(1, length(plotPts))*0.95, 'r^');

    plot(nanmean(totalSceneReg), 'g-', 'LineWidth', 4);
    plot(nanmean(totalSceneReg)+(nanstd(totalSceneReg)./sqrt(length(submat))), 'g--');
    plot(nanmean(totalSceneReg)-(nanstd(totalSceneReg)./sqrt(length(submat))), 'g--');
    [h,p] = ttest(totalSceneReg);

    plotPts = find(nanmean(totalSceneReg)<0 & (p<0.05));
    plot(plotPts, ones(1, length(plotPts))*0.9, 'g*');
    plotPts = find(nanmean(totalSceneReg)>1/2 & (p>0.05 & p<=0.1));
    plot(plotPts, ones(1, length(plotPts))*0.9, 'g^');

    if (opts.plotScrambled)
        plot(nanmean(totalScramReg), 'b-', 'LineWidth', 2);
        plot(nanmean(totalScramReg)+(nanstd(totalScramReg)./sqrt(length(submat))), 'b--');
        plot(nanmean(totalScramReg)-(nanstd(totalScramReg)./sqrt(length(submat))), 'b--');
    end

    plot(ones(1,201)*round(size(totalScramReg, 2)/2), [-1:0.01:1], 'k--', 'LineWidth', 4);

    axis([1 (opts.plotWindow*2)+1 -1 1]);
    set(gca, 'XTick', [1:(2*opts.plotWindow)+1]);
    set(gca, 'XTickLabel', arrayfun(@num2str, [-opts.plotWindow:opts.plotWindow], 'UniformOutput', false));
    ylabel('Regression coefficient');
    xlabel('TR (cue-locked)');
    title('All subjects');
end

end % if 0
