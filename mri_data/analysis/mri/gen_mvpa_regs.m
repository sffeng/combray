function [conds, runs, runCounter] = gen_mvpa_regs(subj, varargin)
% function [conds, runs, runCounter] = gen_mvpa_regs(subj, varargin)
%
% OPTIONS:
%   'numRuns'   (3)
%   'TR'        (1)
%   'IBI'       (12; inter-block-interval)
%   'stimOn'    (0.5)
%   'stimOff'   (1.3)
%   'doplot'    (false)
%   'outputdir' ({'./'})
%   'numConds'  (4; (e.g. face, object, scene, scrambled_scene))
%   'sampleBin' (0.01; for upsampling)
%   'TL'        (1.8; trial length, unused)
%
%
%

opts = parse_args(varargin, 'numRuns', 3, ...
                  'TR', 1, ...
                  'IBI', 12, ...
                  'doplot', false, ...
                  'outputdir', {'./'}, ...
                  'numConds', 4, ...
                  'sampleBin', 0.01, ...
                  'TL', 1.8, ...
                  'stimOn', 0.5, ...
                  'stimOff', 1.3, ...
                  'binThresh', 4, ...
                  'numTrialsPerBlock', 10, ...
                  'numBlocks', 12, ...
                  'displayWindowLen', 50);

TRbins      = opts.TR/opts.sampleBin;
stimOnBins  = opts.stimOn/opts.sampleBin;
stimOffBins = opts.stimOff/opts.sampleBin;
IBIbins     = opts.IBI/opts.sampleBin;

% cmdline
if (ischar(subj))
    subj = str2num(subj);
end

% PARAM: restCond
restCond = 0;   % 0;

if (restCond)
    opts.numConds = restCond;
end

sd    = loadSubj(subj, '../../');
bv    = sd.badvols;

% Inputs are images probed
probeRec    = [sd.localizer.probeOrder{:}];
numTrials   = length(probeRec);
maskTrials  = find(probeRec>0);

runs        = zeros(1, numTrials);

% Generate conds in onset space
% XXX: Should just do this using the actual localizer onset times, but the cumulative error is something like 20ms over an entire session, so it doesn't matter.
tmpConds = repmat(sd.localizer.blockType', 1, opts.numTrialsPerBlock);
tmpConds = tmpConds';
tmpConds = tmpConds(:)';

localizerOnsets = [sd.localizer.localizerOnsets{:}];

if (opts.doplot)
    figure;
    plot(tmpConds(1:opts.displayWindowLen));
end

runIdx  = 1;
msConds = [];
msRuns  = [];

runCounter                          = zeros(opts.numConds, opts.numRuns);
pickRun                             = 1;
runCounter(tmpConds(1), pickRun)    = 1;

% Now arrange runs so that there are an even number of conditions per run.
for condIdx = 1:length(tmpConds);
    % IBI before each block after the first.
    if (condIdx > 1 && mod(condIdx, opts.numTrialsPerBlock) == 1)
        msConds = [msConds zeros(opts.numConds, IBIbins)];

        if (restCond)
            msConds(restCond, end-IBIbins:end) = 1;
        end

% NB: We code IBI as part of a run; it will be excised by norest_sel anyway
%        msRuns  = [msRuns zeros(1, 1200)]; % Code IBI as empty elsewise

        pickRun = find(runCounter(tmpConds(condIdx), :) == min(runCounter(tmpConds(condIdx), :)), 1);
        msRuns  = [msRuns ones(1, IBIbins)*pickRun];
        runCounter(tmpConds(condIdx), pickRun) = runCounter(tmpConds(condIdx), pickRun) + 1;
    end

    % Code run: 500ms stim on + 1300ms ISI
    % XXX: Skip ISI after repeats (identified in maskTrials)?
    % Pick a run - pick the first one that is equal the minimum of run counter
    msRuns   = [msRuns ones(1, stimOnBins+stimOffBins)*pickRun];

    % Code onset: 500ms stim on + 1300ms ISI
    % XXX: Skip ISI after repeats (identified in maskTrials)
    thisOns  = zeros(opts.numConds, stimOnBins+stimOffBins);

    % If not the last item in the block, code the ISI as on. If the last item, we're on to the IBI so no need to.
    if (mod(condIdx,opts.numTrialsPerBlock))
        thisOns(tmpConds(condIdx), :)    = 1;
    else
        thisOns(tmpConds(condIdx), 1:stimOnBins) = 1;
        if (restCond)
            thisOns(restCond, (stimOnBins+1):(stimOnBins+stimOffBins)) = 1;
        end
    end
    msConds  = [msConds thisOns];
end
if (opts.doplot)
    figure;
    subplot(2,2,1); imagesc(flipud(msConds(:,1:end)));
    title(['1. By onset']);
end

if (opts.doplot)
    subplot(2,2,3); imagesc(flipud(msConds(:,1:end)));
    colorbar;
    title(['2. Convolved']);
end

conds   = zeros(opts.numConds,1);
runs    = zeros(1,1);
condIdx = 0;

% Downsample to TR space
for timeIdx = 0:TRbins:size(msConds,2)-TRbins-1;
    condIdx = condIdx + 1;

    for rowIdx = 1:opts.numConds;
        conds(rowIdx,condIdx) = sum(msConds(rowIdx,(timeIdx + 1):(timeIdx+1+TRbins)),2);
    end
    runs(condIdx) = max(msRuns((timeIdx + 1):(timeIdx+1+TRbins)));
end
 if (opts.doplot)
    subplot(2,2,4); imagesc(flipud(conds(:,1:end)));
    colorbar;
    title(['3. By TR']);
end

% Threshold and binarize
thresh = TRbins/opts.binThresh;    % NB Arbitrary (e.g. 1/4 TR len)
for rowIdx = 1:opts.numConds;
    conds(rowIdx,conds(rowIdx,:)<thresh) = 0;
    conds(rowIdx,conds(rowIdx,:)>=thresh) = 1;
end

% Do we have 'bad' volumes (motion, spikes) to exclude?
if (~isempty(bv))
    disp(['Excluding bad volumes']);
    conds(:,bv{end}.badvols) = 0;                   % Last session is localizer
else
    disp(['NOT excluding bad volumes']);
end


if (opts.doplot)
    subplot(2,2,2); imagesc(flipud(conds(:,1:end)));
    title(['4. Thresholded and binarized']);
end

if (opts.doplot)
    figure;

    plot(runs);
end

if (iscell(opts.outputdir))
    opts.outputdir = opts.outputdir{:};
end

save([opts.outputdir 'localizer_regs_' num2str(subj) '.mat'], ...
                     'conds', 'runs', 'probeRec', 'maskTrials');
