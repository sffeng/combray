#!/bin/sh
#
# copybehav <subj>
#

if [ "$#" -lt 1 ];then
   echo "Usage: $0 <subj>"
   exit
fi

MLBCMD="/Applications/MATLAB_R2014b.app/bin/matlab -nojvm -nodisplay -nodesktop -nosplash -singleCompThread -r"
SUBJ=$1

STARTDIR=`pwd`

BASEDIR=~/Dropbox/brains/episodic_controller/combray/shared/exptCode/combrayCode/data
ANALYSISDIR=$BASEDIR/analysis
MRIDIR=$ANALYSISDIR/mri
STAGINGDIR=$MRIDIR/behav

rm -f $STAGINGDIR/*

# 1. Copy raw behavioral files
cp $BASEDIR/*_$SUBJ* $STAGINGDIR

# 2. Generate MVPA regs & subjSummary
cd $MRIDIR
$MLBCMD "gen_mvpa_regs $SUBJ;cd $ANALYSISDIR;plotSubj $SUBJ 0 0 -1;parseSubjs;exit"

# 3. Copy
mv $MRIDIR/localizer_regs_$SUBJ.mat $STAGINGDIR
cp $ANALYSISDIR/subjSummary.mat $STAGINGDIR

cd $STAGINGDIR
scp * rondo:matlab/combray/behav/

rm -f $STAGINGDIR/*

cd $STARTDIR
