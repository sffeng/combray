#!/bin/sh
#
# Warp the statmaps to standard space
#

if [ "$#" -lt 1 ];then
   echo "$0 <subj>"
   exit
fi

source sharedvars 2>/dev/null

SUBJDATADIR=`ls -1d $DATADIR/$EXPTNAME* | head -$SUBJ | tail -1`
SUBJROIDIR=$ROIDIR/$SUBJ/mri
MAPDIR=$CODEDIR/SLmaps

echo "$OUTPUTPREFIX Removing old warped files for $SUBJ..."
rm -f $MAPDIR/slmap_$SUBJ\_*warped*

featIdx=0
# For each session, check for the isorespTrain FEAT dir.
for featDir in `ls -1d $SUBJDATADIR/*isorespTrain.feat` ; do
    featIdx=`expr $featIdx + 1`
    echo "$OUTPUTPREFIX Block $featIdx: $featDir..."
    for mapFile in `ls -1 $MAPDIR/slmap_$SUBJ\_*_$featIdx\_*_same.nii.gz`; do
        mapFile=`echo $mapFile | sed 's/.nii.gz//'`
        echo "$OUTPUTPREFIX Warping $mapFile to standard space..."
        applywarp --ref=$featDir/reg/standard --in=$mapFile --warp=$featDir/reg/highres2standard_warp --premat=$featDir/reg/example_func2highres.mat --out=$mapFile\_warped
    done

    for mapFile in `ls -1 $MAPDIR/slmap_$SUBJ\_*_$featIdx\_*_diff.nii.gz`; do
        mapFile=`echo $mapFile | sed 's/.nii.gz//'`
        echo "$OUTPUTPREFIX Warping $mapFile to standard space..."
        applywarp --ref=$featDir/reg/standard --in=$mapFile --warp=$featDir/reg/highres2standard_warp --premat=$featDir/reg/example_func2highres.mat --out=$mapFile\_warped
    done
done
