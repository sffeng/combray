#!/bin/sh
#
# Generate an FSL configuration file for each EPI
#

if [ "$#" -lt 1 ]; then
    echo $0 "<subj_number>"
    exit
fi

source sharedvars 2>/dev/null

# XXX: Check that dir exists
echo "$OUTPUTPREFIX Processing $SUBJ: $SUBJDIR..."

PPDIR="$HOME/matlab/$EXPTNAME/preproc_files"
PPTEMPLATE="$PPDIR/_template"

# Get the name of the structural (take the last one if there are multiple)
# Struct files are of the form: /jukebox/cohen/aaronmb/$EXPTNAME/mri/_SUBJDIR_/_STRUCTFN_
STRUCTFN=`ls -1 $SUBJDIR/|fgrep $STRUCTTAG|fgrep _brain|fgrep -v _skull|tail -1`
echo "$OUTPUTPREFIX Using structural $STRUCTFN..."

SUBJBASE=`basename $SUBJDIR`

# Loop through EPIs, generate a separate config file for each.
# EPI files are of the form: /jukebox/cohen/aaronmb/$EXPTNAME/mri/_SUBJDIR_/_EPIFN_
EPIIDX=0
for EPIFN in `find $SUBJDIR -name *$EPITAG* -type f | sort` ; do
    EPIFN=`basename $EPIFN`
    echo "$OUTPUTPREFIX Processing EPI $EPIIDX: $EPIFN..."
    rm -f $PPDIR/preproc_$SUBJ\_$EPIIDX.fsf

    SIZSTR=`fslsize $SUBJDIR/$EPIFN -s | cut -d' ' -f 3,5,7,9`
    DIM1=`echo $SIZSTR|cut -d' ' -f 1`
    DIM2=`echo $SIZSTR|cut -d' ' -f 2`
    DIM3=`echo $SIZSTR|cut -d' ' -f 3`
    DIM4=`echo $SIZSTR|cut -d' ' -f 4`

    NUMVOX=`expr $DIM1 '*' $DIM2 '*' $DIM3 '*' $DIM4`

    STRUCTFN=`echo $STRUCTFN|sed 's/.nii.gz//'`
    EPIFN=`echo $EPIFN|sed 's/.nii.gz//'`
    cat $PPTEMPLATE | sed s/_SUBJDIR_/$SUBJBASE/ | sed s/_STRUCTFN_/$STRUCTFN/ | sed s/_EPIFN_/$EPIFN/ | sed s/_NUMVOX_/$NUMVOX/ | sed s/_NUMVOLS_/$DIM4/ > $PPDIR/preproc_$SUBJ\_$EPIIDX.fsf
    echo "$OUTPUTPREFIX Submitting feat for $EPIFN ($NUMVOX voxels, $DIM4 volumes)..."
    fsl_sub -m be -q long.q -N $EXPTNAME\_feat_$SUBJ\_$EPIIDX feat $PPDIR/preproc_$SUBJ\_$EPIIDX.fsf

    EPIIDX=`expr $EPIIDX + 1`
done

echo "$OUTPUTPREFIX $SUBJDIR: Processed $EPIIDX EPI files."
