#!/bin/sh
#
# Generate a mask in functional space
#

if [ "$#" -lt 1 ];then
   echo "$0 <subj> [anatmask]"
   exit
fi

source sharedvars 2>/dev/null

SUBJDATADIR=`ls -1d $DATADIR/$EXPTNAME* | head -$SUBJ | tail -1`
SUBJROIDIR=$ROIDIR/$SUBJ/mri

# Filename of the mask in anatomical space
ANATMASKNAME=$SUBJROIDIR/bilat_vtmask
# Filename of the output mask in individual functional space
FUNCMASKNAME=$SUBJROIDIR/bilat_vtmask_warped

if [ "$#" -gt 1 ]; then
    ANATMASKNAME=`echo $2 | sed 's/.nii//'`
    FUNCMASKNAME=`basename $ANATMASKNAME`
    FUNCMASKNAME=$FUNCMASKNAME\_warped

    echo "$OUTPUTPREFIX Using anat $ANATMASKNAME and func $FUNCMASKNAME..."
fi

# Check for the localizer FEAT dir.
TESTLOCDIR=0
test -d $SUBJDATADIR/*localizer*feat && TESTLOCDIR=1

if [ "$TESTLOCDIR" -eq 1 ]; then
    echo "$OUTPUTPREFIX Subject $SUBJ - $SUBJDATADIR"
    # Change to subject directory
    cd $SUBJDATADIR/*localizer*feat/reg

	echo "$OUTPUTPREFIX Copying $ANATMASKNAME..."
    # Copy anatomical-space mask to the reg directory
    cp $ANATMASKNAME.nii .
	ANATMASKNAME=`basename $ANATMASKNAME`

	if [ `echo $ANATMASKNAME | grep ^std_` ]; then
		echo "$OUTPUTPREFIX $ANATMASKNAME is in standard space. Applying standard2highres..."
		HRMASKNAME=`echo $ANATMASKNAME | sed 's/std_//'`
		applywarp --ref=highres --in=$ANATMASKNAME --premat=standard2highres.mat --out=$HRMASKNAME
		ANATMASKNAME=$HRMASKNAME
		FUNCMASKNAME=$ANATMASKNAME\_warped
		echo "$OUTPUTPREFIX *** Note that new func mask name is $FUNCMASKNAME ***..."
	fi

	rm -f $FUNCMASKNAME*

    echo "$OUTPUTPREFIX Applying highres2example_func..."
#	applywarp --ref=example_func --in=/jukebox/cohen/aaronmb/combray/mri/ROIs/7/mri/bilat_vtmask --premat=highres2example_func.mat --out=warped_vtmask_pre
	applywarp --ref=example_func --in=$ANATMASKNAME --premat=highres2example_func.mat --out=$FUNCMASKNAME

    echo "$OUTPUTPREFIX Re-binarizing..."
    # Re-binarize (because applywarp creates a weighted average of neighboring voxels)
    fslmaths $FUNCMASKNAME -thr 0.05 -bin $FUNCMASKNAME

	gzip -d $FUNCMASKNAME.nii.gz

    echo "$OUTPUTPREFIX Copying..."
    # Copy new mask to ROI directory
    #        mv $FUNCMASKNAME.nii $SUBJROIDIR

    # Clean up
    rm -f $ANATMASKNAME.nii

    cd $STARTDIR
fi

