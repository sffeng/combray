function results = computeRSA(subj, varargin)
% function results = computeRSA(subj, ...)
%
% OPTIONS:
%                            'doPlot', false, ...
%                            'TR', 1000, ...
%                            'voxSz', 2.5, ...
%                            'shiftvols', 5, ...
%                            'numDispVols', 2, ...
%                            'saveout', {['computeRSA_results_' num2str(subj, '%.2d'), '.mat']}, ...
%                            'verbose', false, ...
%                            'veryverbose', false, ...
%                            'debug', false, ...
%                            'radius', 10);
%
% For each session
%   parfor each radius-cube searchlight ROI, compute
%      for each image
%          For each of K presentations of that image,
%              The average pattern across the 2TRs of its presentation (onset + shiftvols)
%          end
%      end
%
%     For each image
%         For each of K presentations of that image
%             The correlation with the K-1 presentations of the same image
%             The correlation with the K presentations of the different image from that category
%         end
%     end
%   end
%
%   save the correlation results (roughly 20x20 matrix) for this ROI
%
% OUTPUT:
%   results = cell array of numSessions, containing cell array of number ROIs
%             containing 4D array: numImages x numTrials x 2 x numTrials
%

if (ischar(subj))             % qsub
    subj = str2num(subj);
end

opts = parse_args(varargin, 'doPlot', false, ...
                            'TR', 1000, ...
                            'voxSz', 2.5, ...
                            'shiftvols', 5, ...
                            'numDispVols', 2, ...
                            'saveout', {['computeRSA_results_' num2str(subj, '%.2d'), '.mat']}, ...
                            'verbose', false, ...
                            'veryverbose', false, ...
                            'debug', false, ...
                            'radius', 10);


% NB: Subjects before 16, skip first presentation in each block due to offset error (see email thread 'hmm' with MA 09.30.2015)
% XXX: Unimplemented
skipFirstTrial = false;
if (subj < 16)
    skipFirstTrial = true;
end

%% Constants
dataDir     = ['/jukebox/cohen/aaronmb/combray/mri'];
behavDir    = fullfile(dataDir, 'behav');
baseDir     = dataDir;

behavdat    = loadSubj(subj, behavDir);

% XXX: ToDo: Excise high-motion volumes, runs? (Don't seem to be more than a few of those in respTrain, so low-priority)

%% Initialize
if (iscell(opts.saveout))
    % Get string var from cell
    opts.saveout   = opts.saveout{:};
end

if (opts.verbose)
    disp(['computeRSA: Subject ' ...
          num2str(subj, '%.2d') ...
          ', voxel dim (mm): ' num2str(opts.voxSz) ...
          ', searchlight radius in mm: ' num2str(opts.radius) ...
          ', TR (ms): ' num2str(opts.TR) ...
          ', hemodynamic shift in volumes: ' num2str(opts.shiftvols) ...
          ', number of volumes per image: ' num2str(opts.numDispVols) ...
          '. Saving output to ' opts.saveout]);
end

% Convert radius from mm to voxels
opts.radius = opts.radius/opts.voxSz;

subjDir     = dir(fullfile(dataDir, 'combray_*'));
subjDir     = subjDir(subj).name;

rtFeatDir   = dir(fullfile(dataDir, subjDir, '*isorespTrain*feat'));
numSess     = length(rtFeatDir);                                            % XXX: In the future, discard failed sessions? Also, discard incorrect responses?

% Sessions x number searchlights x maxTrials x 2 x maxTrials
results      = cell(numSess, 1);
dataDims     = cell(numSess, 1);
centerPoints = cell(numSess, 1);

for sessIdx = 1:numSess;
    thisFeatDir = rtFeatDir(sessIdx).name;

    rtFullFile   = fullfile(dataDir, subjDir, thisFeatDir, 'filtered_func_data.nii');
    maskFullFile = fullfile(dataDir, subjDir, thisFeatDir, 'mask.nii');

    if (opts.verbose)
        disp(['computeRSA: Loading ' rtFullFile]);
    end

    sessData     = cbiReadNifti(rtFullFile);
    maskData     = cbiReadNifti(maskFullFile);

    centerPoints{sessIdx} = find(maskData);
    dataDims{sessIdx}     = size(maskData);                                          % For later ind2sub parsing

    % z-score sessData
    if (opts.verbose)
        disp(['computeRSA: Z-scoring ...']);
    end

    sessData     = zscore(sessData, 0, 4);

%    numImages    = max(unique([behavdat.pracrec{sessIdx}.images]));
    numImages   = 4;
    numTrials   = 0;    % Number of trials per image

    imageTrials  = cell(numImages, 1);

    for imgIdx   = 1:numImages;
        imageTrials{imgIdx} = find(arrayfun(@(x)(x==imgIdx), [behavdat.pracrec{sessIdx}.image]));
        if (length(imageTrials{imgIdx}) > numTrials)
            numTrials = length(imageTrials{imgIdx});
        end
    end

    % For each searchlight ROI, for each image, for each trial, same/diff, each other trial
    corrMatrix = cell(length(centerPoints{sessIdx}), 1);

    if (opts.verbose)
        disp(['computeRSA: Session ' num2str(sessIdx) ...
              ', performing searchlight across ' num2str(length(centerPoints{sessIdx})) ...
              ' voxels, max ' num2str(numTrials), ...
              ' trials per image.']);
    end

    if (opts.debug && ~isempty(opts.saveout))
        save(fullfile(dataDir, [opts.saveout(1:end-4) '_sess_' ...
                                num2str(sessIdx) '.mat']), ...
             'corrMatrix', 'opts', 'centerPoints', 'dataDims');
    end

    parfor cpIdx = 1:length(centerPoints{sessIdx});
%    for cpIdx = 1:length(centerPoints{sessIdx});           % for debugging
        thisCorr        = NaN(numImages, numTrials, 2, numTrials);

        % Construct cube
        [cx cy cz]      = ind2sub(dataDims{sessIdx}, centerPoints{sessIdx}(cpIdx));
        cubeCoords      = [cx cx cy cy cz cz];
        cubeCoords(1:2:end) = cubeCoords(1:2:end)-opts.radius;
        cubeCoords(2:2:end) = cubeCoords(2:2:end)+opts.radius;

        % Truncate where cube runs over mask
        cubeCoords(cubeCoords<1) = 1;
        for checkDim = 1:3;
            if (cubeCoords(2*(checkDim-1)+1)>size(maskData, checkDim))
                cubeCoords(2*(checkDim-1)+1) = size(maskData, checkDim);
            end
            if (cubeCoords(2*(checkDim-1)+2)>size(maskData, checkDim))
                cubeCoords(2*(checkDim-1)+2) = size(maskData, checkDim);
            end
        end

        if (opts.verbose)
            disp(['computeRSA: Subject ' num2str(subj) ', ROI ' num2str(cpIdx) '/' num2str(length(centerPoints{sessIdx})) ': ' num2str([cx cy cz]) '...']);
        end

        % Images x numTrials
        thesePatterns = cell(numImages, numTrials);

        % First, select the per-trial patterns
        for imgIdx = 1:numImages;
            for trialIdx = 1:length(imageTrials{imgIdx});
                thisTrial = imageTrials{imgIdx}(trialIdx);
                startVol  = floor(behavdat.pracrec{sessIdx}(thisTrial).onset/(opts.TR/1000));                      % Get volume number
                startVol  = startVol + opts.shiftvols;

                thesePatterns{imgIdx, trialIdx} = mean(sessData(cubeCoords(1):cubeCoords(2), ...
                                                                cubeCoords(3):cubeCoords(4), ...
                                                                cubeCoords(5):cubeCoords(6), ...
                                                                startVol:(startVol+(opts.numDispVols-1))), 4);
            end
        end

        % Next, compute all the pairwise correlations
        for imgIdx = 1:numImages;
            imageCategory = [imgIdx > 2];               % 0 faces, 1 scenes
            otherImage    = (imageCategory * 2) + ((imgIdx-(imageCategory*2))>1 * -1) + ((imgIdx-(imageCategory*2))<2 * 1);     % Pair 1 w 2 and 3 w 4

            for trialIdx = 1:length(imageTrials{imgIdx});
                % First compute same trials
                for sameIdx = 1:length(imageTrials{imgIdx});
                    if (sameIdx == trialIdx)
                        continue;
                    end
                    cc = corrcoef(thesePatterns{imgIdx, trialIdx}, thesePatterns{imgIdx, sameIdx});
                    thisCorr(imgIdx, trialIdx, 1, sameIdx) = cc(1, 2);
%                    thisCorr(imgIdx, trialIdx, 1, sameIdx) = 1 - slmetric_pw(thesePatterns{imgIdx, trialIdx}, thesePatterns{imgIdx, sameIdx}, 'corrdist');
                    if (opts.veryverbose)
                        disp(['computeRSA: ... image ' num2str(imgIdx) ...
                              ' trial ' num2str(imageTrials{imgIdx}(trialIdx)) ...
                              ' x same-trial ' num2str(imageTrials{imgIdx}(sameIdx)) ...
                              ': ' num2str(cc(1, 2))]);
                    end
                end

                % Skip other-corrs for the second image in each category, since we already have them from the first image.
                if (otherImage < imgIdx)
                    continue;
                end

                % Now for diff trials
                for otherIdx = 1:length(imageTrials{otherImage});
                    cc = corrcoef(thesePatterns{imgIdx, trialIdx}, thesePatterns{otherImage, otherIdx});
                    thisCorr(imgIdx, trialIdx, 2, otherIdx) = cc(1,2);
%                    thisCorr(imgIdx, trialIdx, 2, sameIdx) = 1 - slmetric_pw(thesePatterns{imgIdx, trialIdx}, thesePatterns{otherImage, otherIdx}, 'corrdist');
                    if (opts.veryverbose)
                        disp(['computeRSA: ... image ' num2str(imgIdx) ...
                              ' trial ' num2str(imageTrials{imgIdx}(trialIdx)) ...
                              ' x other-trial ' num2str(imageTrials{otherImage}(otherIdx)) ...
                              ': ' num2str(cc(1, 2))]);
                    end
                end
            end
        end

        corrMatrix{cpIdx} = thisCorr;
        % Save intermediate data at the end of each centerpoint (VERY SLOW, USE ONLY FOR DEBUGGING)
        if (opts.debug && ~isempty(opts.saveout))
            save(fullfile(dataDir, [opts.saveout(1:end-4) '_sess_' ...
                                    num2str(sessIdx) '.mat']), ...
                 'corrMatrix', 'opts', 'centerPoints', 'dataDims', '-append');
        end

    end % parfor cpIdx

    results{sessIdx} = corrMatrix;
    if (~isempty(opts.saveout))
        save(fullfile(dataDir, opts.saveout), 'results', 'opts', 'centerPoints', 'dataDims');
    end
end
