#!/bin/sh
#
# mricopy <scandir>
#
# Where scandir is the name of the directory under the conquest folder (e.g. "combray_20150901_01_7341-2823")
#
# Or with no args to copy whatever is uncopied
#

source sharedvars 2>/dev/null

WAITTIME=60

BASEDIR="/jukebox/dicom/conquest/Prisma-MSTZ400D/Cohen^/2015"
EXPTDIR="/jukebox/cohen/aaronmb/combray/mri"
SCANDIR=""

if [ $# -gt 0 ]; then
    SCANDIR=$1
else
    # Find any scan dirs that don't exist in $EXPTDIR
    for fn in `ls -1d $BASEDIR/combray*`; do
        TESTDIRFN=`basename $fn`
        test -d $EXPTDIR/$TESTDIRFN || SCANDIR=$TESTDIRFN
    done

    if [ -z "$SCANDIR" ]; then
        echo "$OUTPUTPREFIX Found no uncopied scandirs. Exiting..."
        exit
    else
        echo "$OUTPUTPREFIX Copying $SCANDIR..."
        break
    fi

fi

while true; do
    fgrep ',99,-1' $BASEDIR/$SCANDIR/TransferStatus.log && break
    echo "$OUTPUTPREFIX Transfer for $SCANDIR not yet complete... checking again in $WAITTIME seconds."
    sleep $WAITTIME
done

echo "$BASEDIR/$SCANDIR/NII -> $EXPTDIR/$SCANDIR"
cp -R $BASEDIR/$SCANDIR/NII $EXPTDIR/$SCANDIR
echo "$BASEDIR/$SCANDIR/QA -> $EXPTDIR/$SCANDIR/QA"
cp -R $BASEDIR/$SCANDIR/QA $EXPTDIR/$SCANDIR/

