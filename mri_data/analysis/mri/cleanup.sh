#!/bin/sh
#
# Clean up unused files
#

source sharedvars 2>/dev/null

# FEAT directories: Make sure filtered_func_data and mask files are uncompressed because MVPA toolbox is stupid
#
for featfn in `find $DATADIR -name 'filtered_func_data.nii.gz'`; do
	echo "$OUTPUTPREFIX Uncompressing $featfn..."
	gzip -d $featfn
done

for featfn in `find $DATADIR -name 'mask.nii.gz'`; do
	echo "$OUTPUTPREFIX Uncompressing $featfn..."
	gzip -d $featfn
done

# ROI directories:
#	1. Make sure ROI dirs are in ROIs/
#	2. Only need mri/ subdir
#	3. Uncompress warped files
#
for roisub in `find $DATADIR -mindepth 1 -maxdepth 1 -type d -not -name "$EXPTNAME\_*" -not -name 'ROIs' -not -name 'localizer_regs' -not -name 'badvols' -not -name 'behav' -not -name 'mvpa' -not -name 'RSAresults' -not -name 'SLmaps'` -not -name 'group_maps'; do
	echo "$OUTPUTPREFIX Moving $roisub to $ROIDIR..."
	mv $roisub $ROIDIR
done

for roisub in `find $ROIDIR -mindepth 1 -maxdepth 1 -type d`; do
	for roifn in `find $roisub/mri -name '*_warped.nii.gz'`; do
		echo "$OUTPUTPREFIX Uncompressing warped mask $roifn..."
		gzip -d $roifn
	done

	for subsubdir in `find $roisub -mindepth 1 -maxdepth 1 -type d -not -name mri`; do
		echo $subsubdir
		rm -rf $subsubdir
	done
done

echo "$OUTPUTPREFIX Cleaning up recon-all logfiles from $ROIDIR..."
rm -f $ROIDIR/recon*

for roisub in `find $ROIDIR -mindepth 1 -maxdepth 1 -type d|fgrep -v localizer_regs`; do
	echo "$OUTPUTPREFIX Cleaning up $roisub..."
	for subsubdir in `find $roisub -mindepth 1 -maxdepth 1 -type d -not -name mri`; do
		echo $subsubdir
		rm -rf $subsubdir
	done
done

# cluster outputs
#
echo "$OUTPUTPREFIX Cleaning up cluster output files in $CODEDIR..."
rm -f $CODEDIR/$EXPTNAME\_feat*

# RSA/MVPA data
#
echo "$OUTPUTPREFIX Moving RSA and MVPA results files into place..."
mv $DATADIR/computeRSA*.mat $DATADIR/RSAresults/
mv $CODEDIR/mvpa_*.mat $DATADIR/mvpa/

echo "$OUTPUTPREFIX Uncompressing copies of example_func files..."
