function [subStruct results acts scratchpad acts_test scratchpad_test test_perf] = run_mvpa(subj, varargin)
% function [subStruct results acts scratchpad acts_test scratchpad_test test_perf] = run_mvpa(subj, [varargin])
%
% ARGUMENTS (default)
%   checkOverlays  (false)
%   excludeRest    (true)
%   shiftTRs       (5)
%   ROIfn          ({'bilat_vtmask_warped'})
%   useSmoothed    (true)
%   penalty        (10)
%   featureSelect  (0.05)
%   binThresh      (4)
%   numRuns        (3)
%   trainNew       (false)
%   includeObjects (true)
%   verbose        (true)
%   runTest        (false)
%

[opts] = parse_args(varargin, 'checkOverlays', false, ...
                              'excludeRest', true, ...
                              'shiftTRs', 5, ...
                              'ROIfn', {'bilat_vtmask_warped'}, ...
                              'useSmoothed', true, ...
                              'penalty', 10, ...
                              'featureSelect', 0.05, ...
                              'binThresh', 4, ...
                              'numRuns', 3, ...
                              'trainNew', false, ...
                              'includeObjects', true, ...
                              'verbose', true, ...
                              'runTest', true);

%% Constants
dataDir     = ['/jukebox/cohen/aaronmb/combray/mri'];
baseDir     = dataDir;
roiDir      = fullfile(baseDir, 'ROIs');
locRegsDir  = fullfile(baseDir, 'localizer_regs');

% qsub
if (ischar(subj))
    subj = str2num(subj);
end

%% Initialize
if (isnumeric(opts.ROIfn))
    % Convert index to string
    roilist    = dir(fullfile(roiDir, num2str(subj), 'mri', '*.nii'));
    opts.ROIfn = roilist(opts.ROIfn).name(1:end-4);                  % Strip '.nii'
elseif (iscell(opts.ROIfn))
    % Get string var from cell
    opts.ROIfn   = opts.ROIfn{:};
end
roiFullFile = fullfile(roiDir, num2str(subj), 'mri', ...
                       [opts.ROIfn '.nii']);

if (opts.verbose)
    disp(['run_mvpa: Subject ' num2str(subj) ...
          ', ROI ' roiFullFile]);
end

subjDir     = dir(fullfile(dataDir, 'combray_*'));
subjDir     = subjDir(subj).name;

locFeatDir  = dir(fullfile(dataDir, subjDir, '*ocalizer*feat'));
locFeatDir  = locFeatDir.name;
locFullFile = fullfile(dataDir, subjDir, locFeatDir, 'filtered_func_data.nii');

if (opts.runTest)
    testFeatDir     = dir(fullfile(dataDir, subjDir, '*test*feat'));          % XXX there are two of these
    testFullFile    = cell(length(testFeatDir),1);

    % Because MRI runs come off the scanner numbered in a weird way
    [testRunNumbers newIdx] = sort(cellfun(@(x)(str2num(x)),strtok({testFeatDir.name},'-')));
    testFeatDir    = testFeatDir(newIdx);

    for tIdx = 1:length(testFeatDir);
        testFullFile{tIdx} = fullfile(dataDir, subjDir, testFeatDir(tIdx).name, 'filtered_func_data.nii');
    end
end
if (0)
	% XXX
	gen_mvpa_regs(subj, 'numRuns', opts.numRuns, ...
                    'binThresh', opts.binThresh, ...
                    'doplot', false, ...
                    'outputdir', locRegsDir);
end

%% Run
subStruct = init_subj('combray', ['sub' num2str(subj)]);
subStruct = load_spm_mask(subStruct, opts.ROIfn, roiFullFile);
subStruct = load_spm_pattern(subStruct, ['loc_' opts.ROIfn], opts.ROIfn, locFullFile);

if (opts.checkOverlays)
    h = cbiReadNiftiHeader(locFullFile);
    wholevol  = ones(h.dim(2), h.dim(3), h.dim(4));
    subStruct = initset_object(subStruct, 'mask', 'wholevol', wholevol);

    subStruct = load_spm_pattern(subStruct, 'loc_wholevol', 'wholevol', locFullFile);
    view_pattern_overlay(subStruct, 'loc_wholevol', opts.ROIfn);
    pause

    if (opts.runTest)
        for testIdx = 1:length(testFullFile);
            subStruct = load_spm_pattern(subStruct, ['test_wholevol_' num2str(testIdx)], ...
                                         'wholevol', testFullFile{testIdx});
            view_pattern_overlay(subStruct, ['test_wholevol_' num2str(testIdx)], opts.ROIfn);
            pause
        end
    end
end

% Get epi length
epiLen    = subStruct.patterns{1}.matsize(2);

% Load subject-specific regs identifying each TR as belonging to one of 3 types
regs = load(fullfile(locRegsDir, ['localizer_regs_' num2str(subj, '%.2d') '.mat']));
% Pad the conds regressors with rest trials
regs.conds(:, end:epiLen) = 0;
if (size(regs.conds, 1) == 5)
    condnames = {'faces', 'objects', 'scenes', 'scrambled_scenes', 'rest'};
else
    condnames = {'faces', 'objects', 'scenes', 'scrambled_scenes'};
end
if (~opts.includeObjects)
    nonobjcond = cellfun(@(x)(~isempty(strfind(x, 'objects'))), condnames);
    regs.conds = regs.conds(nonobjcond, :);
    condnames  = {condnames{nonobjcond}};
end

subStruct = init_object(subStruct, 'regressors', 'conds');
subStruct = set_mat(subStruct, 'regressors', 'conds', regs.conds);

% Define 'runs', which are in our case (clusters of) miniblocks
% NB now 'runs' are meant to be balanced - equal numbers of scenes and faces, etc; so no longer correspond to miniblocks
% Pad the runs with the end run (doesn't matter, these vols will be excluded below)
regs.runs(end:epiLen)   = max(regs.runs);
subStruct               = init_object(subStruct, 'selector', 'runs');
subStruct               = set_mat(subStruct, 'selector', 'runs', regs.runs);

subStruct = set_objfield(subStruct, 'regressors', 'conds', 'condnames', condnames);

subStruct = shift_regressors(subStruct, 'conds', 'runs', opts.shiftTRs);
condsname = sprintf('conds_sh%i', opts.shiftTRs);

class_args.train_funct_name = 'train_L2_RLR';
class_args.test_funct_name  = 'test_L2_RLR';
class_args.penalty          = opts.penalty;
class_args.lambda           = 'crossvalidation';

if (opts.excludeRest)
    subStruct = create_norest_sel(subStruct, condsname);

    subStruct = create_xvalid_indices(subStruct, 'runs', ...
                   'actives_selname', [condsname '_norest'], ...
                   'ignore_jumbled_runs', true, ...
                   'new_selstem','runs_norest_xval');

    subStruct = zscore_runs(subStruct, ['loc_' opts.ROIfn], ...
                                       'runs', 'actives_selname', ...
                                       [condsname '_norest'], 'ignore_jumbled_runs', true);

    runSelector = 'runs_norest_xval';
else
    subStruct = create_xvalid_indices(subStruct, 'runs', 'ignore_jumbled_runs', true);

    subStruct = zscore_runs(subStruct, ['loc_' opts.ROIfn], ...
                                       'runs', 'ignore_jumbled_runs', true);

    runSelector = 'runs_xval';
end

if (opts.featureSelect)
    subStruct = feature_select(subStruct, ['loc_' opts.ROIfn '_z'], condsname, runSelector);
    [subStruct results acts scratchpad] = cross_validation(subStruct, ['loc_' opts.ROIfn '_z'], condsname, runSelector, ...
                                                                      ['loc_' opts.ROIfn '_z_thresh' num2str(opts.featureSelect)], class_args);
else
    [subStruct results acts scratchpad] = cross_validation(subStruct, ['loc_' opts.ROIfn '_z'], condsname, runSelector, ...
                                                                      opts.ROIfn, class_args);
end


realign   = 'y';                % XXX
rest_cond = 'n';                % XXX
preproc   = 5*opts.useSmoothed; % XXX
runtime   = datestr(now);

scratchpad_test = [];
acts_test       = [];
test_perf       = -1*ones(1, size(regs.conds, 1));

%% Run test (XXX: Do this with cross_validation() "correctly".
if (opts.runTest)
    if (opts.verbose)
        disp(['run_mvpa: Generating evidence timeseries for ' num2str(length(testFullFile)) ...
              ' runs.']);
    end

    scratchpad_test = cell(length(testFullFile), 1);
    acts_test       = cell(length(testFullFile), 1);
    test_perf       = cell(length(testFullFile), 1); % -1*ones(1, size(regs.conds, 1));

    train_funct_hand = str2func(class_args.train_funct_name);
    test_funct_hand  = str2func(class_args.test_funct_name);

    % Load training regressors
    traintargs = get_mat(subStruct, 'regressors', 'conds');

    % Load test patterns
    for testIdx = 1:length(testFullFile);
        if (opts.verbose)
            disp(['run_mvpa: Running ' testFullFile{testIdx}]);
        end

        subStruct  = load_spm_pattern(subStruct, ['test_' opts.ROIfn ...
                                                  '_' num2str(testIdx)], opts.ROIfn, testFullFile{testIdx});
        % Get epi length
        testEpiLen{testIdx}    = get_object(subStruct, 'pattern', ['test_' opts.ROIfn ...
                                                               '_' num2str(testIdx)]);
        testEpiLen{testIdx}    = testEpiLen{testIdx}.matsize(2);
        testRuns{testIdx}      = ones(1, testEpiLen{testIdx});

        subStruct = init_object(subStruct, 'selector', ['testRuns_' num2str(testIdx)]);
        subStruct = set_mat(subStruct, 'selector', ['testRuns_' num2str(testIdx)], testRuns{testIdx});
        subStruct = zscore_runs(subStruct, ['test_' opts.ROIfn ...
                                            '_' num2str(testIdx)], ...
                                           ['testRuns_' num2str(testIdx)], ...
                                           'ignore_jumbled_runs', true);

        if (0)
            % XXX Is this a good idea? (yes, because zscore)
            [sd]   = loadSubj(subj);
            bv     = sd.badvols;
            if (~isempty(bv))
                testRuns{testIdx}(bv) = 0;
            end
        end

        if (opts.trainNew)
            % XXX feature select training? Call statmap_anova
            % XXX exclude rest voxels from training?
            [scratchpad_test{testIdx}]                      = train_funct_hand(get_masked_pattern(subStruct, ['loc_' opts.ROIfn '_z'], opts.ROIfn), traintargs, class_args, []);
            [acts_test{testIdx} scratchpad_test{testIdx}]   = test_funct_hand(get_masked_pattern(subStruct, ['test_' opts.ROIfn '_' num2str(testIdx) '_z'], opts.ROIfn), [], scratchpad_test{testIdx});

            test_perf{testIdx} = nansum(acts_test{testIdx}');
        else
            if (~opts.featureSelect)
                testPattern = get_masked_pattern(subStruct, ['test_' opts.ROIfn '_' num2str(testIdx) '_z'], opts.ROIfn);
            end

            % Run each of the x-validation trained classifiers on the test data
            scratchpad_test{testIdx} = cell(length(scratchpad), 1);
            acts_test{testIdx}       = cell(length(scratchpad), 1);

            for classifierIdx = 1:length(scratchpad);
                if (opts.featureSelect)
                    testPattern = get_masked_pattern(subStruct, ['test_' opts.ROIfn ...
                                                                     '_' num2str(testIdx) ...
                                                                     '_z'], ...
                                                                    ['loc_' opts.ROIfn ...
                                                                     '_z_thresh' num2str(opts.featureSelect) ...
                                                                     '_' num2str(classifierIdx)]);
                end

                [acts_test{testIdx}{classifierIdx} scratchpad_test{testIdx}{classifierIdx}] = test_funct_hand(testPattern, [], scratchpad{classifierIdx});
                test_perf{testIdx}(classifierIdx, :)                                        = nansum(acts_test{testIdx}{classifierIdx}');

                if (opts.verbose)
                    disp(['run_mvpa: Ran session ' num2str(testIdx) ...
                          ' with classifier ' num2str(classifierIdx) ...
                          ' got test_perf: ' num2str(mean(test_perf{testIdx}(classifierIdx, :)))]);
                end
            end

            test_perf{testIdx} = mean(test_perf{testIdx});
        end % if opts.trainNew

    end     % for testIdx

    save(['mvpa_' num2str(subj) '.mat'], 'acts_test', 'scratchpad_test', 'scratchpad', 'acts', 'subStruct', 'test_perf', 'results', 'opts');
end

logFile = fopen(fullfile(baseDir, 'xval_results.csv'), 'a');
% runtime,subj,opts.ROIfn,preproc,realign,opts.binThresh,opts.shiftTRs,opts.numRuns,rest_cond,opts.excludeRest,opts.featureSelect,class_args.train_funct_name,class_args.opts.penalty,results.total_perf
fprintf(logFile, '%s,%d,%s,%d,%s,%d,%d,%d,%s,%d,%0.2f,%s,%d,%0.6f\n', ...
                 runtime, subj, opts.ROIfn, preproc, realign, opts.binThresh, opts.shiftTRs, ...
                 opts.numRuns, rest_cond, opts.excludeRest, opts.featureSelect, class_args.train_funct_name, class_args.penalty, results.total_perf);
fclose(logFile);
