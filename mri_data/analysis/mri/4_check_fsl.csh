#!/bin/tcsh
#
# Visualize the individual anatomical warped to template
#

if ( $#argv < 1 ) then
    echo $0 "<subj_number>"
    exit
endif

source sharedvars >&/dev/null

setenv CHECKEXCLUDEFILE $DATADIR/feat_aligned

echo "$OUTPUTPREFIX Checking $SUBJDIR..."
foreach FEATEPIDIR ( `find $SUBJDIR -type d -name '*.feat'` )

    fgrep $FEATEPIDIR $CHECKEXCLUDEFILE >/dev/null && echo "$OUTPUTPREFIX Skipping $FEATEPIDIR..." && continue
    echo "$OUTPUTPREFIX Entering $FEATEPIDIR..."

    setenv STDFN "$FEATEPIDIR/reg/highres2standard.nii.gz"
    echo "$OUTPUTPREFIX Checking $STDFN..."
#    fslview $FSLDIR/data/standard/MNI152_T1_2mm_brain.nii.gz $STDFN -t 0.5 -l "Red-Yellow"
    fslview $FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz $STDFN -t 0.5 -l "Red-Yellow"
    echo "$OUTPUTPREFIX Checking filtered_func_data..."
    fslview $FEATEPIDIR/filtered_func_data

    echo "$OUTPUTPREFIX Look good? [Y/n]"
    set inputline=$<

    if (`echo $inputline|tr "[:upper:]" "[:lower:]"` == "n") then
        echo "$OUTPUTPREFIX Skipping $SUBJDIR..."
        continue;
    else
        echo "$OUTPUTPREFIX Adding $SUBJDIR to $CHECKEXCLUDEFILE..."
        touch $CHECKEXCLUDEFILE
        echo $FEATEPIDIR >> $CHECKEXCLUDEFILE
    endif
end

