
rois{1} = 'bilat_vtmask_warped';
rois{2} = 'GKmask_warped';

parfor subj = [9 10 11 12 13 14 15];
	for roiIdx = [1 2];
		for featureSelect = [0 0.05];
			for penalty = [10 100 500];
				for shiftTRs = [0:10];
					run_mvpa(subj, 'ROIfn', rois{roiIdx}, ...
						       'featureSelect', featureSelect, ...
						       'penalty', penalty, ...
						       'shiftTRs', shiftTRs);
				end
			end
		end
	end
end
