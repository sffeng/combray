function newharness(submat, varargin)
% function newharness(submat, varargin)
%
% OPTIONS (defaults):
%                    'plotLearn'        - plot only learning phase (false)
%                    'plotTest'         - plot only test phase (false)
%                    'plotEarly'        - plot only early responses during test phase (false)
%                    'plotLate'         - plot only late responses during test phase (false)
%                    'plotRawRT'        - plot histograms of raw RTs, across all trials and by perceptual and memory bins (false)
%                    'plotStimLockedRT' - plot histograms of stimulus-locked RTs, across all trials and by perceptual and memory bins (false)
%                    'plotRegressions'  - plot beta weights relating memory and perceptual evidence to choice and RT, in each coh/cue bin (false)
%
%                    'plotIndividual'   - plot individual subjects (false)
%                    'plotGroup'        - plot group combined (false)
%
%                    'zscore'           - (false)
%                    'dataDir'          - ('../')
%                    'savefigs'         - print figures to file (false)
%                    'saveRun'          - save matlab workspace and code files in a unique directory (XXX: unimplemented)
%                    'verbose'          - (false)
%                    'veryverbose'      - (false) Print for each subject
%
%
% TODO:
%   Exclude individual runs
%   Add regression on RTs to take out confounds (post-errors, perseveration)
%   Add back in evidence path plots and regressions (see plotEvidence.m)
%   Add back in calibration differences (see harness.m)
%
% For learn phase
%   Accuracy (best response) at 0.5 0.6 0.7 0.8
%       over time and summary
%
% For early responses
%       (z)RT (within early responses) by mem level 0.5 0.6 0.7 0.8
%       and by percept level 0.65 0.85
%       and by both (2x3 heatmap)
%
% For late responses
%       (z)RT (within late responses), accuracy
%       by mem level 0.2 0.3 0.4 0.5 0.6 0.7 0.8
%       and by percept level 0.65 0.85
%       and by both (2x6 heatmap)
%
% For all test responses
%       (z)RT
%       by mem level 0.2 0.3 0.4 0.5 0.6 0.7 0.8
%       and by percept level 0.65 0.85
%       and by both (2x6 heatmap)
%
% Raw RT distributions
%

%%
% Constants
cueDuration = 0.75;
perList     = [0.65 0.85];
cueList     = [0.5 0.6 0.7 0.8];

[opts] = parse_args(varargin, ...
                    'testTrialOffset', 0, ...
                    'plotLearn', false, ...
                    'plotEarly', false, ...
                    'plotLate', false, ...
                    'plotTest', false, ...
                    'plotRawRT', false, ...
                    'plotStimLockedRT', false, ...
                    'plotRegressions', false, ...
                    'plotIndividual', false, ...
                    'plotGroup', false, ...
                    'zscore', false, ...
                    'dataDir', {'../'}, ...
                    'saveRun', false, ...
                    'savefigs', false, ...
                    'veryverbose', false, ...
                    'verbose', false);

if (iscell(opts.dataDir))
    opts.dataDir = opts.dataDir{:};
end
disp(['Using dir ' opts.dataDir]);

if (~exist('submat', 'var') || isempty(submat))
    dataFiles = dir([opts.dataDir '*.txt']);
    submat   = [1:length(dataFiles)];
end

%%
% Collect parsed data for all subjects
%
for subj = submat;
    try
        % XXX: Replace with parseSubj, refactored plotSubj
        pr{subj} = plotSubj(subj, 0, 0, 0, -1, opts.dataDir);
    catch
        disp(['newharness: Skipping subject ' num2str(subj)]);
        continue;
    end
end

%%
% Plot accuracy during learning phase
%
if (opts.plotLearn)
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;


    memBins = [0.5 0.6 0.7 0.8];
    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    learnTrials                  = zeros(360, 1);
    learnTrials([1:100 181:280]) = 1;

    resprec = NaN(max(submat), length(memBins), 50);
    acc     = NaN(max(submat), length(memBins));
    for memIdx = 1:length(memBins);
        for subj = submat;
            memMask                  = pr{subj}(:, 1) == memBins(memIdx) | pr{subj}(:, 1) == 1-memBins(memIdx);

            % Record the history of responses to each memory level, padding with NaNs where needed (e.g. for skipped trials)
            resprec(subj, memIdx, :) = [pr{subj}(memMask & learnTrials, 7) == pr{subj}(memMask & learnTrials, 8); NaN(1, size(resprec, 3) - sum(memMask & learnTrials))];
            acc(subj, memIdx)        = nanmean(resprec(subj, memIdx, :));

            if (opts.verbose)
                disp(['newharness: plotLearn: Subject ' num2str(subj) ', p=' num2str(memBins(memIdx)) ': ' num2str(acc(subj, memIdx)) '/' num2str(sum(memMask & learnTrials))]);
            end
        end

        % Plot mean accuracy across subjects
        errorbar(memIdx, nanmean(acc(:, memIdx)), nanstd(acc(:, memIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end

    title(['Learning phase accuracy (n=' num2str(length(submat)) ')']);
    set(gca, 'XTick', [1:length(memBins)]);
    set(gca, 'XTickLabel', ...
             cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    xlabel('Cued probability of best response');
    ylabel('Overall accuracy');

    % Plot accuracy trend across subjects
    memCols = ['r' 'g' 'b'];
    ph      = zeros(1, length(memBins));
    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for memIdx = 1:length(memBins);
        % XXX I'm not sure these error bars are correct.
        meanperf = cumsum(squeeze(nanmean(resprec(:, memIdx, :))))'./[1:size(resprec, 3)];
        perfsem  = squeeze(nanstd(resprec(:, memIdx, :)))'/sqrt(length(submat));
        ph(memIdx) = plot(meanperf, [memCols(memIdx) '-'], 'LineWidth', 4);
        plot(meanperf+perfsem, [memCols(memIdx) '--'], 'LineWidth', 1);
        plot(meanperf-perfsem, [memCols(memIdx) '--'], 'LineWidth', 1);
    end
    axis([0 size(resprec, 3) 0 1]);

    legend(ph, cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)), ...
           'Location', 'SouthEast');
    xlabel('Trial #');
    ylabel('Cumulative accuracy');

    if (opts.savefigs)
        print('-depsc', '-r800', 'learn_accuracy.eps');
        if (opts.verbose)
            disp(['newharness: plotLearn: Wrote figure learn_accuracy.eps']);
        end
    end
end

%%
% Early responses in test
%
%       # of early responses by mem level 0.6 0.7 0.8
%       and by percept level 0.65 0.85
%       and by both (2x3 heatmap)
%
%       (z)RT (within early responses) by mem level 0.6 0.7 0.8
%       and by percept level 0.65 0.85
%       and by both (2x3 heatmap)
%
% XXX: Split up by correct / incorrect? Not sure that's valid for early responses.
%
if (opts.plotEarly)
    testTrials                    = zeros(360, 1);
    testTrials([101:180 281:360]) = 1;

    memBins = [0.5 0.6 0.7 0.8];
    perBins = [0.65 0.85];

    memMask = cell(max(submat), length(memBins));
    memRT   = NaN(max(submat), length(memBins));
    earlyRT = cell(max(submat), 1);

    mixmapRT    = NaN(max(submat), length(memBins), length(perBins));
    mixmapCount = NaN(max(submat), length(memBins), length(perBins));

    for subj = submat;
        % Select trials where RT is less than ISI + cueDuration (allow for trial-by-trial ISI)
        if (~isempty(strfind(opts.dataDir, 'V6')))
            % Constant ISI for V6, 1s
            earlyTrials{subj}     = pr{subj}(:, 2)<=(1+cueDuration) & testTrials;
        else
            earlyTrials{subj}     = pr{subj}(:, 2)<=(pr{subj}(:, 9)+cueDuration) & testTrials;
        end
        if (opts.verbose)
            disp(['newharness: plotEarly: Subject ' num2str(subj) ...
                  ': ' num2str(sum(earlyTrials{subj})) ' early responses.']);
        end
        earlyRT{subj}   = pr{subj}(earlyTrials{subj}, 2);

        % Z-Score within-subject, within early trials
        if (opts.zscore)
            earlyRT{subj} = (earlyRT{subj} - nanmean(earlyRT{subj})/nanstd(earlyRT{subj}));
        end

        for memIdx = 1:length(memBins);
            allMemMask{subj, memIdx} = pr{subj}(:, 1) == memBins(memIdx) | pr{subj}(:, 1) == 1-memBins(memIdx);
            memMask{subj, memIdx}    = pr{subj}(earlyTrials{subj}, 1) == memBins(memIdx) | pr{subj}(earlyTrials{subj}, 1) == 1-memBins(memIdx);
            memRT(subj, memIdx)      = nanmean(earlyRT{subj}(memMask{subj, memIdx}));

            if (opts.verbose)
                disp(['newharness: plotEarly: Subject ' num2str(subj) ', cue p = ' num2str(memBins(memIdx)) ...
                      ': mean RT ' num2str(memRT(subj, memIdx), 2) ...
                      ' on ' num2str(sum(memMask{subj, memIdx})) ...
                      ' trials.']);
            end
        end

        for perIdx = 1:length(perBins);
            allPerMask{subj, perIdx} = pr{subj}(:, 6) == perBins(perIdx);
            perMask{subj, perIdx}    = pr{subj}(earlyTrials{subj}, 6) == perBins(perIdx);
            perRT(subj, perIdx)      = nanmean(earlyRT{subj}(perMask{subj, perIdx}));

            if (opts.verbose)
                disp(['newharness: plotEarly: Subject ' num2str(subj) ', coherence = ' num2str(perBins(perIdx)) ...
                      ': mean RT ' num2str(perRT(subj, perIdx), 2) ...
                      ' on ' num2str(sum(perMask{subj, perIdx})) ...
                      ' trials.']);
            end

            for memIdx = 1:length(memBins);
                allMixMask                          = (pr{subj}(:, 6) == perBins(perIdx)) & (pr{subj}(:, 1) == memBins(memIdx) | pr{subj}(:, 1) == 1-memBins(memIdx));
                mixMask                             = perMask{subj, perIdx} & memMask{subj, memIdx};
                mixmapRT(subj, memIdx, perIdx)      = nanmean(earlyRT{subj}(mixMask));
                % Proportion of all trials at this mem and per level that were early responses
                mixmapCount(subj, memIdx, perIdx)   = sum(mixMask)./sum(allMixMask);
            end
        end
    end

    %% Heatmaps
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    mixmapRT = squeeze(nanmean(mixmapRT, 1));
    imagesc(mixmapRT);

    set(gca, 'YTick', [1:length(memBins)]);
    set(gca, 'YTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    colorbar;
    title(['RT on early response trials (n=' num2str(length(submat)) ')']);

    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    mixmapCount = squeeze(nanmean(mixmapCount, 1));
    imagesc(mixmapCount);
    xlabel('Cued perceptual coherence');
    ylabel('Cued probability of best response');
    title('% Early responses');
    set(gca, 'YTick', [1:length(memBins)]);
    set(gca, 'YTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    colorbar;

    %% Response counts
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for memIdx = 1:length(memBins);
        numEarly = cellfun(@sum, {memMask{:, memIdx}});
        numEarly = numEarly./[cellfun(@sum, {allMemMask{:, memIdx}})];
        errorbar(memIdx, nanmean(numEarly), nanstd(numEarly)/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(memBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    xlabel('Cued probability of best response');
    title(['Early response trials (n=' num2str(length(submat)) ')']);

    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for perIdx = 1:length(perBins);
        numEarly = cellfun(@sum, {perMask{:, perIdx}});
        numEarly = numEarly./[cellfun(@sum, {allPerMask{:, perIdx}})];
        errorbar(perIdx, nanmean(numEarly), nanstd(numEarly)/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    xlabel('Cued perceptual coherence level');
    ylabel('% Early responses');

    %% (z)RT
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for memIdx = 1:length(memBins);
        errorbar(memIdx, nanmean(memRT(:, memIdx)), nanstd(memRT(:, memIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(memBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    xlabel('Cued probability of best response');
    if (opts.zscore)
        ylabel('zRT');
    else
        ylabel('RT');
    end
    title(['Early response trials (n=' num2str(length(submat)) ')']);

    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for perIdx = 1:length(perBins);
        errorbar(perIdx, nanmean(perRT(:, perIdx)), nanstd(perRT(:, perIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    xlabel('Cued perceptual coherence level');
    if (opts.zscore)
        ylabel('zRT');
    else
        ylabel('RT');
    end
end

%%
% Plot only late-response trials
%
if (opts.plotLate)
    testTrials                    = zeros(360, 1);
    testTrials([101:180 281:360]) = 1;
    cueDuration                   = 0.75;

    memBins = [0.2 0.3 0.4 0.5 0.6 0.7 0.8];
    perBins = [0.65 0.85];

    memMask = cell(max(submat), length(memBins));
    memRT   = NaN(max(submat), length(memBins));
    lateRT  = cell(max(submat), 1);

    mixmapRT    = NaN(max(submat), length(memBins), length(perBins));
    mixmapCount = NaN(max(submat), length(memBins), length(perBins));

    for subj = submat;
        % Select trials where RT is more than ISI + cueDuration (allow for trial-by-trial ISI)
        if (~isempty(strfind(opts.dataDir, 'V6')))
            % Constant ISI for V6
            lateTrials{subj}     = pr{subj}(:, 2)>(1+cueDuration) & testTrials;
        else
            lateTrials{subj}     = pr{subj}(:, 2)>(pr{subj}(:, 9)+cueDuration) & testTrials;
        end
        if (opts.verbose)
            disp(['newharness: plotLate: Subject ' num2str(subj) ...
                  ': ' num2str(sum(lateTrials{subj})) ' late responses.']);
        end
        lateRT{subj}   = pr{subj}(lateTrials{subj}, 2);

        % Z-Score within-subject, within late trials
        if (opts.zscore)
            lateRT{subj} = (lateRT{subj} - nanmean(lateRT{subj})/nanstd(lateRT{subj}));
        end

        for memIdx = 1:length(memBins);
            allMemMask{subj, memIdx} = pr{subj}(:, 1) == memBins(memIdx);
            memMask{subj, memIdx}    = pr{subj}(lateTrials{subj}, 1) == memBins(memIdx);
            memRT(subj, memIdx)      = nanmean(lateRT{subj}(memMask{subj, memIdx}));

            if (opts.verbose)
                disp(['newharness: plotLate: Subject ' num2str(subj) ', cue p = ' num2str(memBins(memIdx)) ...
                      ': mean RT ' num2str(memRT(subj, memIdx), 2) ...
                      ' on ' num2str(sum(memMask{subj, memIdx})) ...
                      ' trials.']);
            end
        end

        for perIdx = 1:length(perBins);
            allPerMask{subj, memIdx} = pr{subj}(:, 6) == perBins(perIdx);
            perMask{subj, perIdx}    = pr{subj}(lateTrials{subj}, 6) == perBins(perIdx);
            perRT(subj, perIdx)      = nanmean(lateRT{subj}(perMask{subj, perIdx}));

            if (opts.verbose)
                disp(['newharness: plotLate: Subject ' num2str(subj) ', coherence = ' num2str(perBins(perIdx)) ...
                      ': mean RT ' num2str(perRT(subj, perIdx), 2) ...
                      ' on ' num2str(sum(perMask{subj, perIdx})) ...
                      ' trials.']);
            end

            for memIdx = 1:length(memBins);
                allMixMask                          = (pr{subj}(:, 6) == perBins(perIdx)) & (pr{subj}(:, 1) == memBins(memIdx));
                mixMask                             = perMask{subj, perIdx} & memMask{subj, memIdx};
                mixmapRT(subj, memIdx, perIdx)      = nanmean(lateRT{subj}(mixMask));
                % Proportion of all trials at this mem and per level that were early responses
                mixmapCount(subj, memIdx, perIdx)   = sum(mixMask)./sum(allMixMask);
            end
        end
    end

    %% Heatmaps
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    mixmapRT = squeeze(nanmean(mixmapRT, 1));
    imagesc(mixmapRT);
    set(gca, 'YTick', [1:length(memBins)]);
    set(gca, 'YTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    colorbar;
    title(['RT on late response trial RT (n=' num2str(length(submat)) ')']);

    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    mixmapCount = squeeze(nanmean(mixmapCount, 1));
    imagesc(mixmapCount);
    xlabel('Cued perceptual coherence');
    ylabel('Cued probability of best response');
    title('% Late responses');
    set(gca, 'YTick', [1:length(memBins)]);
    set(gca, 'YTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    colorbar;

    %% Response counts
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for memIdx = 1:length(memBins);
        numLate = cellfun(@sum, {memMask{:, memIdx}});
        numLate = numLate./[cellfun(@sum, {allMemMask{:, memIdx}})];
        errorbar(memIdx, nanmean(numLate), nanstd(numLate)/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(memBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    xlabel('Cued probability of best response');
    title(['Late response trial counts (n=' num2str(length(submat)) ')']);

    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for perIdx = 1:length(perBins);
        numLate = cellfun(@sum, {perMask{:, perIdx}});
        numLate = numLate./[cellfun(@sum, {allPerMask{:, perIdx}})];
        errorbar(perIdx, nanmean(numLate), nanstd(numLate)/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    xlabel('Cued perceptual coherence level');
    ylabel('% Late responses');

    %% (z)RT
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for memIdx = 1:length(memBins);
        errorbar(memIdx, nanmean(memRT(:, memIdx)), nanstd(memRT(:, memIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(memBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    xlabel('Cued probability of best response');
    if (opts.zscore)
        ylabel('zRT');
    else
        ylabel('RT');
    end
    title(['Late response trials (n=' num2str(length(submat)) ')']);

    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for perIdx = 1:length(perBins);
        errorbar(perIdx, nanmean(perRT(:, perIdx)), nanstd(perRT(:, perIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    xlabel('Cued perceptual coherence level');
    if (opts.zscore)
        ylabel('zRT');
    else
        ylabel('RT');
    end
end

%%
% Plot all test-period responses
%
if (opts.plotTest)
    testTrials                    = zeros(360, 1);
    testTrials([101:180 281:360]) = 1;

    testTrials = find(testTrials);

    memBins = [0.5 0.6 0.7 0.8];
    perBins = [0.65 0.85];

    memMask = cell(max(submat), length(memBins));
    memRT   = NaN(max(submat), length(memBins));

    mixmapRT    = NaN(max(submat), length(memBins), length(perBins));

    for subj = submat;
        if (opts.verbose)
            disp(['newharness: plotTest: Subject ' num2str(subj) ...
                  ': ' num2str(length(testTrials)) ' test trials.']);
        end
        testRT{subj}   = pr{subj}(testTrials, 2);

        % Z-Score within-subject, within all test trials
        if (opts.zscore)
            testRT{subj} = (testRT{subj} - nanmean(testRT{subj})/nanstd(testRT{subj}));
        end

        for memIdx = 1:length(memBins);
            memMask{subj, memIdx} = pr{subj}(testTrials, 1) == memBins(memIdx) | pr{subj}(testTrials, 1) == 1-memBins(memIdx);
            memRT(subj, memIdx)   = nanmean(testRT{subj}(memMask{subj, memIdx}));

            if (opts.verbose)
                disp(['newharness: plotTest: Subject ' num2str(subj) ', cue p = ' num2str(memBins(memIdx)) ...
                      ': mean RT ' num2str(memRT(subj, memIdx), 2) ...
                      ' on ' num2str(sum(memMask{subj, memIdx})) ...
                      ' trials.']);
            end
        end

        for perIdx = 1:length(perBins);
            perMask{subj, perIdx} = pr{subj}(testTrials, 6) == perBins(perIdx) | pr{subj}(testTrials, 6) == 1-perBins(perIdx);
            perRT(subj, perIdx)   = nanmean(testRT{subj}(perMask{subj, perIdx}));

            if (opts.verbose)
                disp(['newharness: plotTest: Subject ' num2str(subj) ', coherence = ' num2str(perBins(perIdx)) ...
                      ': mean RT ' num2str(perRT(subj, perIdx), 2) ...
                      ' on ' num2str(sum(perMask{subj, perIdx})) ...
                      ' trials.']);
            end

            for memIdx = 1:length(memBins);
                mixMask                             = perMask{subj, perIdx} & memMask{subj, memIdx};
                mixmapRT(subj, memIdx, perIdx)      = nanmean(testRT{subj}(mixMask));
            end
        end
    end

    %% Heatmaps
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    mixmapRT = squeeze(nanmean(mixmapRT, 1));
    imagesc(mixmapRT);
    if (opts.zscore)
        title('zRT on test trials');
    else
        title('RT on test trials');
    end
    set(gca, 'YTick', [1:length(memBins)]);
    set(gca, 'YTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    colorbar;


    %% (z)RT
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    subplot(2, 1, 1);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for memIdx = 1:length(memBins);
        errorbar(memIdx, nanmean(memRT(:, memIdx)), nanstd(memRT(:, memIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(memBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, memBins, 'UniformOutput', false)));
    xlabel('Cued probability of best response');
    if (opts.zscore)
        ylabel('zRT');
    else
        ylabel('RT');
    end
    title(['All test trials (n=' num2str(length(submat)) ')']);

    subplot(2, 1, 2);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;
    for perIdx = 1:length(perBins);
        errorbar(perIdx, nanmean(perRT(:, perIdx)), nanstd(perRT(:, perIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
    end
    set(gca, 'XTick', [1:length(perBins)]);
    set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, perBins, 'UniformOutput', false)));
    xlabel('Cued perceptual coherence level');
    if (opts.zscore)
        ylabel('zRT');
    else
        ylabel('RT');
    end
end


figSubjBySess = [];
mainFig       = [];
%%
% Plot raw RT distributions for each subject
%
if (opts.plotRawRT)
    %%
    % All (test) trials
    if (opts.plotIndividual)
        if (exist('aaron_newfig', 'file'))
            mainFig = aaron_newfig;
        else
            mainFig = figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;
    end

    allRTs = [];

    % For each subject
    for subjIdx = 1:length(submat);
        testTrials                    = zeros(360, 1);
        firstTest = [101 281]+opts.testTrialOffset;
        lastTest  = [180 360];
        testTrials([firstTest(1):lastTest(1) firstTest(2):lastTest(2)]) = 1;
        if (size(pr{subj}, 1) < 360)
            testTrials                    = zeros(180, 1);
        %    testTrials([101:180 281:360]) = 1;
            testTrials([firstTest(1):lastTest(1)]) = 1;
        end
        testTrials                    = logical(testTrials);

        if (opts.plotIndividual)
            figure(mainFig);
            subplot(ceil(sqrt(length(submat))), ceil(sqrt(length(submat))), subjIdx);
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;
        end

        subj = submat(subjIdx);

        try
            tmpChk = pr{submat(subjIdx)}(1,1);

            % XXX: Aggressive?
%            assert(size(pr{submat(subjIdx)}, 1) == 360)
        catch
            if (opts.verbose)
                disp(['newharness: plotRawRT: Skipping subject ' num2str(subj)]);
            end

            submat(subjIdx) = -1;

            continue;
        end

        if (opts.verbose)
            disp(['newharness: plotRawRT: Opening subject ' num2str(subj)]);
        end

        theseTestTrials = testTrials(1:size(pr{submat(subjIdx)}, 1));

        notNans = ~isnan(pr{submat(subjIdx)}(:, 2));
        % Add to all-subject record
        allRTs = [allRTs ; pr{submat(subjIdx)}(notNans & theseTestTrials, 2)];

        if (opts.plotIndividual)
            hist(pr{submat(subjIdx)}(notNans & theseTestTrials, 2));

            if (~isempty(strfind(opts.dataDir, 'V6')))
                % Constant ISI for V6
                plot(ones(1, 50) * 1+cueDuration, 1:2:100, 'r--');
            else
                plot(ones(1, 50) * pr{submat(subjIdx)}(1, 9)+cueDuration, 1:2:100, 'r--');
            end
            axis([0 5 0 100]);
            set(gca, 'XTick', [1:5]);
            set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, [1:5], 'UniformOutput', false)));

            if (subjIdx == 1)
                title(['Raw RT, all test trials, subject ' num2str(submat(subjIdx))]);
            else
                title(['Subject ' num2str(submat(subjIdx))]);
            end
        end

        if (opts.verbose)
            disp(['newharness: plotRawRT: Subject ' num2str(subj) ...
                  ', mean RT = ' num2str(nanmean(pr{submat(subjIdx)}(theseTestTrials, 2)),3) ...
                  ', std = ' num2str(nanstd(pr{submat(subjIdx)}(theseTestTrials, 2)),3) ...
                  ', kurtosis = ' num2str(kurtosis(pr{submat(subjIdx)}(notNans & theseTestTrials, 2)),3)]);
        end
    end     % for subjIdx


    if (opts.plotIndividual)
        if (exist('aaron_newfig', 'file'))
            figSubjBySess = aaron_newfig;
        else
            figSubjBySess = figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        for subjIdx = 1:length(submat);
            if (submat(subjIdx) == -1)
                continue
            end
            for sessIdx = [1 2];
                if (sessIdx > 1 && size(pr{submat(subjIdx)}, 1) < 360)
                    continue;
                end

                subplot(length(submat), 2, ((subjIdx-1)*2)+sessIdx);
                set(gca, 'FontWeight', 'demi');
                set(gca, 'FontSize', 24);
                hold on;

                startTrial = [101 281];
                endTrial   = [180 360];
                RTs = [pr{submat(subjIdx)}(startTrial(sessIdx):endTrial(sessIdx), 2)];
                hist(RTs, ceil(max(RTs)/0.1));

                if (sessIdx == 1)
                    ylabel(num2str(submat(subjIdx)));
                end
            end
        end
    end

    % All subjects
    if (opts.plotGroup)
        if (exist('aaron_newfig', 'file'))
            aaron_newfig;
        else
            figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        binCounts = hist(allRTs, [0:0.1:5]);
        hist(allRTs, [0:0.1:5]);
        binCounts = max(binCounts)+10;
        axis([0 5 0 binCounts]);
        set(gca, 'XTick', [1:5]);
        set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, [1:5], 'UniformOutput', false)));
        xlabel('RT (sec from cue onset)');
        if (~isempty(strfind(opts.dataDir, 'V6')))
            % Constant ISI for V6
%           plot(ones(1, 250) * 1+cueDuration, 1:2:500, 'r--');
            plot(ones(1, round(binCounts/2)) * 1+cueDuration, 1:2:binCounts, 'r--');
        else
%           plot(ones(1, 250) * pr{submat(subjIdx)}(1, 9)+cueDuration, 1:2:500, 'r--');
            plot(ones(1, round(binCounts/2)) * pr{submat(subjIdx)}(1, 9)+cueDuration, 1:2:binCounts, 'r--');
        end
    end

    %%
    % For each memory bin
    for memBins = [0.5 0.6 0.7 0.8];
        if (opts.plotIndividual)
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;
        end

        for subjIdx = 1:length(submat);
            if (submat(subjIdx) == -1)
                continue;
            end
            testTrials                    = zeros(360, 1);
            testTrials([101:180 281:360]) = 1;
            if (size(pr{submat(subjIdx)}, 1) < 360)
                testTrials                    = zeros(180, 1);
                testTrials([101:180 281:360]) = 1;
            end
            testTrials                    = logical(testTrials);

            theseTestTrials = testTrials(1:size(pr{submat(subjIdx)}, 1));

            if (opts.plotIndividual)
                subplot(ceil(sqrt(length(submat))), ceil(sqrt(length(submat))), subjIdx);
                set(gca, 'FontWeight', 'demi');
                set(gca, 'FontSize', 24);
                hold on;
            end

            subj = submat(subjIdx);

            if (subj == -1)
                continue;
            end

            memMask = pr{submat(subjIdx)}(:, 1) == memBins | pr{submat(subjIdx)}(:, 1) == 1-memBins;
            notNans = ~isnan(pr{submat(subjIdx)}(:, 2));

            if (opts.plotIndividual)
                hist(pr{submat(subjIdx)}(notNans & memMask & theseTestTrials, 2));

                if (~isempty(strfind(opts.dataDir, 'V6')))
                    % Constant ISI for V6
                    plot(ones(1, 50) * 1+cueDuration, [1:2:100], 'r--');
                else
                    plot(ones(1, 50) * pr{submat(subjIdx)}(1, 9)+cueDuration, [1:2:100], 'r--');
                end

                axis([0 5 0 100]);
                set(gca, 'XTick', [1:5]);
                set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, [1:5], 'UniformOutput', false)));

                if (subjIdx == 1)
                    title(['Raw RT, cue p=' num2str(memBins) ...
                           ', subject ' num2str(submat(subjIdx))]);
                else
                    title(['Subject ' num2str(submat(subjIdx))]);
                end
            end

            if (opts.verbose)
                disp(['newharness: plotRawRT: Subject ' num2str(subj) ...
                      ', cue = ' num2str(memBins) ...
                      ', mean RT = ' num2str(nanmean(pr{submat(subjIdx)}(memMask & theseTestTrials, 2)),3) ...
                      ', std = ' num2str(nanstd(pr{submat(subjIdx)}(memMask & theseTestTrials, 2)),3) ...
                      ', kurtosis = ' num2str(kurtosis(pr{submat(subjIdx)}(notNans & memMask & theseTestTrials, 2)),3)]);
            end
        end
    end

    %%
    % For each perceptual bin
    for perBins = [0.65 0.85];
        if (opts.plotIndividual)
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;
        end

        for subjIdx = 1:length(submat);
            if (submat(subjIdx) == -1)
                continue;
            end
            testTrials                    = zeros(360, 1);
            testTrials([101:180 281:360]) = 1;
            if (size(pr{submat(subjIdx)}, 1) < 360)
                testTrials                    = zeros(180, 1);
                testTrials([101:180 281:360]) = 1;
            end
            testTrials                    = logical(testTrials);

            theseTestTrials = testTrials(1:size(pr{submat(subjIdx)}, 1));

            if (opts.plotIndividual)
                subplot(ceil(sqrt(length(submat))), ceil(sqrt(length(submat))), subjIdx);
                set(gca, 'FontWeight', 'demi');
                set(gca, 'FontSize', 24);
                hold on;
            end

            subj = submat(subjIdx);

            if (subj == -1)
                continue;
            end

            perMask = pr{submat(subjIdx)}(:, 6) == perBins;
            notNans = ~isnan(pr{submat(subjIdx)}(:, 2));

            if (opts.plotIndividual)
                hist(pr{submat(subjIdx)}(notNans & perMask & theseTestTrials, 2));
                if (~isempty(strfind(opts.dataDir, 'V6')))
                    % Constant ISI for V6
                    plot(ones(1, 50) * 1+cueDuration, [1:2:100], 'r--');
                else
                    plot(ones(1, 50) * pr{submat(subjIdx)}(1, 9)+cueDuration, [1:2:100], 'r--');
                end

                axis([0 5 0 100]);
                set(gca, 'XTick', [1:5]);
                set(gca, 'XTickLabel', cellstr(arrayfun(@num2str, [1:5], 'UniformOutput', false)));

                if (subjIdx == 1)
                    title(['Raw RT, coh=' num2str(perBins) ...
                           ', subject ' num2str(submat(subjIdx))]);
                else
                    title(['Subject ' num2str(submat(subjIdx))]);
                end
            end

            if (opts.verbose)
                disp(['newharness: plotRawRT: Subject ' num2str(subj) ...
                      ', coh = ' num2str(perBins) ...
                      ', mean RT = ' num2str(nanmean(pr{submat(subjIdx)}(perMask & theseTestTrials, 2)),3) ...
                      ', std = ' num2str(nanstd(pr{submat(subjIdx)}(perMask & theseTestTrials, 2)),3) ...
                      ', kurtosis = ' num2str(kurtosis(pr{submat(subjIdx)}(notNans & perMask & theseTestTrials, 2)),3)]);
            end
        end
    end
end

%%
% Plot raw RT distributions for each subject
%
if (opts.plotStimLockedRT)
    histLeft  = -8-cueDuration; % NB calculate max ISI from data
    histLeft  = round(histLeft);
    histRight = 4;

    %%
    % All (test) trials
    if (opts.plotIndividual)
        if (exist('aaron_newfig', 'file'))
            aaron_newfig;
        else
            figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;
    end

    testTrials                    = zeros(360, 1);
    firstTest = [101 281]+opts.testTrialOffset;
    lastTest  = [180 360];
    testTrials([firstTest(1):lastTest(1) firstTest(2):lastTest(2)]) = 1;
    if (size(pr{subj}, 1) < 360)
        testTrials                    = zeros(180, 1);
        testTrials([firstTest(1):lastTest(1)]) = 1;
    end
    testTrials                    = logical(testTrials);

    allRTs = [];

    % For each subject
    for subjIdx = 1:length(submat);
        if (opts.plotIndividual)
            subplot(ceil(sqrt(length(submat))), ceil(sqrt(length(submat))), subjIdx);
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;
        end

        subj = submat(subjIdx);

        try
            tmpChk = pr{submat(subjIdx)}(1,1);

            % XXX: Aggressive?
            assert(size(pr{submat(subjIdx)}, 1) == 360)
        catch
            if (opts.verbose)
                disp(['newharness: plotStimLockedRT: Skipping subject ' num2str(subj)]);
            end

            submat(subjIdx) = -1;

            continue;
        end

        if (opts.verbose)
            disp(['newharness: plotStimLockedRT: Opening subject ' num2str(subj)]);
        end

        theseTestTrials = testTrials(1:size(pr{subj}, 1));

        notNans  = ~isnan(pr{subj}(:, 2));
        % Produces a stimulus-locked RT; negative when RT < ISI
        theseRTs = pr{subj}(notNans & theseTestTrials, 2) - pr{subj}(notNans & theseTestTrials, 9) - cueDuration;
        % Add to all-subject record
        allRTs = [allRTs ; theseRTs];

        if (opts.plotIndividual)
            hist(theseRTs);

            % Plot stimulus onset time at t=0
            plot(zeros(1, 50), 1:2:100, 'r--');
            axis([histLeft histRight 0 100]);
            set(gca, 'XTick', [histLeft:histRight]);
            set(gca, 'XTickLabel', ...
                     cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));

            if (subjIdx == 1)
                title(['Stimulus-locked RT, all test trials, subject ' num2str(submat(subjIdx))]);
            else
                title(['Subject ' num2str(submat(subjIdx))]);
            end
        end

        if (opts.verbose)
            disp(['newharness: plotStimLockedRT: Subject ' num2str(subj) ...
                  ', mean RT = ' num2str(nanmean(theseRTs)) ...
                  ', std = ' num2str(nanstd(theseRTs)) ...
                  ', kurtosis = ' num2str(kurtosis(theseRTs))]);
        end
    end

    % All subjects
    if (opts.plotGroup)
        if (exist('aaron_newfig', 'file'))
            aaron_newfig;
        else
            figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        binCounts = hist(allRTs, [histLeft:0.1:histRight]);
        binCounts = max(binCounts)+10;
        hist(allRTs, [histLeft:0.1:histRight]);
        axis([histLeft histRight 0 binCounts]);
        set(gca, 'XTick', [histLeft:histRight]);
        set(gca, 'XTickLabel', ...
                 cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));
        xlabel('RT (sec from stim onset)');
        plot(zeros(1, round(binCounts/2)), 1:2:binCounts, 'r--');
    end

    %%
    % For testing ghootstrapping
    % subj by cue strength by trials
    allRTsAllCues = cell(length(submat), 4, 2);

    %%
    % For each memory bin
    for memBins = cueList;
        if (opts.plotIndividual)
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;
        end

        allBinnedRTs = [];

        for subjIdx = 1:length(submat);
            if (opts.plotIndividual)
                subplot(ceil(sqrt(length(submat))), ceil(sqrt(length(submat))), subjIdx);
                set(gca, 'FontWeight', 'demi');
                set(gca, 'FontSize', 24);
                hold on;
            end

            subj = submat(subjIdx);

            if (subj == -1)
                continue;
            end

            memMask = pr{subj}(:, 1) == memBins | pr{subj}(:, 1) == 1-memBins;
            notNans = ~isnan(pr{subj}(:, 2));
            theseRTs = pr{subj}(notNans & memMask & theseTestTrials, 2) - pr{subj}(notNans & memMask & theseTestTrials, 9) - cueDuration;
            allBinnedRTs = [allBinnedRTs ; theseRTs];
            earlyRTs = theseRTs(theseRTs<=0);
            allRTsAllCues{subjIdx, find(memBins==cueList), 1} = earlyRTs;
            allRTsAllCues{subjIdx, find(memBins==cueList), 2} = ones(length(earlyRTs), 1)*memBins;

            if (opts.plotIndividual)
                hist(theseRTs);

                plot(zeros(1, 50), 1:2:100, 'r--');
                axis([histLeft histRight 0 100]);
                set(gca, 'XTick', [histLeft:histRight]);
                set(gca, 'XTickLabel', ...
                         cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));

                if (subjIdx == 1)
                    title(['Stimulus-locked RT, cue p=' num2str(memBins) ...
                           ', subject ' num2str(submat(subjIdx))]);
                else
                    title(['Subject ' num2str(submat(subjIdx))]);
                end
            end

            if (opts.veryverbose)
                disp(['newharness: plotStimLockedRT: Subject ' num2str(subj) ...
                      ', cue = ' num2str(memBins) ...
                      ', mean RT = ' num2str(nanmean(theseRTs)) ...
                      ', std = ' num2str(nanstd(theseRTs)) ...
                      ', kurtosis = ' num2str(kurtosis(theseRTs))]);
            end

            earlyByCueBySubj(subjIdx, find(memBins==cueList)) = sum(theseRTs<=0)/length(theseRTs);
            if (opts.veryverbose)
                disp(['newharness: plotStimLockedRT: Subject ' num2str(submat(subjIdx)) ...
                      ', cue=' num2str(memBins) ...
                      ', fraction early responses=' num2str(earlyByCueBySubj(subjIdx, find(memBins==cueList)))]);
            end
        end % for subjIdx

        % Plot combined histogram
        if (opts.plotGroup)
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);

            binCounts = hist(allBinnedRTs, [histLeft:0.1:histRight]);
            binCounts = max(binCounts)+10;
            % for presentations want a uniform plot height across all quantities
            if (~isempty(strfind(opts.dataDir, 'V6')))
                binCounts = 100;
            else
                binCounts = 30;         % SfN 2015 poster @ max(submat)=20
            end
            hist(allBinnedRTs, [histLeft:0.1:histRight]);
            axis([histLeft histRight 0 binCounts]);
            set(gca, 'XTick', [histLeft:histRight]);
            set(gca, 'XTickLabel', ...
                     cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));
            xlabel('RT (sec from stim onset)');
            plot(zeros(1, round(binCounts/2)), 1:2:binCounts, 'r--');
            title(['Stim-Locked RT, all subjects, cue p=' num2str(memBins) ...
                   ' (' num2str(sum(allBinnedRTs<=0)) ...
                    '/' num2str(length(allBinnedRTs)) ...
                    ' early responses)']);
        end
        if (opts.verbose)
            disp(['newharness: plotStimLockedRT: cue p=' num2str(memBins) ...
                  ', ' num2str(sum(allBinnedRTs<=0)) ...
                  ' early responses, ' ...
                  num2str(sum(allBinnedRTs>0)) ...
                  ' late responses']);
        end

        earlyByCue(find(memBins==cueList)) = sum(allBinnedRTs<=0)/length(allBinnedRTs);
        disp(['cue=' num2str(memBins) ...
              ', fraction early responses=' num2str(earlyByCue(find(memBins==cueList)))]);
    end % for memBins

    % 160 test trials
    savedRTsByCues = NaN(length(submat), 160, 2);
    for subjIdx = 1:length(submat);
        for sliceIdx = 1:2;
            tmp     =   [allRTsAllCues{subjIdx, 1, sliceIdx} ; allRTsAllCues{subjIdx, 2, sliceIdx} ; allRTsAllCues{subjIdx, 3, sliceIdx} ; allRTsAllCues{subjIdx, 4, sliceIdx}];
            savedRTsByCues(subjIdx, :, sliceIdx) = [tmp ; NaN(size(savedRTsByCues,2)-length(tmp), 1)];
        end
    end
    save('savedRTsByCues.mat', 'savedRTsByCues');
    if (opts.verbose)
        if (exist('ghootstrap', 'file'))
            [p r] = ghootstrap(savedRTsByCues);
            disp(['newharness: plotStimLockedRT: Cue reliability correlated with early RT, ghootstrap R=' ...
                 num2str(nanmean(r), '%.2f') ...
                 ', p=' ...
                 num2str(p, '%.2d')]);
        end
    end

    if (opts.plotGroup)
        if (exist('aaron_newfig', 'file'))
            aaron_newfig;
        else
            figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        for cueIdx = 1:length(cueList);
            bar(cueIdx, nanmean(earlyByCueBySubj(:, cueIdx)), 'FaceColor', [0.8 0.8 0.8]);
            errorbar(cueIdx, nanmean(earlyByCueBySubj(:, cueIdx)), nanstd(earlyByCueBySubj(:, cueIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
        end

        set(gca, 'XTick', [1:length(cueList)]);
        set(gca, 'XTickLabel', ...
                  cellstr(arrayfun(@num2str, cueList, 'UniformOutput', false)));
        xlabel('Cue strength');
        ylabel('Proportion early responses');
    end

    [B,BINT,R,RINT,STATS] = regress([1:length(cueList)]', [ones(1, length(cueList)) ; nanmean(earlyByCueBySubj)]');
    disp(['newharness: plotStimLockedRT: Early responses increase with cue strength: ' num2str([B(2) STATS(2) STATS(3)])]);

    for subjIdx = 1:length(submat);
        B = regress([1:length(cueList)]', [ones(1, length(cueList)) ; earlyByCueBySubj(subjIdx, :)]');
        cueB(subjIdx) = B(2);
    end

    [h, p] = ttest(cueB);
    disp(['newharness: plotStimLockedRT: ... effect reliable within-subjects at: ' num2str(p)]);

    %%
    % For each perceptual bin
    allRTsAllCohs = cell(length(submat), 2, 2);

    for perBins = perList;
        if (opts.plotIndividual)
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;
        end

        allBinnedRTs = [];

        for subjIdx = 1:length(submat);
            if (opts.plotIndividual)
                subplot(ceil(sqrt(length(submat))), ceil(sqrt(length(submat))), subjIdx);
                set(gca, 'FontWeight', 'demi');
                set(gca, 'FontSize', 24);
                hold on;
            end

            subj = submat(subjIdx);

            if (subj == -1)
                continue;
            end

            perMask = pr{subj}(:, 6) == perBins;
            notNans = ~isnan(pr{subj}(:, 2));
            theseRTs = pr{subj}(notNans & perMask & theseTestTrials, 2) - pr{subj}(notNans & perMask & theseTestTrials, 9) - cueDuration;
            allBinnedRTs = [allBinnedRTs ; theseRTs];
            earlyRTs = theseRTs(theseRTs<=0);
            allRTsAllCohs{subjIdx, find(perBins==perList), 1} = earlyRTs;
            allRTsAllCohs{subjIdx, find(perBins==perList), 2} = ones(length(earlyRTs), 1)*memBins;

            if (opts.plotIndividual)
                hist(theseRTs);

                plot(zeros(1, 50), [1:2:100], 'r--');
                axis([histLeft histRight 0 100]);
                set(gca, 'XTick', [histLeft:histRight]);
                set(gca, 'XTickLabel', ...
                         cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));

                if (subjIdx == 1)
                    title(['Stim-locked RT, coh=' num2str(perBins) ...
                           ', subject ' num2str(submat(subjIdx))]);
                else
                    title(['Subject ' num2str(submat(subjIdx))]);
                end
            end

            if (opts.veryverbose)
                disp(['newharness: plotStimLockedRT: Subject ' num2str(subj) ...
                      ', coh = ' num2str(perBins) ...
                      ', mean RT = ' num2str(nanmean(theseRTs)) ...
                      ', std = ' num2str(nanstd(theseRTs)) ...
                      ', kurtosis = ' num2str(kurtosis(theseRTs))]);
            end

            earlyByPerBySubj(subjIdx, find(perBins==perList)) = sum(theseRTs<=0)/length(theseRTs);
            if (opts.veryverbose)
                disp(['newharness: plotStimLockedRT: Subject ' num2str(submat(subjIdx)) ...
                      ', coh=' num2str(perBins) ...
                      ', fraction early responses=' num2str(earlyByPerBySubj(subjIdx, find(perBins==perList)))]);
            end
        end % for subjIdx

        % Plot combined histogram
        if (opts.plotGroup)
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);

            binCounts = hist(allBinnedRTs, [histLeft:0.1:histRight]);
            binCounts = max(binCounts)+10;
            binCounts = 50;                         % SfN 2015 poster @ max(submat)=20
            hist(allBinnedRTs, [histLeft:0.1:histRight]);
            axis([histLeft histRight 0 binCounts]);
            set(gca, 'XTick', [histLeft:histRight]);
            set(gca, 'XTickLabel', ...
                     cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));
            xlabel('RT (sec from stim onset)');
            plot(zeros(1, round(binCounts/2)), 1:2:binCounts, 'r--');
            title(['Stim-Locked RT, all subjects, coh=' num2str(perBins) ...
                   ' (' num2str(sum(allBinnedRTs<=0)) ...
                    '/' num2str(length(allBinnedRTs)) ...
                    ' early responses)']);
        end

        earlyByPer(find(perBins==perList)) = sum(allBinnedRTs<=0)/length(allBinnedRTs);
        disp(['coh=' num2str(perBins) ...
              ', fraction early responses=' num2str(earlyByPer(find(perBins==perList)))]);
    end % for perBins

    % 160 test trials
    savedRTsByCohs = NaN(length(submat), 160, 2);
    for subjIdx = 1:length(submat);
        for sliceIdx = 1:2;
            tmp     =   [allRTsAllCohs{subjIdx, 1, sliceIdx} ; allRTsAllCohs{subjIdx, 2, sliceIdx}];
            savedRTsByCohs(subjIdx, :, sliceIdx) = [tmp ; NaN(size(savedRTsByCohs,2)-length(tmp), 1)];
        end
    end
    save('savedRTsByCohs.mat', 'savedRTsByCohs');
    if (opts.verbose)
        if (exist('ghootstrap', 'file'))
            [p r] = ghootstrap(savedRTsByCohs);
            disp(['newharness: plotStimLockedRT: Perceptual coherence correlated with early RT, ghootstrap R=' ...
                 num2str(nanmean(r), '%.2f') ...
                 ', p=' ...
                 num2str(p, '%.2d')]);
        end
    end

    if (opts.plotGroup)
        if (exist('aaron_newfig', 'file'))
            aaron_newfig;
        else
            figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        for perIdx = 1:length(perList);
            bar(perIdx, nanmean(earlyByPerBySubj(:, perIdx)), 'FaceColor', [0.8 0.8 0.8]);
            errorbar(perIdx, nanmean(earlyByPerBySubj(:, perIdx)), nanstd(earlyByPerBySubj(:, perIdx))/sqrt(length(submat)), 'ko', 'LineWidth', 4);
        end

        set(gca, 'XTick', [1:length(perList)]);
        set(gca, 'XTickLabel', ...
                  cellstr(arrayfun(@num2str, perList, 'UniformOutput', false)));
        xlabel('Cued perceptual coherence');
        ylabel('Proportion early responses');
    end

    [h,p] = ttest(earlyByPerBySubj(:, 1), earlyByPerBySubj(:, 2));
    disp(['newharness: plotStimLockedRT: Early responses decrease with perceptual coherence: ' num2str(p)]);

    for subjIdx = 1:length(submat);
        B = regress([1:length(perList)]', [ones(1, length(perList)) ; earlyByPerBySubj(subjIdx, :)]');
        perB(subjIdx) = B(2);
    end

    [h, p] = ttest(perB);
    disp(['newharness: plotStimLockedRT: ... effect reliable within-subjects at: ' num2str(p)]);

    %%
    % For each ISI

    % Assemble list of unique ISIs used in this dataset
    isiList = [];
    for subj = submat;
        if (subj == -1) continue; end
        isiList = [isiList pr{subj}(theseTestTrials, 9)];
    end
    isiList = unique(isiList);
    isiList = isiList(isiList~=0);

    if (length(isiList) == 1)
        % Before variable ISI, don't need to plot this
        % XXX: This assumes plotStimLockedRT is the last option. May need a
        % goto in the future.
        return;
    end
    for isiIdx = 1:length(isiList);
        isiBins = isiList(isiIdx);
        if (opts.plotIndividual)
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;
        end

        allBinnedRTs = [];

        for subjIdx = 1:length(submat);
            if (opts.plotIndividual)
                subplot(ceil(sqrt(length(submat))), ceil(sqrt(length(submat))), subjIdx);
                set(gca, 'FontWeight', 'demi');
                set(gca, 'FontSize', 24);
                hold on;
            end

            subj = submat(subjIdx);

            if (subj == -1)
                continue;
            end

            isiMask = pr{subj}(:, 9) == isiBins;
            notNans = ~isnan(pr{subj}(:, 2));
            theseRTs = pr{subj}(notNans & isiMask & theseTestTrials, 2) - pr{subj}(notNans & isiMask & theseTestTrials, 9) - cueDuration;
            allBinnedRTs = [allBinnedRTs ; theseRTs];

            if (opts.plotIndividual)
                hist(theseRTs);

                plot(zeros(1, 50), [1:2:100], 'r--');
                axis([histLeft histRight 0 100]);
                set(gca, 'XTick', [histLeft:histRight]);
                set(gca, 'XTickLabel', ...
                         cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));

                if (subjIdx == 1)
                    title(['Stim-locked RT, ISI=' num2str(isiBins) ...
                           ', subject ' num2str(submat(subjIdx))]);
                else
                    title(['Subject ' num2str(submat(subjIdx))]);
                end
            end

            if (opts.verbose)
                disp(['newharness: plotStimLockedRT: Subject ' num2str(subj) ...
                      ', isi = ' num2str(isiBins) ...
                      ', mean RT = ' num2str(nanmean(theseRTs)) ...
                      ', std = ' num2str(nanstd(theseRTs)) ...
                      ', kurtosis = ' num2str(kurtosis(theseRTs))]);
            end

            earlyByISIBySubj(subjIdx, find(isiBins==isiList)) = sum(theseRTs<=0)/length(theseRTs);
            if (opts.verbose)
                disp(['newharness: plotStimLockedRT: Subject ' num2str(submat(subjIdx)) ...
                      ', isi=' num2str(isiBins) ...
                      ', fraction early responses=' num2str(earlyByISIBySubj(subjIdx, find(isiBins==isiList)))]);
            end

        end % for subjIdx

        if (opts.plotGroup)
            % Plot combined histogram
            if (exist('aaron_newfig', 'file'))
                aaron_newfig;
            else
                figure
            end
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);

            binCounts = hist(allBinnedRTs, [histLeft:0.1:histRight]);
            binCounts = max(binCounts)+10;
            binCounts = 50;                         % SfN 2015 poster @ max(submat)=20
            hist(allBinnedRTs, [histLeft:0.1:histRight]);
            axis([histLeft histRight 0 binCounts]);
            set(gca, 'XTick', [histLeft:histRight]);
            set(gca, 'XTickLabel', ...
                     cellstr(arrayfun(@num2str, [histLeft:histRight], 'UniformOutput', false)));
            xlabel('RT (sec from stim onset)');
            plot(zeros(1, round(binCounts/2)), 1:2:binCounts, 'r--');
            title(['Stim-Locked RT, all subjects, ISI=' num2str(isiBins) ...
                   ' (' num2str(sum(allBinnedRTs<=0)) ...
                    '/' num2str(length(allBinnedRTs)) ...
                    ' early responses)']);
        end

        earlyByISI(find(isiBins==isiList)) = sum(allBinnedRTs<=0)/length(allBinnedRTs);
        disp(['ISI=' num2str(isiBins) ...
              ', fraction early responses=' num2str(earlyByISI(find(isiBins==isiList)))]);
    end % for isiIdx

    if (opts.plotGroup)
        if (exist('aaron_newfig', 'file'))
            aaron_newfig;
        else
            figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        for isiIdx = 1:length(isiList);
            bar(isiIdx, nanmean(earlyByISIBySubj(:, isiIdx)), 'FaceColor', [0.8 0.8 0.8]);
            errorbar(isiIdx, nanmean(earlyByISIBySubj(:, isiIdx)), nanstd(earlyByISIBySubj(:, isiIdx))/sqrt(length(earlyByISIBySubj)), 'ko', 'LineWidth', 4);
        end

        set(gca, 'XTick', [1:length(isiList)]);
        set(gca, 'XTickLabel', ...
                  cellstr(arrayfun(@num2str, isiList, 'UniformOutput', false)));
        xlabel('ISI');
        ylabel('Proportion early responses');
    end

    [B,BINT,R,RINT,STATS] = regress([1:length(isiList)]', [ones(1, length(isiList)) ; nanmean(earlyByISIBySubj)]');
    disp(['newharness: plotStimLockedRT: Early responses increase with ISI length: ' num2str([B(2) STATS(2) STATS(3)])]);

    for subjIdx = 1:length(submat);
        B = regress([1:length(isiList)]', [ones(1, length(isiList)) ; earlyByISIBySubj(subjIdx, :)]');
        isiB(subjIdx) = B(2);
    end

    [h, p] = ttest(isiB);
    disp(['newharness: plotStimLockedRT: ... effect reliable within-subjects at: ' num2str(p)]);

end


%%
% Plot beta weights relating memory and perceptual evidence to RT, in each coh/cue bin
%
% XXX: Could we use this on the variable-ISI version to find evidence for sample-time weighting of evidence?
%   (slower has greater effect if fast has low coherence; IOW that timePerStep varies between processes.
%

if (opts.plotRegressions)
    % Parse RTs, memLevel, and perLevel by correct/incorrect trials.
    % 1 = incorrect, 2 = correct
    for subjIdx = 1:length(submat);
        RT       = cell(2, 1);
        memLevel = cell(2, 1);
        perLevel = cell(2, 1);
        cohLevel = cell(2, 1);

        % Perform RT regressions separately for correct (2) and incorrect (1) trials
        for accIdx = 1:2;
            corrMask = [pr{submat(subjIdx)}(:,3) == accIdx-1];

            % ... and for early (1) v late (2)
            for earlyIdx = 1:2;
                % NB probably a clever one-line way to do this but I just can't right now.
                if (earlyIdx == 1)
                    % Early (RT < ISI)
                    earlyMask                              = [pr{submat(subjIdx)}(:, 2)] <  [pr{submat(subjIdx)}(:, 9)];
                else
                    % Late (RT >= ISI)
                    earlyMask                              = [pr{submat(subjIdx)}(:, 2)] >= [pr{submat(subjIdx)}(:, 9)];
                end

                trialMask = corrMask & earlyMask;

                if (opts.verbose)
                        trialType = '';
                        if (earlyIdx == 1)
                            trialType = [trialType 'early'];
                        else
                            trialType = [trialType 'late'];
                        end

                        if (accIdx == 1)
                            trialType = [trialType ', incorrect'];
                        else
                            trialType = [trialType ', correct'];
                        end

                        disp(['newharness: plotRegressions: Subject ' num2str(submat(subjIdx)) ...
                              ' ' trialType ...
                              ': ' num2str(sum(trialMask)) ...
                              ' trials.']);
                end

                if (sum(trialMask) < 10)    % 2
                    if (opts.verbose)
                        disp(['newharness: plotRegressions: Subject ' num2str(submat(subjIdx)) ...
                              ' ' trialType ...
                              ': Not enough trials, skipping...']);
                        continue;
                    end
                end

                RT{submat(subjIdx), accIdx, earlyIdx}       = [pr{submat(subjIdx)}(trialMask, 2)];
                memLevel{submat(subjIdx), accIdx, earlyIdx} = [pr{submat(subjIdx)}(trialMask, 1)];
                perLevel{submat(subjIdx), accIdx, earlyIdx} = [pr{submat(subjIdx)}(trialMask, 4)];    % 4 = observed evidence, 6 = cued coherence
%                cohLevel{submat(subjIdx), accIdx, earlyIdx} = [pr{submat(subjIdx)}(trialMask, 6)];    % 4 = observed evidence, 6 = cued coherence
% NB Response is perfectly confounded with predictability in each block
%                resps{submat(subjIdx), accIdx}    = [pr{submat(subjIdx)}(trialMask, 7)];

                B{submat(subjIdx), accIdx, earlyIdx}        = regress(RT{submat(subjIdx), accIdx, earlyIdx}, ...
                                                                      [ones(length(memLevel{submat(subjIdx), accIdx, earlyIdx}), 1), ...
                                                                      memLevel{submat(subjIdx), accIdx, earlyIdx}, ...
                                                                      perLevel{submat(subjIdx), accIdx, earlyIdx}]);
%                                                                      cohLevel{submat(subjIdx), accIdx, earlyIdx}]);
%                                                                      resps{submat(subjIdx), accIdx}]);
            end
        end
    end

    % Plot the regression results, for correct/incorrect trials and perceptual/memory evidence and early/late
    for earlyIdx = 1:2;
        aaron_newfig;
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        for accIdx = 1:2;
            subplot(2, 1, accIdx);
            set(gca, 'FontWeight', 'demi');
            set(gca, 'FontSize', 24);
            hold on;

            selB = squeeze([B{:, accIdx, earlyIdx}]);
            selB = selB(:, ~any(selB==0));

            % Plot each evidence type separately
            % 1 = memory evidence, 2 = perceptual evidence
            for evidenceType = 1:2;
                bar(evidenceType, mean(selB(evidenceType+1, :)), 'FaceColor', [0.8 0.8 0.8]);
                errorbar(evidenceType, mean(selB(evidenceType+1, :)), std(selB(evidenceType+1, :))/sqrt(size(selB, 2)), 'ko', 'LineWidth', 4);
                [h,p]=ttest(selB(evidenceType+1, :));

                if (opts.verbose)
                    trialType = '';
                    if (earlyIdx == 1)
                        trialType = [trialType 'early'];
                    else
                        trialType = [trialType 'late'];
                    end

                    if (accIdx == 1)
                        trialType = [trialType ', incorrect'];
                    else
                        trialType = [trialType ', correct'];
                    end

                    disp(['newharness: plotRegressions: ' trialType ...
                           ', evidence type=' ...
                           num2str(evidenceType+1) ': Beta=' ...
                           num2str(mean(selB(evidenceType+1, :))) ' (p=' ...
                           num2str(p, 2) ')']);
                end

                if (p<0.05)
                    plot(evidenceType, 1, 'k*', 'MarkerSize', 12, 'LineWidth', 2);
                elseif (p<0.1)
                    plot(evidenceType, 1, 'k^', 'MarkerSize', 12, 'LineWidth', 2);
                end
            end

            set(gca, 'XTick', [1:2]);
            set(gca, 'XTickLabel', {'', ''});
            if (accIdx == 1)
                titleStr = 'Incorrect trials';

                if (earlyIdx == 1)
                    titleStr = [titleStr ' (early)'];
                else
                    titleStr = [titleStr ' (late)'];
                end

                if (length(submat) == 1)
                    titleStr = [titleStr ' subject ' num2str(submat)];
                end
                title(titleStr);
                ylabel('\beta_{RT}');
            else
                title('Correct trials');
                set(gca, 'XTickLabel', {'  Memory  ', 'Perceptual'});
            end

            if (opts.savefigs)
                if (length(submat) == 1)
                    print('-depsc', '-r800', ['RTreg_sub' num2str(submat, '%.2d') '.eps']);
                else
                    print('-depsc', '-r800', ['RTreg_allsubj.eps']);
                end
            end


        end             % accIdx

    end                 % earlyIdx

end
