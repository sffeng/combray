function harness(submat, varargin)
% function harness(submat, ...)
%
%    plotCalib = 0;
%    plotRegression = 0;
%    plotPhase = 0;          % 0 = all trials; 1 = learning only; 2 = test only; 0.5 = early learn; 1.5 = late learn; 2.1 = early test; 2.2 = late test
%    earlyOnly = 0;
%    zAfter = 0;
%    dataDir = '../'
%    savefigs = 0;
%

[opts] = parse_args(varargin, ...
                    'plotCalib', false, ...
                    'plotRegression', false, ...
                    'plotPhase', 0, ...
                    'earlyOnly', 0, ...
                    'zAfter', false, ...
                    'dataDir', {'../'}, ...
                    'savefigs', false);

switch opts.plotPhase
    case 0
        disp('Plotting all trials');
    case 0.5
        disp('Plotting first half of learning trials only');
    case 1
        disp('Plotting learning trials only');
    case 1.5
        disp('Plotting second half of learning trials only');
    case 2
        disp('Plotting test trials only');
    case 2.1
        disp('Plotting first half of test trials only');
    case 2.2
        disp('Plotting second half of test trials only');
end

switch opts.earlyOnly
    case -1
        disp('Plotting post-stimulus responses only');
% case 0 all
    case 1
        disp('Plotting pre-stimulus responses only');
end

if (opts.zAfter)
    disp('Z-scoring within-subject, within-type');
else
    disp('Z-scoring within-subject only');
end

if (iscell(opts.dataDir))
    opts.dataDir = opts.dataDir{:};
end

disp(['Using dir ' opts.dataDir]);

datafiles = dir([opts.dataDir '*.txt']);

if (~exist('submat', 'var') || isempty(submat))
    submat   = [1:length(datafiles)];
end

clear acc;
clear RT

levels=[0.1:0.1:0.9];

iRT = cell(1, length(levels));
cRT = cell(1, length(levels));
acc = cell(1, length(levels));

piRT = cell(1, length(levels));
pcRT = cell(1, length(levels));
pacc = cell(1, length(levels));

for k = 1:length(levels);
    iRT{k} = [];
    cRT{k} = [];
    acc{k} = [];

    piRT{k} = [];
    pcRT{k} = [];
    pacc{k} = [];
end

memBins = [0:0.2:1];

for subj = submat;
    try
        pr = plotSubj(subj, opts.plotPhase, 0, opts.earlyOnly, opts.zAfter, opts.dataDir);
    catch
        continue
    end

    for k = 2:length(memBins)-1;
        % First by memory noise level
        iRT{k-1}  = [iRT{k-1}; pr(pr(:,1)>=memBins(k-1) & pr(:,1)<memBins(k+1) & pr(:,3)==0, 2)];        % NB: Matlab is very capricious when it comes to comparing small numbers
        cRT{k-1}  = [cRT{k-1}; pr(pr(:,1)>=memBins(k-1) & pr(:,1)<memBins(k+1) & pr(:,3)==1, 2)];        % NB: Matlab is very capricious when it comes to comparing small numbers
        acc{k-1}  = [acc{k-1}; pr(pr(:,1)>=memBins(k-1) & pr(:,1)<memBins(k+1), 3)];                     % e.g. pr(:,1) == 0.3 wouldn't actually pull up all 0.3?
    end

    for k = 2:length(levels)-1;
        pmask = ~isnan(pr(:,4));
        % And now by perceptual noise level
        piRT{k-1} = [piRT{k-1}; pr(pmask & pr(:,4)>levels(k-1) & pr(:,4)<levels(k+1) & pr(:,3)==0, 2)];
        pcRT{k-1} = [pcRT{k-1}; pr(pmask & pr(:,4)>levels(k-1) & pr(:,4)<levels(k+1) & pr(:,3)==1, 2)];
        pacc{k-1} = [pacc{k-1}; pr(pmask & pr(:,4)>levels(k-1) & pr(:,4)<levels(k+1), 3)];
    end
end


%% Memory noise
if (exist('aaron_newfig', 'file'))
    aaron_newfig;
else
    figure
end
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
hold on;

for k = 1:length(memBins);
    if (~length(cRT{k}))
        continue;
    end

    subplot(3,1,1);
    hold on;
    bar(k, nanmean(iRT{k}), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(k, nanmean(iRT{k}), nanstd(iRT{k})/sqrt(sum(~isnan(iRT{k}))), 'ko', 'LineWidth', 4);

    subplot(3,1,2);
    hold on;
    bar(k, nanmean(cRT{k}), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(k, nanmean(cRT{k}), nanstd(cRT{k})/sqrt(sum(~isnan(cRT{k}))), 'ko', 'LineWidth', 4);

    subplot(3,1,3);
    hold on;
    bar(k, nanmean(acc{k}), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(k, nanmean(acc{k}), nanstd(acc{k})/sqrt(sum(~isnan(acc{k}))), 'ko', 'LineWidth', 4);
end

subplot(3,1,1);
set(gca, 'XTick', [1 2 3 4]);
set(gca, 'XTickLabel', {[memBins(2:end-1)]});
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
title('zRT Incorrect Trials');

subplot(3,1,2);
set(gca, 'XTick', [1 2 3 4]);
set(gca, 'XTickLabel', {[memBins(2:end-1)]});
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
title('zRT Correct Trials');

subplot(3,1,3);
set(gca, 'XTick', [1 2 3 4]);
set(gca, 'XTickLabel', {[memBins(2:end-1)]});
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
title('Accuracy');

%% Perceptual noise
if (exist('aaron_newfig', 'file'))
    aaron_newfig;
else
    figure
end
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
hold on;

for k = 1:length(levels);
    if (~length(pcRT{k}))
        continue;
    end

    subplot(3,1,1);
    hold on;
    bar(k, nanmean(piRT{k}), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(k, nanmean(piRT{k}), nanstd(piRT{k})/sqrt(sum(~isnan(piRT{k}))), 'ko', 'LineWidth', 4);

    subplot(3,1,2);
    hold on;
    bar(k, nanmean(pcRT{k}), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(k, nanmean(pcRT{k}), nanstd(pcRT{k})/sqrt(sum(~isnan(pcRT{k}))), 'ko', 'LineWidth', 4);

    subplot(3,1,3);
    hold on;
    bar(k, nanmean(pacc{k}), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(k, nanmean(pacc{k}), nanstd(pacc{k})/sqrt(sum(~isnan(pacc{k}))), 'ko', 'LineWidth', 4);
end

subplot(3,1,1);
set(gca, 'XTickLabel', {[levels(1:end)]});
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
title('zRT Incorrect Trials');

subplot(3,1,2);
set(gca, 'XTickLabel', {[levels(1:end)]});
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
title('zRT Correct Trials');

subplot(3,1,3);
set(gca, 'XTickLabel', {[levels(1:end)]});
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
title('Accuracy');

%% Calibration data
%
% For each subject, for each calibration session, plot the threshold over time and the smoothed accuracy rate against the target accuracy rate
%
% Save the total performance for all subjects at each calibration level, and average
%
% smoothingWindow = 5; % XXX

if (opts.plotCalib)

calibPerf = cell(2, 1);
calibCoh  = cell(2, 1);

sd = [];

for subj = submat;
    calibFiles = dir([opts.dataDir 'combray_calib_' num2str(subj, '%.2d') '_*.mat']);

    try
        sd = loadSubj(subj, opts.dataDir);
    catch
        continue;
    end

    if (isempty(sd.params.perNoise))
        perNoise = [0 0];
    else
        perNoise = sd.params.perNoise;
    end

    for calibIdx = 1:length(calibFiles);
        if (exist('aaron_newfig', 'file'))
            aaron_newfig;
        else
            figure
        end
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        cfd = load([opts.dataDir calibFiles(calibIdx).name]);

        plotLen = length(cfd.response(1, :));

        for stairIdx = 1:size(cfd.response, 1);
            subplot(2, 2, stairIdx);
            set(gca, 'FontWeight', 'demi');
            hold on;

            perNoise = cfd.p_stair(ceil(stairIdx/2));

%            plot([1:plotLen], ones(1, plotLen)*perNoise(calibIdx), 'g--', 'LineWidth', 2);
            plot([1:plotLen], ones(1, plotLen)*perNoise, 'g--', 'LineWidth', 2);
            stairPerf = cumsum(cfd.response(stairIdx, :))./[1:plotLen];
            plot(stairPerf, 'r--', 'LineWidth', 2);
            plot(cfd.tTest(stairIdx, :), 'k--', 'LineWidth', 2);

%            if (plotLen == 60)
                % Only save for final version
%                stairPerf = [zeros(1, 60-plotLen) stairPerf];
%                saveCalibIdx = ((perNoise(calibIdx) == 0.85)+1);              % 1 = 0.65, 2 = 0.85
                saveCalibIdx = ((perNoise == 0.85)+1);              % 1 = 0.65, 2 = 0.85
                calibPerf{saveCalibIdx} = [calibPerf{saveCalibIdx} ; stairPerf];
                calibCoh{saveCalibIdx}  = [calibCoh{saveCalibIdx} ; cfd.tTest(stairIdx, :)];
%            end

            if (stairIdx == 1)
                legend('Target', 'Accuracy', 'Coherence', 'Location', 'SouthWest');
            end

            axis([0 plotLen 0 1]);
            set(gca, 'YTick', [0.2 0.4 0.6 0.8]);
        end

        if (opts.savefigs)
            print('-dpng', '-r800', ['sub' num2str(subj, '%.2d') ...
                                     '_calib_' num2str(calibIdx) '.png']);
        end
    end % for calibIdx
end % for submat

% Plot averaged calibration performance
%if (~isempty(calibPerf{1}))
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    accTarg = [0.65 0.85];
    for calibIdx = [1:2];
        subplot(2, 1, calibIdx);
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        errorbar(mean(calibPerf{calibIdx}), std(calibPerf{calibIdx})/sqrt(size(calibPerf{calibIdx},1)), 'ro', 'LineWidth', 4);
        plot([1:size(calibPerf{calibIdx}, 2)], ones(1, size(calibPerf{calibIdx}, 2))*accTarg(calibIdx), 'g--', 'LineWidth', 4);

        errorbar(mean(calibCoh{calibIdx}), std(calibCoh{calibIdx})/sqrt(size(calibPerf{calibIdx},1)), 'ko', 'LineWidth', 4);

        if (calibIdx == 1)
            title('Average calibration performance');
%            axis([0 size(calibPerf{calibIdx}, 2) 0.4 0.75]);
            axis([0 size(calibPerf{calibIdx}, 2) 0 1]);
        else
            xlabel('Trial');
            legend('Cumulative accuracy', 'Target', 'Coherence', 'Location', 'SouthEast');
            axis([0 size(calibPerf{calibIdx}, 2) 0 1]);
        end

    end

%end

end % if (opts.plotCalib)

if (opts.plotRegression)

%% Regression
% For all test trials, regress perceptual noise and memory noise on (correct, incorrect) reaction time
%   - For perceptual noise, use both observed fraction and block noise
%
% Plot bargraph with each regression coefficient and error bars
%

if (exist('aaron_newfig', 'file'))
    aaron_newfig;
else
    figure
end
set(gca, 'FontWeight', 'demi');
set(gca, 'FontSize', 24);
hold on;
clear B;

% Parse RTs, memNoise, and perNoise by correct/incorrect trials.
% 1 = incorrect, 2 = correct
for subj = submat;
    RT       = cell(2, 1);
    memNoise = cell(2, 1);
    perNoise = cell(2, 1);

    try
        pr = plotSubj(subj, 2, 0, 0, 1, opts.dataDir);
    catch
        continue
    end

    for corrIdx = 1:2;
        corrMask = [pr(:,3) == corrIdx-1];

        RT{subj, corrIdx}       = [pr(corrMask, 2)];
        memNoise{subj, corrIdx} = [pr(corrMask, 1)];
        perNoise{subj, corrIdx} = [pr(corrMask, 4)];
        resps{subj, corrIdx}    = [pr(corrMask, 7)];

% XXX: regress out nuisance vars - trial number? perseveration? individual finger effects?
%        B{subj, corrIdx}        = regress(RT{subj, corrIdx}, [ones(length(memNoise{subj, corrIdx}), 1), memNoise{subj, corrIdx}, perNoise{subj, corrIdx}, memNoise{subj, corrIdx}.*perNoise{subj, corrIdx}]);

        B{subj, corrIdx} = regress(RT{subj, corrIdx}, [ones(length(memNoise{subj, corrIdx}), 1), memNoise{subj, corrIdx}, ...
                                   perNoise{subj, corrIdx}, resps{subj, corrIdx}]);

% , memNoise{subj, corrIdx}.*perNoise{subj, corrIdx}]);
    end
end

interB = B;

for corrIdx = 1:2;
    subplot(2, 1, corrIdx);
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    selB = [B{:, corrIdx}];

    for noiseType = 1:2;
        bar(noiseType, mean(selB(noiseType+1, :)), 'FaceColor', [0.8 0.8 0.8]);
        errorbar(noiseType, mean(selB(noiseType+1, :)), std(selB(noiseType+1, :))/sqrt(size(selB, 2)), 'ko', 'LineWidth', 4);
        [h,p]=ttest(selB(noiseType+1, :));

        if (p<0.05)
            plot(noiseType, 1, 'k*', 'MarkerSize', 12, 'LineWidth', 2);
        elseif (p<0.1)
            plot(noiseType, 1, 'k^', 'MarkerSize', 12, 'LineWidth', 2);
        end
    end

%    xlabel('Noise type');

    set(gca, 'XTick', [1:2]);
    set(gca, 'XTickLabel', {'', ''});
    if (corrIdx == 1)
        titleStr = 'Incorrect trials';

        if (length(submat) == 1)
            titleStr = [titleStr ' subject ' num2str(submat)];
        end
        title(titleStr);
        ylabel('\beta_{RT}');
    else
        title('Correct trials');
        set(gca, 'XTickLabel', {'  Memory  ', 'Perceptual'});
    end

    if (opts.savefigs)
        print('-dpng', '-r800', ['sub' num2str(subj, '%.2d') '.png']);
    end
end


%% Regression by noise block type
% For all test trials, regress perceptual noise and memory noise on (correct, incorrect) reaction time
%   - For perceptual noise, use observed fraction, split by block noise
%
% Plot bargraph with each regression coefficient and error bars
%
clear B;
pLevels = [0.65 0.85];

% Parse RTs, memNoise, and perNoise by correct/incorrect trials.
% 1 = incorrect, 2 = correct
for subj = submat;
    RT       = cell(2, 1);
    memNoise = cell(2, 1);
    perNoise = cell(2, 1);

    try
        pr = plotSubj(subj, 2, 0, 0, 1, opts.dataDir);
    catch
        continue
    end

    for corrIdx = 1:2;
        corrMask = [pr(:,3) == corrIdx-1];

        for pIdx = 1:2;
            pMask = [pr(:, 6) == pLevels(pIdx)] & corrMask;

            RT{corrIdx, pIdx}       = [pr(pMask, 2)];
            memNoise{corrIdx, pIdx} = [pr(pMask, 1)];
            perNoise{corrIdx, pIdx} = [pr(pMask, 4)];
            resps{corrIdx, pIdx}    = [pr(pMask, 7)];

            % XXX: regress out nuisance vars - trial number? perseveration? individual finger effects?
            B{subj, corrIdx, pIdx}   = regress(RT{corrIdx, pIdx}, [ones(length(memNoise{corrIdx, pIdx}), 1), memNoise{corrIdx, pIdx}, ...
                                                                   perNoise{corrIdx, pIdx}, resps{corrIdx, pIdx}]);
                                            % , memNoise{subj, corrIdx}.*perNoise{subj, corrIdx}]);

            perCounts{subj, corrIdx, pIdx} = [pr(pMask, 5)];
        end
    end
end

for pIdx = 1:2;
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure
    end
    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);
    hold on;

    for corrIdx = 1:2;
        subplot(2, 1, corrIdx);
        set(gca, 'FontWeight', 'demi');
        set(gca, 'FontSize', 24);
        hold on;

        selB = squeeze([B{:, corrIdx, pIdx}]);

        for noiseType = 1:2;
            bar(noiseType, mean(selB(noiseType+1, :)), 'FaceColor', [0.8 0.8 0.8]);
            errorbar(noiseType, mean(selB(noiseType+1, :)), std(selB(noiseType+1, :))/sqrt(size(selB, 2)), 'ko', 'LineWidth', 4);
            [h,p]=ttest(selB(noiseType+1, :));

            if (p<0.05)
                plot(noiseType, 1, 'k*', 'MarkerSize', 12, 'LineWidth', 2);
            elseif (p<0.1)
                plot(noiseType, 1, 'k^', 'MarkerSize', 12, 'LineWidth', 2);
            end
        end

        set(gca, 'XTick', [1:2]);
        set(gca, 'XTickLabel', {'', ''});
        if (corrIdx == 1)
            titleStr = ['Incorrect trials, level ' num2str(pLevels(pIdx))];

            if (length(submat) == 1)
                titleStr = [titleStr ' subject ' num2str(submat) ' block ' num2str(find(sd.params.perNoise==pLevels(pIdx)))];
            end
            title(titleStr);
            ylabel('\beta_{RT}');
        else
            title('Correct trials');
            set(gca, 'XTickLabel', {'  Memory  ', 'Perceptual'});
        end
    end

    if (opts.savefigs)
        print('-dpng', '-r800', ['sub' num2str(subj, '%.2d') ...
              '_block_' num2str(find(sd.params.perNoise==pLevels(pIdx))) '.png']);
    end
end

end % if (opts.plotRegression)

%% Smoothed plots
% For each perceptual evidence level, and each memory evidence level, smooth plots
%   - of RT (for all trials, and for correct and incorrect trials)
%   - of accuracy

trialrec = [];
RTmeans  = [];
RTerrs   = [];
acc      = [];

for subj = submat;
    try
        pr = plotSubj(subj, 2, 0, 0, 1, opts.dataDir);
    catch
        continue
    end

    % Trialrec array: zRT, corr/incorr, mEv, pEv
    trialrec = [trialrec; pr(:,2) pr(:,3) pr(:,1) pr(:,6)];
end

trialrec = trialrec(~isnan(trialrec(:,1)), :);

perLevels = [0.65 0.85];
memLevels = [0:0.2:1];
for perLevel = 1:length(perLevels);
    for memLevel = 2:length(memLevels)-1;
        % Select incorrect trials with this perLevel and memLevel
        selectRows = [trialrec(:,2) == 0 & trialrec(:,4)==perLevels(perLevel) & trialrec(:,3)>=memLevels(memLevel-1) & trialrec(:,3)<memLevels(memLevel+1)];

        disp(['Incorrect trials, mem = ' num2str(memLevels(memLevel), '%.2d') ', per = ' num2str(perLevels(perLevel), '%.2d') ': ' num2str(sum(selectRows))]);
        RTmeans(1, perLevel, memLevel) = mean(trialrec(selectRows, 1));
        RTerrs(1, perLevel, memLevel)  = std(trialrec(selectRows, 1))/sqrt(sum(selectRows));

        % Select correct trials with this perLevel and memLevel
        selectRows = [trialrec(:,2) == 1 & trialrec(:,4)==perLevels(perLevel) & trialrec(:,3)>=memLevels(memLevel-1) & trialrec(:,3)<memLevels(memLevel+1)];

        disp(['Correct trials, mem = ' num2str(memLevels(memLevel), '%.2d') ', per = ' num2str(perLevels(perLevel), '%.2d') ': ' num2str(sum(selectRows))]);
        RTmeans(2, perLevel, memLevel) = mean(trialrec(selectRows, 1));
        RTerrs(2, perLevel, memLevel)  = std(trialrec(selectRows, 1))/sqrt(sum(selectRows));

        selectRows = [trialrec(:,4)==perLevels(perLevel) & trialrec(:,3)>=memLevels(memLevel-1) & trialrec(:,3)<memLevels(memLevel+1)];
        acc(perLevel, memLevel) = mean(trialrec(selectRows, 2));
    end
end

if (exist('aaron_newfig', 'file'))
    aaron_newfig;
else
    figure
end

perCols = ['r' 'g'];
for corrIdx = [1:2];
    subplot(2,1,corrIdx);
    set(gca, 'FontSize', 24);
    set(gca, 'FontWeight', 'demi');
    hold on;

    for perLevel = 1:2;
        plot(squeeze(RTmeans(corrIdx, perLevel, 2:end)), [perCols(perLevel) '-'], 'LineWidth', 4);

        plot(squeeze(RTmeans(corrIdx, perLevel, 2:end))+squeeze(RTerrs(corrIdx, perLevel, 2:end)), [perCols(perLevel) '--'], 'LineWidth', 2);
        plot(squeeze(RTmeans(corrIdx, perLevel, 2:end))-squeeze(RTerrs(corrIdx, perLevel, 2:end)), [perCols(perLevel) '--'], 'LineWidth', 2);
    end

    if (corrIdx == 1)
        title(['Incorrect trials']);
        ylabel('zRT');
        set(gca, 'XTick', [1 2 3 4]);
        set(gca, 'XTickLabel', arrayfun(@num2str, memLevels([2:end-1]), ...
                 'UniformOutput', false));
    end

    if (corrIdx == 2)
        title(['Correct trials']);
        xlabel('Cue probability of given response');
        set(gca, 'XTick', [1 2 3 4]);
        set(gca, 'XTickLabel', arrayfun(@num2str, memLevels([2:end-1]), ...
                 'UniformOutput', false));
    end
end

if (exist('aaron_newfig', 'file'))
    aaron_newfig;
else
    figure
end

set(gca, 'FontSize', 24);
set(gca, 'FontWeight', 'demi');

for perLevel = 1:2;
    plot(acc(perLevel, 2:end), [perCols(perLevel) '-'], 'LineWidth', 4);
end

ylabel('Accuracy');
xlabel('Cue probability of given response');
set(gca, 'XTick', [1 2 3 4]);
set(gca, 'XTickLabel', arrayfun(@num2str, memLevels([2:end-1]), ...
         'UniformOutput', false));
