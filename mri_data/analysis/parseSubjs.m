function [submat sessmat skippedSubj] = parseSubjs(subjFile, varargin)
% function [submat sessmat skippedSubj] = parseSubjs([subjFile='subjSummary.mat'], ...)
%
% Plot histograms of subject exclusion variables, and identify excluded subjects
%
% Returns arrays of included and skipped subjects & sessions
%

opts = parse_args(varargin, 'verbose', false, ...
                            'doPlot', false);

%%
% Constants
numLearnTrials = 100;
numTestTrials  = 80;
numBlocks      = 2;

if (nargin < 1 || isempty(subjFile))
    subjFile = 'subjSummary.mat';
end

%%
% Initialize
subjSummary = load(subjFile);
subjSummary = subjSummary.subjSummary;

% Non-empty subjects & sessiosn
submat  = find(arrayfun(@(x)(~isempty(x.calibDiffs)), subjSummary));
sessmat = ones(numBlocks, max(submat));
sessmat = sessmat(:, submat);

%%
% Exclusion criteria: 10% learning-phase trials skipped in either block
%
params.learnSkipThresh = 0.1;
if (opts.doPlot)
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure;
    end
end

for blockIdx = 1:numBlocks;
    if (opts.doPlot)
        subplot(numBlocks, 1, blockIdx);
        hold on;
    end

    % learning phase is phase #1
    skippedLearnTrials = arrayfun(@(x)(x.skippedTrials{1, blockIdx}), subjSummary(submat));

    if (opts.doPlot)
        numEl = hist(skippedLearnTrials);
        hist(skippedLearnTrials);

        % Plot threshold line
        plot(ones(1, max(numEl))*(params.learnSkipThresh * numLearnTrials), [1:max(numEl)], 'r--', ...
                                                                                            'LineWidth', 2);
        ylabel(['Block ' num2str(blockIdx)]);
    end

    skippedSubj{1}(blockIdx, :) = skippedLearnTrials > (params.learnSkipThresh * numLearnTrials);
    sessmat(blockIdx, :)        = sessmat(blockIdx, :) & ~skippedSubj{1}(blockIdx, :);

    disp(['Learn-phase skip, block ' num2str(blockIdx) ...
          ', excluding subjects: ' num2str(submat(find(skippedSubj{1}(blockIdx, :))))]);
end
if (opts.doPlot)
    subplot(numBlocks, 1, 1);
    title(['Learning-phase trials skipped: ' ...
            num2str(sum(any(skippedSubj{1}))) ' excluded']);
    print('-depsc', '-r800', 'parseSubjs_1_learnPhaseTrialsSkipped.eps');
end

%%
% Exclusion criteria: 10% test-phase trials skipped in either block
%
params.testSkipThresh = 0.1;
if (opts.doPlot)
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure;
    end
end

for blockIdx = 1:numBlocks;
    if (opts.doPlot)
        subplot(numBlocks, 1, blockIdx);
        hold on;
    end

    % test phase is phase #2
    skippedTestTrials = arrayfun(@(x)(x.skippedTrials{2, blockIdx}), subjSummary(submat));

    if (opts.doPlot)
        numEl = hist(skippedTestTrials);
        hist(skippedTestTrials);

        % Plot threshold line
        plot(ones(1, max(numEl))*(params.testSkipThresh * numTestTrials), [1:max(numEl)], 'r--', ...
                                                                                        'LineWidth', 2);
        ylabel(['Block ' num2str(blockIdx)]);
    end

    skippedSubj{2}(blockIdx, :) = skippedTestTrials > (params.testSkipThresh * numTestTrials);
    sessmat(blockIdx, :)        = sessmat(blockIdx, :) & ~skippedSubj{2}(blockIdx, :);
    disp(['Test-phase skip, block ' num2str(blockIdx) ...
          ', excluding subjects: ' num2str(submat(find(skippedSubj{2}(blockIdx, :))))]);
end
if (opts.doPlot)
    subplot(numBlocks, 1, 1);
    title(['Test-phase trials skipped: ' ...
            num2str(sum(any(skippedSubj{2}))) ' excluded']);
    print('-depsc', '-r800', 'parseSubjs_2_testPhaseTrialsSkipped.eps');
end

%%
% Exclusion criteria: Errors on late responses in learning phase in either block.
%
params.learnErrorThresh = 0.3;
if (opts.doPlot)
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure;
    end
end
for blockIdx = 1:numBlocks;
    if (opts.doPlot)
        subplot(numBlocks, 1, blockIdx);
        hold on;
    end

    errorTrials = arrayfun(@(x)(x.errorsByPhase{1, blockIdx}), subjSummary(submat)) + skippedLearnTrials;

    if (opts.doPlot)
        numEl = hist(errorTrials);
        hist(errorTrials);

        plot(ones(1, max(numEl))*(params.learnErrorThresh * numLearnTrials), [1:max(numEl)], 'r--', ...
                                                                                             'LineWidth', 2);
        ylabel(['Block ' num2str(blockIdx)]);
    end

    skippedSubj{3}(blockIdx, :) = errorTrials > (params.learnErrorThresh * numLearnTrials);
    sessmat(blockIdx, :)        = sessmat(blockIdx, :) & ~skippedSubj{3}(blockIdx, :);
    disp(['Late errors plus skipped trials, block ' num2str(blockIdx) ...
          ', excluding subjects: ' num2str(submat(find(skippedSubj{3}(blockIdx, :))))]);
end
if (opts.doPlot)
    subplot(numBlocks, 1, 1);
    title(['Errors on late responses + skipped trials in learning phase: ' ...
            num2str(sum(any(skippedSubj{3}))) ' excluded']);
    print('-depsc', '-r800', 'parseSubjs_3_errorsLateResponses.eps');
end

%%
% Excusion criteria: Perceptual coherence calibration levels not different by greater than 0.1 on any stimulus, for any block
%
params.calibThresh = 0.1;
if (opts.doPlot)
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure;
    end
end
for blockIdx = 1:numBlocks;
    if (opts.doPlot)
        subplot(numBlocks, 1, blockIdx);
        hold on;
    end

    calibDiffs  = arrayfun(@(x)(x.calibDiffs(blockIdx)), subjSummary(submat));

    if (opts.doPlot)
        numEl = hist(calibDiffs);
        hist(calibDiffs);

        plot(ones(1, max(numEl))*(params.calibThresh), [1:max(numEl)], 'r--', ...
                                                                       'LineWidth', 2);
        ylabel(['Block ' num2str(blockIdx)]);
    end

    skippedSubj{4}(blockIdx, :) = calibDiffs < params.calibThresh;
    sessmat(blockIdx, :)        = sessmat(blockIdx, :) & ~skippedSubj{4}(blockIdx, :);
    disp(['Calibration accuracy differences, block ' num2str(blockIdx) ...
          ', excluding subjects: ' num2str(submat(find(skippedSubj{4}(blockIdx, :))))]);
end
if (opts.doPlot)
    subplot(numBlocks, 1, 1);
    title(['Calibration accuracy differences: ' ...
            num2str(sum(any(skippedSubj{4}))) ' excluded']);
    print('-depsc', '-r800', 'parseSubjs_4_calibrationDifferences.eps');
end

[exclSess exclSubj] = ind2sub(size(sessmat), find(sessmat==0));
disp(['Excluded ' num2str(sum(~sessmat(:))) ...
      ' sessions: ']);
num2str([submat(exclSubj)' exclSess])

[inclSess inclSubj] = ind2sub(size(sessmat), find(sessmat));
disp(['Included ' num2str(sum(sessmat(:))) ...
      ' sessions: ']);
num2str([submat(inclSubj)' inclSess])
sessmat = [submat(inclSubj)' inclSess];

allExcluded   = any(skippedSubj{1}) | any(skippedSubj{2}) | any(skippedSubj{3}) | any(skippedSubj{4});
allIncluded   = ~allExcluded;
allExcluded   = find(allExcluded);
allIncluded   = find(allIncluded);
totalExcluded = length(allExcluded);
totalIncluded = length(allIncluded);
disp(['Subjects excluded: ' ...
       num2str(length(allExcluded)) ' / ' ...
       num2str(length(submat)) ' subjects (' ...
       num2str(submat(allExcluded)), ')']);
disp(['Subjects included: ' ...
       num2str(length(allIncluded)) ' / ' ...
       num2str(length(submat)) ' subjects (' ...
       num2str(submat(allIncluded)), ')']);
submat = submat(allIncluded);

save(subjFile, 'subjSummary', 'params', 'skippedSubj', 'submat', 'sessmat');
