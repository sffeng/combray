% Runs the "round 6" fits, which are the round 5 fits using a genetic
% algorithm. Fitting two driftrates, one threshold, one deadline, and
% nondecision time T_0 for a two stage ddm process, for trials pooled
% across subjects for each of the four cues: .5, .4/.6, .3/.7,
% .2/.8, pooling accross all perceptual condition.
%
% mistake in this fit -- not properly constrained deadline -- see fit_6a and fit_6b instead

clear
close all

%% Genetic algorithm options
opts = gaoptimset('UseParallel','always','Display','diagnose');
lb = [-.2 -.2 0.05 .1 0]; %d1 d2 z dl T0
ub = [3.0 3.0 5.0 3.0  0.5];

%% Data settings
subjects = [1:9 12:16];
cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
percept = [.65 .85];

%% Setup arrays
recoveredParameters = nan(4,length(ub));
nTrialArray = nan(4,1);
finalChiSq = nan(4,1);
exitFlags = nan(4,1);


for k = 1:length(cueArray(:,1))
    
    % collect data and output basic stuff
    cues = cueArray(k,:);
    [tr] = gettesttrials(subjects,cues,0,percept);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues)])

    % Solve the sucker
    rng(19850604,'twister'); 
    [recoveredParameters(k,:),finalChiSq(k), exitFlags(k)] = ...
        ga(@(x) obj2a1z1DT0(x,rt,resp), length(lb), [],[],[],[],...
           lb,ub,[],opts);

    nTrialArray(k) = nTrials;
    exitFlags
    recoveredParameters
end
nTrials = nTrialArray;

save('fit_6.mat','recoveredParameters','nTrials','finalChiSq','exitFlags')