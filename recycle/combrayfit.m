function [inferredX, fv] = combrayfit(rt,resp,objfun,initialX)

% see myobj1 below to see experimentally set values

opts = getfmsopts;

% Do it!
[inferredX, fv] = fminsearch(@(x) myobj1(x,rt,rtResp), ...
                             initialX,opts);
