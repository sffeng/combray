function plotrec = plotSubj(subj, plotPhase, doPlot, earlyOnly, zAfter, dataDir)
% function plotrec = plotSubj(subj, [plotPhase], [doPlot], [earlyOnly], [zAfter], [dataDir])
%
% INPUT:
%   plotPhase: 0 = all (default); 1 = learn only; 2 = test only; 2.1 = early test; 2.2 = late test
%   doPlot:    0 = no (default); 1 = yes
%   earlyOnly: -1 = late responses (after stim onset) only; 0 = all trials (default); 1 = early responses only
%   zAfter:    -1 = no z-scoring at all; 0 = z-score within subject (default); 1 = z-score within type
%
% OUTPUT:
%   plotrec array
%   plotrec(:, 1): transition probability from cue fractal to image
%   plotrec(:, 2): reaction time
%   plotrec(;, 3): correct response? 1 yes 0 no
%   plotrec(:, 4): observed fraction of perceptual evidence congruent with given response (0-1)
%   plotrec(:, 5): total amount of perceptual evidence observed (# of frames)
%   plotrec(;, 6): hardcoded perceptual coherence of this trial (0-1)
%   plotrec(:, 7): response key
%   plotrec(:, 8): best response key
%   plotrec(:, 9): ISI
%
% 4 Nov 2014: SF changed default data dir
% 27 Nov 2014: SF added showoutput var

showoutput = 0;

plotcols = ['r' 'g'];
learnLen = 100;
testLen  = 80;
if (0 && subj <= 3)
    % NB: Hack for pilot round 2+
    learnLen = 140;
    testLen = 40;
end

if (nargin < 2)
    plotPhase = 0;
end

if (nargin < 3)
    doPlot = 0;
end

if (nargin < 4)
    earlyOnly = 0;
end

if (nargin < 5)
    zAfter   = 0;
end

if (nargin < 6)
    dataDir = 'data/';
end

sd = loadSubj(subj, dataDir);

cueDuration = 0.75;

earlymask = [sd.trials(:).RT]'<=(sd.trials(1).ISI+cueDuration);
RTs       = [sd.trials(:).RT];
% z-score RTs within-subject
if (zAfter == 0)
    RTs(~isnan(RTs)) = (RTs(~isnan(RTs)) - nanmean(RTs))/nanstd(RTs);
end

plotrec = zeros(length(sd.trials), 9);
plotrec(:, 2) = NaN;

calibFiles = dir([dataDir 'combray_calib_' num2str(subj, '%.2d') '_*.mat']);

for calibIdx = 1:length(calibFiles)
    cfd{calibIdx} = load([dataDir calibFiles(calibIdx).name]);

    for stairIdx = 1:size(cfd{calibIdx}.response, 1);
        perNoise(calibIdx, stairIdx) = cfd{calibIdx}.p_stair(ceil(stairIdx/2));
    end

end

for rowIdx = 1:length(sd.trials);
    thisTp = sd.params.tp(sd.trials(rowIdx).cue, sd.trials(rowIdx).image, sd.trials(rowIdx).block);

    prow     = sd.params.tp(sd.trials(rowIdx).cue, :, sd.trials(rowIdx).block);
    bestResp = find(prow == max(prow));
    if (thisTp == 0.5)
        bestResp = sd.trials(rowIdx).resp;
    end

    if (sd.trials(rowIdx).phase == plotPhase || ~plotPhase || mod(plotPhase,1))
        if (rowIdx > length(sd.trials)/2)
            testIdx = rowIdx - length(sd.trials)/2;
        else
            testIdx = rowIdx;
        end

        if (mod(plotPhase,1))
            % early or late learning?
            if ((plotPhase == 0.5 && testIdx > learnLen/2) || ...
                (plotPhase == 1.5 && (testIdx <= learnLen/2 || testIdx > learnLen)))
                continue;
            end
        end

        % early or late test?
        if ((plotPhase == 2.1) && ...
            ((testIdx <= learnLen) || (testIdx-learnLen > testLen/2)))
            continue;
        end

        if ((plotPhase == 2.2) && ...
            ((testIdx <= learnLen) || (testIdx-learnLen <= testLen/2)))
            continue;
        end

%        disp(['Plotting ' num2str(rowIdx)]);
        plotrec(rowIdx, :) = [double(thisTp), double(RTs(rowIdx)), double(sd.trials(rowIdx).accurate), ...
                              double(sd.observedFraction(rowIdx)), double(sd.observationCount(rowIdx)), ...
                              double(perNoise(sd.trials(rowIdx).block, sd.trials(rowIdx).corrResp)), double(sd.trials(rowIdx).resp), ...
                              double(bestResp), double(sd.trials(rowIdx).ISI)];
%                              double(bestResp), double(sd.trials(rowIdx).ISI)];

    end
end

if showoutput
disp(['Skipped ' num2str(sum(isnan(plotrec(:,2)))) ...
      ' trials out of ' num2str(sum(plotrec(:,2)~=0))]);
end

zmask     = ~isnan(plotrec(:,2));

% NB: Assumes fixed ISI
early     = sum(earlymask & zmask);
late      = sum(~earlymask & zmask);

hiearly   = sum(earlymask & zmask & plotrec(:, 6)==0.65);
loearly   = sum(earlymask & zmask & plotrec(:, 6)==0.85);
hilate    = sum(~earlymask & zmask & plotrec(:, 6)==0.65);
lolate    = sum(~earlymask & zmask & plotrec(:, 6)==0.85);

latemem{1}  = ~earlymask & zmask & (plotrec(:, 1)==0.8 | plotrec(:, 1)==0.2);
lateRTs{1}  = plotrec(latemem{1}, 2);
latemem{2}  = ~earlymask & zmask & (plotrec(:, 1)==0.7 | plotrec(:, 1)==0.3);
lateRTs{2}  = plotrec(latemem{2}, 2);
latemem{3}  = ~earlymask & zmask & (plotrec(:, 1)==0.6 | plotrec(:, 1)==0.4);
lateRTs{3}  = plotrec(latemem{3}, 2);
latemem{4}  = ~earlymask & zmask & (plotrec(:, 1)==0.5);
lateRTs{4}  = plotrec(latemem{4}, 2);

if showoutput
disp(['RTs for late responses at mem = 0.8/0.2: ' num2str(nanmean(lateRTs{1})) ...
      ' +/- ' num2str(nanstd(lateRTs{1})/sqrt(sum(~isnan(lateRTs{1}))))]);
disp(['RTs for late responses at mem = 0.7/0.3: ' num2str(nanmean(lateRTs{2})) ...
      ' +/- ' num2str(nanstd(lateRTs{2})/sqrt(sum(~isnan(lateRTs{2}))))]);
disp(['RTs for late responses at mem = 0.6/0.4: ' num2str(nanmean(lateRTs{3})) ...
      ' +/- ' num2str(nanstd(lateRTs{3})/sqrt(sum(~isnan(lateRTs{3}))))]);
disp(['RTs for late responses at mem = 0.5: ' num2str(nanmean(lateRTs{4})) ...
      ' +/- ' num2str(nanstd(lateRTs{4})/sqrt(sum(~isnan(lateRTs{4}))))]);
end

earlymem(1) = sum(earlymask & zmask & (plotrec(:, 1)==0.8 | plotrec(:, 1)==0.2));
totalmem(1) = sum(zmask & (plotrec(:, 1)==0.8 | plotrec(:, 1)==0.2));
earlymem(2) = sum(earlymask & zmask & (plotrec(:, 1)==0.7 | plotrec(:, 1)==0.3));
totalmem(2) = sum(zmask & (plotrec(:, 1)==0.7 | plotrec(:, 1)==0.3));
earlymem(3) = sum(earlymask & zmask & (plotrec(:, 1)==0.6 | plotrec(:, 1)==0.4));
totalmem(3) = sum(zmask & (plotrec(:, 1)==0.6 | plotrec(:, 1)==0.4));
earlymem(4) = sum(earlymask & zmask & (plotrec(:, 1)==0.5));
totalmem(4) = sum(zmask & (plotrec(:, 1)==0.5));

if showoutput
disp(['Early responses at mem = 0.8/0.2: ' num2str(earlymem(1)) ...
      ' out of ' num2str(totalmem(1))]);
disp(['Early responses at mem = 0.7/0.3: ' num2str(earlymem(2)) ...
      ' out of ' num2str(totalmem(2))]);
disp(['Early responses at mem = 0.6/0.4: ' num2str(earlymem(3)) ...
      ' out of ' num2str(totalmem(3))]);
disp(['Early responses at mem = 0.5: ' num2str(earlymem(4)) ...
      ' out of ' num2str(totalmem(4))]);

disp([num2str(early) ' early responses and ' ...
      num2str(late) ' late responses out of ' ...
      num2str(size(plotrec(zmask,:),1)) ' responded trials.']);
end

if (earlyOnly == 1)
    zmask = earlymask & zmask;
elseif (earlyOnly == -1)
    zmask = ~earlymask & zmask;
end

if showoutput
disp([num2str(sum(plotrec(zmask, 3))) ' correct and ', ...
      num2str(sum(plotrec(zmask, 3)==0)) ' incorrect out of ' ...
      num2str(size(plotrec(zmask,:),1)) ' included trials.']);
end

plotrec(~zmask, 2) = NaN;
plotrec(~zmask, 3) = NaN;

% z-score RTs within-subject, post-masking
if (zAfter == 1)
    plotrec(zmask, 2) = (plotrec(zmask, 2) - nanmean(plotrec(zmask, 2)))/nanstd(plotrec(zmask, 2));
end

% XXX Plot the Weibull PDFs for this subject
if (0 && opts.plotWeibull)
    if (exist('aaron_newfig', 'file'))
        aaron_newfig;
    else
        figure;
        hold on;
    end

    set(gca, 'FontWeight', 'demi');
    set(gca, 'FontSize', 24);

    subplot(length(sd.calibRec), length(sd.calibRec{1}), 1);

    for calibIdx = 1:length(sd.calibRec);
        for stimIdx = 1:length(sd.calibRec{calibIdx});
            % sd.calibRec{calibIdx}{stimIdx}.beta
        end
    end

end

% x TODO: z-score RTs within-subject
% x TODO: plot error bars
% TODO: label figures with % of trials this represents
% TODO: bin adjacent transition p's
% TODO: DDM model fits? where to get t_0
