function inferredX = combrayfit_lsqnonlin(subjArray)
% subjArray is Nx2 matrix, 1st column RTs, 2nd column responses (>0
% if correct)
rt = subjArray(:,1);
rtResp = subjArray(:,2);

tFinal = max(rt) + 3;

% Initial values for 2-stage DDM
a = [.16 .16];
z = 2.0;
initialX = [a(1) a(2) z];
% see myobj1 below to see experimentally set values

%Let's go with fminsearch first...
opts = optimset('fminsearch');
opts = optimset(opts,'Display','iter');
opts = optimset(opts,'TolFun',1e-8);
opts = optimset(opts,'TolX',1e-8);
opts = optimset(opts,'MaxFunEvals',1000*5);
opts = optimset(opts,'MaxIter',500*5);

% Bounds if needed
LB = [0.01 0.01 1e-2];
UB = [2 2 5];

% Do it!
inferredX = lsqnonlin(@(x) myobj1(x,rt,rtResp,tFinal),initialX,LB, ...
                       UB)


function val = myobj1(x,rt,rtResp,tFinal)
a = [x(1) x(2)];
z = [x(3) x(3)];

% Fixed (experimentally set) values
s = [1 1];
x0 = 0; % before cue comes on, either alternative equally likely
dl = [0 2.5];

[val,df] = chisq_rt2002(rt,rtResp,a,s,z,x0,1,dl,tFinal);
val = df;
