% objective function for fitting three parameters for a two stage DDM:
% two drifts, one threshold, using rt2002.
%

function val = obj2a1zT0(x,rt,rtResp)
a = [x(1) x(2)];
z = [x(3) x(3)];
rt = rt - x(4);
% Fixed (experimentally set) values
s = [1 1];
x0 = 0; % before cue comes on, either alternative equally likely
dl = [0 2.5];
tFinal = max(rt) + 3;

val = rt2002(rt,rtResp,a,s,z,x0,1,dl,tFinal);
