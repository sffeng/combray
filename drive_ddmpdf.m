function drive_ddmpdf
close all, clear all, clc

A = 5;
T0 = 0;
x0 = 0;
z = .5;
sigma = 1.0;
T = linspace(0,5,1200);
T(1) = [];

[p0,RT0,myP0] = ddmpdf(T,0,A,T0,x0,z,sigma); % lower boundary
[p1,RT1,myP1] = ddmpdf(T,1,A,T0,x0,z,sigma); % upper boundary

figure(1)
plot(T,p0,'r'), hold on
plot(T,p1,'b'), hold off
legend('Lower boundary','Upper boundary')

dt = T(2)-T(1);

% numerically computed choice probabilities
P0 = sum(trapz(p0)*dt)
P1 = sum(trapz(p1)*dt)
disp(['Missing mass: ' num2str(1 - (P0+P1))])

% Now to compute numerical mean RTs, need to take numerical
% expectation

% Don't use these...
% P0 = sum(trapz(T.*p0)*dt);
% P1 = sum(trapz(T.*p1)*dt);

% Use these to check the mean RT computations
% (T*p0')/sum(p0)
% (T*p1')/sum(p1)

RT0 % lower mean rt
RT1 % upper mean rt

T = linspace(0,5,100000);
[rtT,rtB] = simRT(T,A,T0,x0,z,sigma,10000);
mean(rtB)
mean(rtT)




function [rtT,rtB] = simRT(T,A,T0,x0,z,sigma, nTrials)

rtT = nan(1,nTrials);
rtB = nan(1,nTrials);

nT = length(T);
dt = T(2) - T(1);


for tr = 1:nTrials
    X = zeros(1,nT);    
    X(1) = x0;
    winc = sigma*sqrt(dt)*randn(1,nT);
    
    for k = 1:(nT-1)
        t = T(k+1);
        if X(k) > z
            rtT(tr) = t+T0;
            break
        elseif X(k) < -z
            rtB(tr) = t+T0;
            break
        else
            X(k+1) = X(k) + dt*A + winc(k);
        end
    end
    
end

rtT(isnan(rtT)) = [];
rtB(isnan(rtB)) = [];