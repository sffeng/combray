% Computes the ebars for one set of parameters given a previous fit
% (in this case fit_msddm_1)
%
% Samuel Feng (fengman@gmail.com)
% Last updated: 4 Jan 2015

dat = load('collatedData');
dat = dat.collatedData;
p = load('fit_msddm_1.mat');
fitChiSq = p.finalChiSq(1,:);
fitp = p.recoveredParameters(1,:); %d1 d2 dl z x0 T0
D = dat{1,1,1}; % Here just working with the 1,1,1 data
rt = D(:,1);
resp = D(:,2);

disp('Verifying  optimal chi2 value .....')
if abs(fitChiSq - obj2a1d1z1xT0(fitp, rt,resp)) < .001
    disp('Passed')
else
    disp('Failed')
    return
end

%% Specify CI constant and 
fv = chi2inv(.68,5); % 1sigma CI with 5 dof (12 bins - 6 params - 1)

%% Locate sign changes
myobj = @(x) fitChiSq+fv ...
        - obj2a1d1z1xT0(fitp+[x 0 0 0 0 0],rt, resp);

ndx = -.01;
while sign(myobj(0)) == sign(myobj(ndx))
    ndx = ndx * 2;
end

dx = .01;
while sign(myobj(0)) == sign(myobj(dx))
    dx = dx * 2;
end

%% Compute lower and upper CI bounds
opts = optimset('Display','off'); % off -> iter for testing
iminus = fminbnd(@(x) myobj(x)^2,ndx,0,opts);
iplus = fminbnd(@(x) myobj(x)^2,0,dx,opts);

%% output CI
fitp(1) + [iminus iplus]