
clear
close all

%% Compare fits 8 and 9 (things hardcoded below)
fit1 = load('fit_8.mat');
fit2 = load('fit_9.mat');

%% Data settings (will need to change to cueperceptdata.mat)
%subjects = [1:9 12:16];
subjects = [1 2 5 7 8 9 13 18 20 21 22 27 28 30 31 32 34 ...
            39 40 42 43];

cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
perceptArray = [.65 .85];

runIter = 1;
for k = 1:length(cueArray(:,1))
    for j = 1:length(perceptArray)


        % collect data and output basic stuff
        cues = cueArray(k,:);
        percept = perceptArray(j);
        fitSettings(runIter,:) = [cues percept];
        [tr] = gettesttrials(subjects,cues,0,percept);
        rt = tr(:,1);
        resp = tr(:,2);
        nTrials = length(rt);
        disp(['Using ' num2str(nTrials) ' trials for cues ' ...
              num2str(cues) ' and percept ' num2str(percept)])
        
        figure(runIter);
        rtCor = rt(resp>0);
        [N,X] = hist(rtCor,nbins(rtCor)+10);
        dx = X(2)-X(1);
        NN = cumsum(N);
        acc = sum(resp>0)/length(resp);
        NN = NN/NN(end)*acc;
        %bar(X,NN,'b')
        bar(X,N/trapz(X,N),'b')
        hold on
        
        %% Fit 8 stuff
        a = fit1.recoveredParameters(runIter,1);
        a = [a a];
        s = [1 1];        
        th = fit1.recoveredParameters(runIter,2);
        th = [th th];
        x0 = fit1.recoveredParameters(runIter,3);
        x0dist = 1;        
        dl = [0 2.5];
        tFinal = max(rt)+3;
        T0 = fit1.recoveredParameters(runIter,4);
        
        %% Compute Fit 8 fit, adjusting T0
        [tArray,~,yPlus,yMinus] = multistage_ddm_fpt_dist(...
            a,s,th,x0,x0dist,dl,tFinal);
        I = find(tArray<X(end));
        %plot(tArray(I),yPlus(I),'r','LineWidth',2)
        
        dt = tArray(3)-tArray(2);
        nShift = round(T0/dt);
        tmpt = (tArray(end)+(1:nShift)*dt);
        tArray = [tArray tmpt];
        yPlus = [zeros(1,nShift) yPlus];
        yMinus = [zeros(1,nShift) yMinus];
        tt = tArray(2:end);
        yy = diff(yPlus)/dt;
        
        plot(tt,yy/trapz(tt,yy),'r','Linewidth',2)
        drawnow

        %% Fit 9 stuff
        a1 = fit2.recoveredParameters(runIter,1);
        a2 = fit2.recoveredParameters(runIter,2);
        a = [a1 a2];
        s = [1 1];        
        th = fit2.recoveredParameters(runIter,4);
        th = [th th];
        x0 = fit2.recoveredParameters(runIter,5);
        x0dist = 1;
        Del = fit2.recoveredParameters(runIter,3);
        dl = [0 Del];
        tFinal = max(rt)+3;
        T0 = fit2.recoveredParameters(runIter,6);


        %% Compute Fit 9 fit, adjusting T0
        [tArray,~,yPlus,yMinus] = multistage_ddm_fpt_dist(...
            a,s,th,x0,x0dist,dl,tFinal);
        I = find(tArray<X(end));        
        %plot(tArray(I),yPlus(I),'m','Linewidth',2);
        
        dt = tArray(3)-tArray(2);
        nShift = round(T0/dt);
        tmpt = (tArray(end)+(1:nShift)*dt);
        tArray = [tArray tmpt];
        yPlus = [zeros(1,nShift) yPlus];
        yMinus = [zeros(1,nShift) yMinus];
        tt = tArray(2:end);
        yy = diff(yPlus)/dt;
        
        plot(tt,yy/trapz(tt,yy),'m','Linewidth',2)


        s = ['Cue = ' num2str(cues(1)) ' Percept = ' num2str(percept)];
        title(s)
        xlabel('Time (sec)')
        ylabel('Cumulative response probability')
        formatfigurefonts;
        drawnow
        
        fs = ['fithist_c' num2str(cues(1)) '_per' ...
              num2str(percept)];
        saveas(gcf,['figures/' fs '.eps'],'psc2')
        saveas(gcf,['figures/' fs '.fig'],'fig')

        runIter = runIter + 1;
    end
end

