
clear
close all

%% Genetic algorithm options
lb = [-.2 -.2 2 .05 -1  0]; %d1 d2 dl z x0 T0
ub = [3.0 3.0 10 5.0  1  .5]; 
nVars = length(lb);
opts = gaoptimset('UseParallel','always','Display','diagnose',...
                  'Generations',100*nVars,...
                  'PopulationSize',10*nVars);

% impose abs(x0) \leq z
conA = [0 0 0 -1 1 0;
        0 0 0 -1 -1 0];
conB = [0;
        0];

%% Data settings (will need to change to cueperceptdata.mat)
datadir = 'mri_data/';
subjects = [7:18 20:31 33:36];

subjects = [7 10:14 17 18 20 22:24 26:30 33:34 36];
cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
perceptArray = [.65 .85];
dlArray = [4 6 8];
nFits = length(cueArray(:,1))*length(perceptArray)*length(dlArray);

%% Setup arrays
recoveredParameters = nan(nFits,length(ub));
nTrialArray = nan(nFits,1);
finalChiSq = nan(nFits,1);
exitFlags = nan(nFits,1);
fitSettings = nan(nFits,4);

runIter = 1;
diary fit_mri_5.diary
for k = 1:length(cueArray(:,1))
for j = 1:length(perceptArray)
for ell = 1:length(dlArray);
    % collect data and output basic stuff
    cues = cueArray(k,:);
    percept = perceptArray(j);
    dl = dlArray(ell);
    
    fitSettings(runIter,:) = [cues percept dl];
    [tr] = gettesttrials(subjects,cues,0,percept,datadir,dl);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues(1)) ' and percept ' num2str(percept)])
    disp(['Deadline: ' num2str(dl)])

    % Now the clamping changes depending on the pooling of
    % deadlined trials
    lb = [-.2 -.2 dl+.75 .05 -1  0]; %d1 d2 dl z x0 T0
    ub = [3.0 3.0 dl+.75 5.0  1  .5];

    % Solve the sucker
    rng(19850604,'twister'); 
    [recoveredParameters(runIter,:),finalChiSq(runIter), ...
     exitFlags(runIter)] =...
        ga(@(x) obj2a1d1z1xT0(x,rt,resp), length(lb), conA,conB,...
           [],[], lb,ub,[],opts);

    nTrialArray(runIter) = nTrials;
    exitFlags
    recoveredParameters
    runIter = runIter + 1;

    nTrials = nTrialArray;

    save('fit_mri_5.mat','recoveredParameters','nTrials','finalChiSq', ...
         'exitFlags','fitSettings')

end
end
end
    nTrials = nTrialArray;

    save('fit_mri_5.mat','recoveredParameters','nTrials','finalChiSq', ...
         'exitFlags','fitSettings')

diary off