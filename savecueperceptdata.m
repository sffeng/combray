function d = savecueperceptdata(subjects,cueArray,perceptArray)

%% Data settings

if nargin < 3
    perceptArray = [.65 .85];
end

if nargin < 2
    cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
end

if nargin < 1
    subjects = [1:9 12:16];
end

%% Setup arrays
d = struct([]); 

for k = 1:length(cueArray(:,1))
for j = 1:length(perceptArray)

    % collect current cue/percept
    cues = cueArray(k,:);
    percept = perceptArray(j);

    d(k,j).cues = cues;
    d(k,j).percept = percept;


    [tr] = gettesttrials(subjects,cues,0,percept);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    
    d(k,j).rt = rt;
    d(k,j).resp = resp;
    d(k,j).nTrials = nTrials;

    
end
end

disp('Saving data to cueperceptdata.mat');
save('cueperceptdata.mat','d');
