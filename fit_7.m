% Runs the "round 7" fits, which fit 5 parameters (2 drifts,
% threshold, deadline, nondecision time) for a two stage ddm, but
% now trials are grouped into eight pools, depending onthe four cue categories
% (.5 .6 .7 .8) and two perceptual levels (.65 .85).
%
% Also started saving exitflags and explicit fit settings
%
% 12 Nov: rerunning, changed some bounds

clear
close all

%% Genetic algorithm options
lb = [-.2 -.2 0.05 1.75 0]; %d1 d2 z dl T0
ub = [3.0 3.0 5.0  4.75  .5];
nVars = length(lb);
opts = gaoptimset('UseParallel','always','Display','diagnose',...
                  'Generations',100*nVars,...
                  'PopulationSize',10*nVars);

%% Data settings
subjects = [1:9 12:16];
cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
perceptArray = [.65 .85];
nFits = length(cueArray(:,1))*length(perceptArray);

%% Setup arrays
recoveredParameters = nan(nFits,length(ub));
nTrialArray = nan(nFits,1);
finalChiSq = nan(nFits,1);
exitFlags = nan(nFits,1);
fitSettings = nan(nFits,3);

runIter = 1;
for k = 1:length(cueArray(:,1))
for j = 1:length(perceptArray)
    % collect data and output basic stuff
    cues = cueArray(k,:);
    percept = perceptArray(j);
    fitSettings(runIter,:) = [cues percept];
    [tr] = gettesttrials(subjects,cues,0,percept);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues) ' and percept ' num2str(percept)])

    % Solve the sucker
    rng(19850604,'twister'); 
    [recoveredParameters(runIter,:),finalChiSq(runIter), ...
     exitFlags(runIter)] =...
        ga(@(x) obj2a1z1DT0(x,rt,resp), length(lb), [],[],[],[],...
           lb,ub,[],opts);

    nTrialArray(runIter) = nTrials;
    exitFlags
    recoveredParameters
    runIter = runIter + 1;
end
end
nTrials = nTrialArray;

save('tmp.mat','recoveredParameters','nTrials','finalChiSq', ...
     'exitFlags','fitSettings')
