% Runs the "round 13" fits, which fit 7 parameters (2 drifts,
% 2 threshold, x0, deadline, nondecision time) for a 2-stage ddm,
% but ONLY to perceptual responses.
% Trials are grouped into eight pools, depending on the four cue
% categories (.5 .6 .7 .8) and two perceptual levels (.65 .85).
%

clear
close all

%% Genetic algorithm options
lb = [-.2 -.2 1.75 .05 .05 -1  0]; %d1 d2 dl z1 z2 x0 T0
ub = [3.0 3.0 4.75 5.0 5.0  1  .5];
nVars = length(lb);
opts = gaoptimset('UseParallel','always','Display','diagnose',...
                  'Generations',100*nVars,...
                  'PopulationSize',10*nVars);

% impose abs(x0) \leq z1
conA = [0 0 0 -1 0 1 0;
        0 0 0 -1 0 -1 0];
conB = [0;
        0];


%% Data settings (will need to change to cueperceptdata.mat)
subjects = [1:9 12:16];
cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
perceptArray = [.65 .85];
nFits = length(cueArray(:,1))*length(perceptArray);

%% Setup arrays
recoveredParameters = nan(nFits,length(ub));
nTrialArray = nan(nFits,1);
finalChiSq = nan(nFits,1);
exitFlags = nan(nFits,1);
fitSettings = nan(nFits,3);

runIter = 1;
for k = 1:length(cueArray(:,1))
for j = 1:length(perceptArray)
    % collect data and output basic stuff
    cues = cueArray(k,:);
    percept = perceptArray(j);
    fitSettings(runIter,:) = [cues percept];
    [tr] = gettesttrials(subjects,cues,0,percept);
    rt = tr(:,1);
    resp = tr(:,2);
    
    % Get responses after 1.75
    I = find(rt>1.75);
    rt = rt(I);
    resp = resp(I);

    nTrials = length(rt);
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues) ' and percept ' num2str(percept)])

    % length(rt(resp>0))
    % length(rt(resp<=0))
    % continue

    % Solve the sucker
    rng(19850604,'twister'); 
    [recoveredParameters(runIter,:),finalChiSq(runIter), ...
     exitFlags(runIter)] =...
        ga(@(x) obj2a1d2z1xT0(x,rt,resp), length(lb), conA,conB,...
           [],[], lb,ub,[],opts);

    nTrialArray(run_iter) = nTrials;
    exitFlags
    recoveredParameters
    runIter = runIter + 1;
end
end
nTrials = nTrialArray;

save('fit_13.mat','recoveredParameters','nTrials','finalChiSq', ...
     'exitFlags','fitSettings')
