% Runs the "round 8" fits, which fit 4 parameters (1 drift, threshold,
% x0, nondecision time) for a one stage ddm, but now trials are
% grouped into eight pools, depending ont he four cue categories (.5
% .6 .7 .8) and two perceptual levels (.65 .85).
%
% 18 Oct 2015: SF changed to fit_mri_3

clear
close all

%% Genetic algorithm options
lb = [-.2 .05 -1  0]; %d z x0 T0
ub = [3.0 5.0 1 0.5];
nVars = length(lb);

% impose abs(x0) \leq z
conA = [0 -1 1 0;
        0 -1 -1 0];
conB = [0;
        0];

opts = gaoptimset('UseParallel','always','Display','diagnose',...
                  'Generations',100*nVars,...
                  'PopulationSize',10*nVars);

%% Data settings (will need to change to cueperceptdata.mat)
datadir = 'mri_data/';
subjects = [7:18 20:23];
cueArray = [.5 .5; .4 .6; .3 .7; .2 .8];
perceptArray = [.65 .85];
nFits = length(cueArray(:,1))*length(perceptArray);

%% Setup arrays
recoveredParameters = nan(nFits,length(ub));
nTrialArray = nan(nFits,1);
finalChiSq = nan(nFits,1);
exitFlags = nan(nFits,1);
fitSettings = nan(nFits,3);

runIter = 1;
diary fit_mri_3.diary
for k = 1:length(cueArray(:,1))
for j = 1:length(perceptArray)
    % collect data and output basic stuff
    cues = cueArray(k,:);
    percept = perceptArray(j);
    fitSettings(runIter,:) = [cues percept];
    [tr] = gettesttrials(subjects,cues,0,percept,datadir);
    rt = tr(:,1);
    resp = tr(:,2);
    nTrials = length(rt);
    disp(['Using ' num2str(nTrials) ' trials for cues ' ...
          num2str(cues) ' and percept ' num2str(percept)])

    % Solve the sucker
    rng(19850604,'twister'); 
    [recoveredParameters(runIter,:),finalChiSq(runIter), ...
     exitFlags(runIter)] =...
        ga(@(x) obj1a1z1xT0(x,rt,resp), length(lb), conA,conB,...
           [],[], lb,ub,[],opts);

    nTrialArray(runIter) = nTrials;
    exitFlags
    recoveredParameters
    runIter = runIter + 1;
    nTrials = nTrialArray;
    save('fit_mri_3.mat','recoveredParameters','nTrials','finalChiSq', ...
         'exitFlags','fitSettings')

end
end


    nTrials = nTrialArray;
    save('fit_mri_3.mat','recoveredParameters','nTrials','finalChiSq', ...
         'exitFlags','fitSettings')

    diary off