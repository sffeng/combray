function formatfigurefonts(axishandle)
if nargin < 1
    ah = gca;
else
    ah = axishandle;
end

h = get(ah,'xlabel');
set(h,'FontSize',16,'FontWeight','bold');

h = get(ah,'ylabel');
set(h,'FontSize',16,'FontWeight','bold');

h = get(ah,'title');
set(h,'FontSize',20,'FontWeight','bold');

h = findobj(gcf,'Type','axes','Tag','legend');
set(h,'FontSize',14,'FontWeight','demi')

%set the x and y tick label size
set(ah,'FontSize',12,'FontWeight','demi')