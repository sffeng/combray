% function to standardize axes and labels
%
% Does not do legends
function formatfitfigure(plotType,fh,ah)

if nargin < 3
    ah = gca;
end

if nargin < 2
    fh = gcf;
end

if nargin < 1
    type = '';
end


switch plotType
  case {'drift', 'drifts'},
    axis(ah, [.45 .85 -.2 2.0]);
    title(ah, 'Fitted drift values');
    xlabel(ah, 'Cue proportion of responses predicted');
    ylabel(ah, 'Fitted drift rate');
    formatfigurefonts(ah);
  case {'threshold','thresh','th'},
    axis(ah, [.45 .85 .5 2]);
    title(ah,'Fits of threshold (fixed throughout cue and stim)');
    xlabel(ah,'Cue proportion of responses predicted');
    ylabel(ah,'Fitted threshold z');
    formatfigurefonts(ah);
  case {'ntrials','nt','nT','nTrials'},
    title(ah,'Number of (pooled) trials for each fit');
    xlabel(ah,'Cue proportion of responses predicted');
    ylabel(ah,'Number of trials');
    formatfigurefonts(ah);
  case {'chisq','chi-squared','chiSq','chiSquared'},
    axis(ah,[.45 .9 0 500]);
    title(ah,'Final Chi-squared value of fitted parameters');
    xlabel(ah,'Cue proportion of responses predicted');
    ylabel(ah,'Chi-squared');
    formatfigurefonts(ah);
  case {'deadline','dl','DL'},
    axis(ah,[.45 .85 0 4.5]);
    title(ah,'Fitted Deadline time (sec)');
    xlabel(ah,'Cue proportion of responses predicted');
    ylabel(ah,'Deadline (sec)');
    formatfigurefonts(ah);
  case {'T0','t0','nondecision'},
    axis(ah,[.45 .85 0 .5]);
    title(ah,'Non-decision time');
    xlabel(ah,'Cue proportion of responses predicted');
    ylabel(ah,'Non-decision time (sec)');
    formatfigurefonts(ah);
  case {'X0','x0'},
    axis(ah,[.45 .85 -1 1]);
    title(ah,'Initial condition');
    xlabel(ah,'Cue proportion of responses predicted');
    ylabel(ah,'x_0');
    formatfigurefonts(ah);
  otherwise
    error('Error in formatfitfigure')
    return
end