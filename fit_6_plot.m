
clear, close all, clc
load('fit_6.mat');

%%%%%%%%%%%%%%
d1 = recoveredParameters(:,1);
d2 = recoveredParameters(:,2);
z = recoveredParameters(:,3);
D = recoveredParameters(:,4);
T0 = recoveredParameters(:,5);

cues = [.5 .6 .7 .8];


figure(1)
plot(cues,d1,'r-*','Linewidth',2.5)
hold on
plot(cues,d2,'b-*','Linewidth',2.5)
hold off
axis([.45 .85 -.2 1.2])
title('Fitted drift values')
xlabel('Cue proportion of responses predicted')
ylabel('Fitted drift rate')
legend('Cue drift','Perceptual drift','Location','NorthWest')
formatfigurefonts;
saveas(gcf,'figures/fit_6_drifts.eps','psc2')

figure(2)
plot(cues,z,'g-*','Linewidth',2.5)
axis([.45 .85 .5 2])
title('Fits of threshold (fixed throughout cue and stim)')
xlabel('Cue proportion of responses predicted')
ylabel('Fitted threshold z')
formatfigurefonts
saveas(gcf,'figures/fit_6_threshold.eps','psc2')

figure(3)
bar(cues,nTrials)
title('Number of (pooled) trials for each fit')
xlabel('Cue proportion of responses predicted')
ylabel('Number of trials')
formatfigurefonts
saveas(gcf,'figures/fit_6_ntrials.eps','psc2')

figure(4)
bar(cues,finalChiSq)
title('Final Chi-squared value of fitted parameters')
xlabel('Cue proportion of responses predicted')
ylabel('Chi-squared')
axis([.45 .9 0 500])
formatfigurefonts
saveas(gcf,'figures/fit_6_chisq.eps','psc2')

figure(5)
plot(cues,D,'m-*','Linewidth',2.5)
title('Fitted Deadline time (sec)')
xlabel('Cue proportion of responses predicted')
ylabel('Deadline (sec)')
axis([.45 .85 0 2.5])
formatfigurefonts
saveas(gcf,'figures/fit_6_deadline.eps','psc2')

figure(6)
plot(cues,T0,'k-*','Linewidth',2.5)
title('Non-decision time (sec)')
xlabel('Cue proportion of responses predicted')
ylabel('Non-decision time')
axis([.45 .85 0 .5])
formatfigurefonts
saveas(gcf,'figures/fit_6_T0.eps','psc2')


set(1,'Position',[50 690 560 420])
set(2,'Position',[630 690 560 420])
set(3,'Position',[50 185 560 420])
set(4,'Position',[630 185 560 420])
set(5,'Position',[1200 690 560 420])
set(6,'Position',[1200 185 560 420])

fprintf(1,'\n\n*** Writing table to fit_6_table.txt ***\n\n\n')
fid = fopen('fit_6_table.txt','w');

for k = 1:4

    nparams = 5;
    aic = finalChiSq(k) + 2*nparams;
    bic = finalChiSq(k) + nparams*log(nTrials(k));

    fprintf(fid,'\\hline ');
    fprintf(fid,'$%2.1f$ / $%2.1f$ & ',cues(k),1-cues(k));
    fprintf(fid,'$%4.1f$ & ',finalChiSq(k));
    fprintf(fid,'$%3.3d$ & ',nTrials(k));
    fprintf(fid,'$%4.1f$ & ',aic);
    fprintf(fid,'$%4.1f$ & ',bic);
    fprintf(fid,'$%4.4f$ & ',d1(k));
    fprintf(fid,'$%4.4f$ & ',d2(k));
    fprintf(fid,'$%4.4f$ & ',z(k));
    fprintf(fid,'$%4.4f$ & ',D(k));
    fprintf(fid,'$%4.4f$ '  ,T0(k));
    fprintf(fid,'\\\\\n');
end
fclose(fid);