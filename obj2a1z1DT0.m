% objective function for fitting three parameters for a two stage DDM:
% two drifts, one threshold, using rt2002.
%

function val = obj2a1z1DT0(x,rt,rtResp)
a = [x(1) x(2)];
z = [x(3) x(3)];
dl = [0 x(4)];
T0 = x(5);
% Fixed (experimentally set) values
s = [1 1];
x0 = 0; % before cue comes on, either alternative equally likely

tFinal = max(rt) + 3;
val = rt2002(rt,rtResp,a,s,z,x0,1,dl,tFinal,T0);
